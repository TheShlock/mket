"""
Create scoring for reversals
    Define reversals

Storing the scores
    An ordered dict

Define invalidations
    pass throughs and their effect on the relative score

Devise error function
    price passing through is bad
    price reversing is good

    price randomly moves around
    the null hypothesis is that support/resistance has no effect on the movements
    therefore we should see the distribution of price movement near levels to break through the level

    will we see a reversal?
    what is the size of the move after the reversal?!?nahhh>?

"""

import pandas as pd
import numpy as np
from numpy.core.numeric import NaN
import matplotlib.pyplot as plt
import talib as ta
from scipy.signal import argrelextrema
import heapq

def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append([row.open, row.high, row.low, row.close])
    df = pd.DataFrame(condensed, columns=['open', 'high', 'low', 'close'])
    return df

class trader(object):
    def __init__(self):
        self.active = None
        self.entry = 0
        self.position = 0
        self.capital = 1000
        self.trades = 0

    def pos(self, price, pos):
        self.capital += ((price-self.entry)/price) * self.position * self.capital
        self.position = pos
        self.entry = price
        self.trades += 1

class Node(object):
    def __init__(self, value):
        self.value = value
        self.link = None

class linkedList(object):
    def __init__(self, init_):
        self.head = Node(init_)
    
    def link(self, node):
        node.link = self.head
        self.head = node
    
    def replace(self):
        self.head = self.head.link

class counter(object):
    def __init__(self, limit):
        self.val = 0
        self.limit = limit

    def change(self, value):
        self.val += value
        if self.val > self.limit:
            self.val = self.limit
        if self.val < -self.limit:
            self.val = -self.limit

df = condense(24, pd.read_csv("../stats/bitstamp_hourly.csv")[40000:])

"""Find minima and maxima"""
n = 1  # number of points to be checked before and after
 # interesting property when n = 1 minima and maxima always consecutive
df['min'] = df.iloc[argrelextrema(np.array(df.close), np.less_equal,
                    order=n)]['close']
df['max'] = df.iloc[argrelextrema(np.array(df.close), np.greater_equal,
                    order=n)]['close']

"""Set up environment"""
t = trader()
df["buy"] = NaN
df["sell"] = NaN
df['sequence'] = 0

"""Compose sequenc of the count of breaks of minima and maxima
The value written is +1 for each maxima broke by this candle and
-1 for each minima broken"""
def compose_sequence():
    minims = linkedList(1)
    maxims = linkedList(9999999)
    sequence = []
    for key, val in df.iterrows():
        seq_item = 0
        while val['close'] < minims.head.value:
            minims.replace()
            seq_item -= 1

        while val['close'] > maxims.head.value:
            maxims.replace()
            seq_item += 1

        df.at[key, 'sequence'] = seq_item

        if not pd.isna(val['min']):
            minims.link(Node(val['close']))
        if not pd.isna(val['max']):
            maxims.link(Node(val['close']))

        df.at[key, 'capital'] = t.capital

    return sequence

def rand_strat(n):
    x = trader()
    for key, val in df.iterrows():
        if np.random.randn() > 0:
            x.pos(val['close'], 1)
        else:
            x.pos(val['close'], -1)
        df.at[key, 'capital'] = x.capital

def seq_strat_count(s, thresh, limit):
    """Keep a count that pertains to the sequence
    limit is the max count of the sequence
    modes are 
    buy if sequence greater than 0 and sell if its lower
    count starts from 0 and counts up past a certain threshold for buy/sell, if the count moves in the opposite direction
    it resets to 0"""
    x = trader()
    count = counter(limit)
    for key, val in df.iterrows():
        count.change(val['sequence'])
        if count.val >= thresh:
            x.pos(val['close'], -1)
        if count.val <= -thresh:
            x.pos(val['close'], 1)
        df.at[key, f"capital{s}"] = x.capital

def seq_strat_rst(s, thresh, limit):
    """Keep a count that pertains to the sequence
    limit is the max count of the sequence
    modes are 
    buy if sequence greater than 0 and sell if its lower
    count starts from 0 and counts up past a certain threshold for buy/sell, if the count moves in the opposite direction
    it resets to 0"""
    x = trader()
    count = counter(limit)
    for key, val in df.iterrows():
        if count.val > 0 and val['sequence'] < 0 :
            count.val = 0
        if count.val < 0 and val['sequence'] > 0 :
            count.val = 0
        count.change(val['sequence'])
        if count.val >= thresh:
            x.pos(val['close'], 1)
        if count.val <= -thresh:
            x.pos(val['close'], -1)
        if count.val == 0:
            x.pos(val['close'], 0)
        df.at[key, f"capital{s}"] = x.capital
    print(x.capital)

        
print_l = [] 
for i in range(14):
    df[f'capital{i}'] = 1000
    print_l.append(i)
    # seq_strat_count(str(i), 1, i`)

compose_sequence()
for i in range(10):
    seq_strat_rst(str(i), i, 10)
    
# seq_strat_rst('1', 2, 10)
# seq_strat_rst('2', 3, 10)
# seq_strat_rst('3', 4, 10)
# seq_strat_rst('4', 5, 10)
# seq_strat_count('1', 2, 2)
# seq_strat_count('2', 2, 3)
# seq_strat_count('3', 2, 4)
# seq_strat_count('4', 2, 5)
# seq_strat_count('5', 2, 6)
# seq_strat_count('6', 2, 7)
# seq_strat_count('7', 2, 8)
# seq_strat_count('8', 2, 9)
# seq_strat_count('9', 2, 10)

df.to_csv('trend.csv')

rand_strat('11')
rand_strat('12')
rand_strat('13')

"""Printing Graph"""
fig = plt.figure()
gs = fig.add_gridspec(2, hspace=0)
ax = gs.subplots(sharex=True)

ax[0].plot(df.close, color = 'black')
ax[0].scatter(df.index, df['min'], color = 'blue')
ax[0].scatter(df.index, df['max'], color = 'orange')
start = 0 # for printing shaded areas on graph
for key, val in df.iterrows():
    if not pd.isna(val['buy']):
        ax[0].axvspan(start, key, facecolor='green', alpha=0.2)
        start = key
    if not pd.isna(val['sell']):
        ax[0].axvspan(start, key, facecolor='red', alpha=0.2)
        start = key
# ax[0].axvspan(10, 1000, facecolor='red', alpha=0.2)
for i in print_l:
    ax[1].plot(df.index, df[f"capital{i}"], label = f"{i}")
plt.legend()
plt.yscale("log")

plt.show()
exit()