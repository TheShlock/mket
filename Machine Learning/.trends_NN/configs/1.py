"""
Create scoring for reversals
    Define reversals

Storing the scores
    An ordered dict

Define invalidations
    pass throughs and their effect on the relative score

Devise error function
    price passing through is bad
    price reversing is good

    price randomly moves around
    the null hypothesis is that support/resistance has no effect on the movements
    therefore we should see the distribution of price movement near levels to break through the level

    will we see a reversal?
    what is the size of the move after the reversal?!?nahhh>?

"""

import pandas as pd
import numpy as np
from numpy.core.numeric import NaN
import matplotlib.pyplot as plt
import talib as ta
from scipy.signal import argrelextrema
import heapq

def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append([row.open, row.high, row.low, row.close])
    df = pd.DataFrame(condensed, columns=['open', 'high', 'low', 'close'])
    return df

class trader(object):
    def __init__(self):
        self.active = None
        self.entry = 0
        self.position = 0
        self.capital = 1000
        self.trades = 0

    def pos(self, price, pos):
        self.capital += ((price-self.entry)/price) * self.position * self.capital
        self.position = pos
        self.entry = price
        self.trades += 1

class Node(object):
    def __init__(self, value):
        self.value = value
        self.link = None

class linkedList(object):
    def __init__(self, init_):
        self.head = Node(init_)
    
    def link(self, node):
        node.link = self.head
        self.head = node
    
    def replace(self):
        self.head = self.head.link

df = condense(24, pd.read_csv("../stats/bitstamp_hourly.csv")[40000:])

"""Find minima and maxima"""
n = 1  # number of points to be checked before and after
 # interesting property when n = 1 minima and maxima always consecutive
df['min'] = df.iloc[argrelextrema(np.array(df.close), np.less_equal,
                    order=n)]['close']
df['max'] = df.iloc[argrelextrema(np.array(df.close), np.greater_equal,
                    order=n)]['close']

t = trader()
df.to_csv('trend.csv')
df["buy"] = NaN
df["sell"] = NaN
df['capital'] = t.capital


def compose_sequence():
    minims = linkedList(1)
    maxims = linkedList(9999999)
    sequence = []
    for key, val in df.iterrows():
        while val['close'] < minims.head.value:
            minims.replace()
            sequence += [0]
            df.at[key, 'sell'] = val['close'] # mark
            t.pos(val['close'], -1)

        while val['close'] > maxims.head.value:
            maxims.replace()
            sequence += [1]
            df.at[key, 'buy'] = val['close'] # mark
            t.pos(val['close'], 1)

        if not pd.isna(val['min']):
            minims.link(Node(val['close']))
        if not pd.isna(val['max']):
            maxims.link(Node(val['close']))
        
        print(t.capital)

        df.at[key, 'capital'] = t.capital

    return sequence

compose_sequence()
print(t.capital)
print(df)

fig = plt.figure()
gs = fig.add_gridspec(2, hspace=0)
ax = gs.subplots(sharex=True)

ax[0].plot(df.close, color = 'black')
ax[0].scatter(df.index, df['min'], color = 'blue')
ax[0].scatter(df.index, df['max'], color = 'orange')
start = 0 # for printing shaded areas on graph
for key, val in df.iterrows():
    if not pd.isna(val['buy']):
        ax[0].axvspan(start, key, facecolor='green', alpha=0.2)
        start = key
    if not pd.isna(val['sell']):
        ax[0].axvspan(start, key, facecolor='red', alpha=0.2)
        start = key
# ax[0].axvspan(10, 1000, facecolor='red', alpha=0.2)

ax[1].plot(df.index, df['capital'])
plt.yscale("log")

plt.show()
exit()

plt.plot(df.close)
plt.show()

for key, val in df.iterrows():
    
    if trend != 'down' and minims.m2:
        if df.iloc[key]['close'] < minims.m2:
            trend = 'down'
            t.pos(df.iloc[key]['close'], 1)
            df.iloc[key]['sell'] = df.iloc[key]["close"]+10
            minims.reset()

    if trend != 'up' and maxims.m2:
        if df.iloc[key]['close'] > maxims.m2:
            t.pos(df.iloc[key]['close'], 1)
            trend = 'up'
            df.iloc[key]['buy'] = df.iloc[key]["close"]+10
            maxims.reset()

    if not pd.isna(df.iloc[key]['min']) and trend != 'down':
        minims.push(df.iloc[key]['min'])
    if not pd.isna(df.iloc[key]['max']) and trend != 'up':
        maxims.push(df.iloc[key]['max'])

    print('trend', trend)
    print('min1', minims.m1)
    print('min2', minims.m2)
    print(t.capital)
    print(t.trades)

    # input('next')

"""You have to store the minims and maxims in a variable size data structure or a graph otherwise its just not gonna work
In my heart i believe the algorithm can still be linear candle-wise"""


# if the price exceeds the minim.m2 it becomes a downtrend
    


plt.scatter(df.index, df['min'])
plt.scatter(df.index, df['max'])
plt.scatter(df.index, df['buy'])
plt.scatter(df.index, df['sell'])
plt.plot(df.close)
plt.show()


