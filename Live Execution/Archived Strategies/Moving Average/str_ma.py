#Samuel Lockton 2021 ©
import strategy

class ll(object):
    def __init__(self, value):
        self.value = value
        self.link = None


class MAD(strategy.strategy):
    """Moving Average
    period: number of datapoints to average over
    Takes values and when the period is reached, calculates the average, for each subsequent datapoint, 
    removes the oldest value divided by the period and adds the newest value divided by the period
    Also keeps track of the instantaneous change between the periods.
    """
    def __init__(self, inputs, outputs, period):
        strategy.strategy.__init__(self, inputs, outputs)
        self.period = period
        self.llHead = None
        self.llTail = None
        self.average = None
        self.delta = None
        self.dps = 0 #datapoints


    def interval(self, datamap, timestamp):
        dp = ll(datamap[timestamp][self.inputs]) # datapoint

        if self.dps == 0: # instantiation
            self.llHead = dp
            self.llTail = dp
        
        if self.dps > 0:
            self.llHead.link = dp # link the current head to the new head
            self.llHead = dp # set the new head

        if self.dps == self.period - 1: #calculate the average conventionally
            sum_ = 0
            item = self.llTail
            while item.link:
                sum_ += item.value
                item = item.link
            sum_ += item.value #add the head, as it will fail the while item.link check
            self.average = sum_/self.period

        if self.dps >= self.period: # Update the average and Linked List
            prevAvg = self.average
            self.average -= (self.llTail.value / self.period) # subtract the oldest value
            self.llTail = self.llTail.link # update the tail
            self.average += (dp.value / self.period) # add the newest value
    
            self.delta = self.average - prevAvg

        self.dps += 1

        # add values to datamap
        datamap[timestamp][self.outputs[0]] = self.average #MA value
        try : 
            datamap[timestamp][self.outputs[1]] = self.delta #Change in MA
        except:
            pass
        try:
            if self.delta:
                datamap[timestamp][self.outputs[2]] = self.delta/datamap[timestamp]['open'] #Percentage Change
        except:
            pass
        return datamap
