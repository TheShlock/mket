#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
from time import time, sleep
import collections
import copy

#Project files
import str_ma
import ma_connect

class mketbot(object):
    """Manages sending price data to strategies, and the opening and closing of trades"""
    def __init__(self, strategies):
        self.capital = 1000
        self.wontrades = 0
        self.losttrades = 0
        self.feespaid = 0
        self.finishedtrades = 0
        self.datapoints = 0

        self.risk = 0.01
        self.position = 0 # number of contracts
        self.entryprice = None
        self.exitCondition = None

        self.strategies = strategies

        self.datamap = {}
        self.datarowtemplate = {"open": 0, "high": 0, "low": 0, "close": 0, 'buy': None, 'sell': None}

        for s in self.strategies: # for each of the strategies
            for o in s.outputs:
                self.datarowtemplate[o] = None # add their outputs to the datarowtemplate


    def openPosition(self, newpos):
        response = ma_connect.limit_maker(newpos)
        self.position = response


    def tradeMaker(self, timestamp):
        if self.datamap[timestamp][self.strategies[0].outputs[0]] and self.datamap[timestamp][self.strategies[1].outputs[0]]:
            if self.datamap[timestamp][self.strategies[0].outputs[0]] > self.datamap[timestamp][self.strategies[1].outputs[0]]:
                if not self.position > 0:
                    self.openPosition(1)

            if self.datamap[timestamp][self.strategies[0].outputs[0]] < self.datamap[timestamp][self.strategies[1].outputs[0]]:
                if not self.position < 0:
                    self.openPosition(-1)
        else:
            print('no data yet')


    def interval(self, timestamp, open_, high, low, close):
        datarow = copy.deepcopy(self.datarowtemplate)
        datarow['open'] = open_
        datarow['high'] = high
        datarow['low'] = low

        self.datamap[timestamp] = datarow

        for s in self.strategies:
            if self.datamap[timestamp][s.inputs]:
                self.datamap = s.interval(self.datamap, timestamp) #send the datamap to be modified by each strategy

        self.tradeMaker(timestamp)
        self.datapoints += 1


def live_run(ma1, ma2):
    strategies = [str_ma.MAD('open', [str(ma1)+'MA'], ma1), str_ma.MAD('open', [str(ma2)+'MA'], ma2)]
    bot = mketbot(strategies)
    t = 0
    while True:
        if round(time())%60 == 0: #every 60 seconds
            t += 1
            bot.interval(t, ma_connect.price, None, None, None)
            print(bot.datamap[t])
            sleep(1)

    for i in range(points):
        dataRow = btc.iloc[rsp - i]
        bot.interval(i, dataRow['open'], dataRow['high'], dataRow['low'], dataRow['close']) # feed the bot candles


"""Test Routine"""
while not ma_connect.price:
    continue
# ma_connect.limit_maker(-1)
# sleep(1)
# ma_connect.limit_maker(0)

live_run(8,2)