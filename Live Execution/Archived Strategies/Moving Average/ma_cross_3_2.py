#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
from time import time, sleep
from datetime import datetime
import collections
import copy
import talib as ta

#Project files
import ma_connect

while not ma_connect.price: # wait for prices to become available
    continue

closes = []

position = 0
datetime_attempt = None
datetime_actual = None
entry_attempt = 0
entry_actual = 0
exit_attempt = 0
exit_actual = 0
balance_before = ma_connect.wallet_balance()

side = 'long'

w_dict_prev = {}
w_dict_next = {}

def c_pos(bias):
    datetime_attempt = datetime.now()
    entry_attempt = ma_connect.price
    bal_before = ma_connect.wallet_balance()
    position = round((bias * bal_before / 1000 * ma_connect.price))

    ma_connect.limit_maker(position)

    datetime_actual = datetime.now()
    entry_price_actual = ma_connect.price
    bal_after = ma_connect.wallet_balance()


    with open('live_log2.csv', 'a') as f:
        f.write(
        str(datetime_attempt)+','+
        str(datetime_actual)+','+
        str(position)+','+
        str(entry_attempt)+','+
        str(entry_price_actual)+','+
        str(bal_before)+','+
        str(bal_after)+'\n'
        )

class strategy(object):
    def __init__(self):
        self.closes = []

    def feed(self, price): # takes price, returns position bias {-1, 1}
        self.closes.append(price)
        if len(self.closes) > 3:
            self.closes = self.closes[1:]
        if len(self.closes) == 3:
            df = pd.DataFrame(self.closes, columns = ['close'])
            df['2MA'] = ta.SMA(df.close, timeperiod = 2)
            df['3MA'] = ta.SMA(df.close, timeperiod = 3)
            print(df)
            if df.iloc[-1]['3MA'] >= df.iloc[-1]['2MA']:
                if position <= 0:
                    c_pos(1)

            else:
                if position >= 0:
                    c_pos(-1)

s = strategy()

while True:
    if round(time())%60 == 0:
        s.feed(ma_connect.price)
        sleep(1)