from pybit import HTTP, WebSocket
import time
from datetime import datetime
from threading import *
import json

#Shlockton3
api_key = "RAFATBFTVPVYUCLPUX"
api_secret = "SCEDIBTVFRBQLFRIFHUDFDXVGPBEUETWKCJE"
symbol = "BTCUSDZ21"

session = HTTP(
    endpoint='https://api.bybit.com',
    api_key=api_key,
    api_secret=api_secret
)

ws = WebSocket(
    endpoint='wss://stream.bybit.com/realtime', 
    subscriptions=['execution', 'position', 'instrument_info.100ms.'+symbol], 
    api_key=api_key,
    api_secret=api_secret
)

class bybit_ws(Thread):
    def run(self):
        global price
        price = 0
        global execution
        execution = None
        global position
        position = None
        global open_trades
        open_trades = {}
        
        while True:
            pricedata = ws.fetch('instrument_info.100ms.'+symbol)
            if pricedata:
                tickprice = pricedata['last_price_e4']/10000
                if tickprice != price:
                    price = tickprice
                    # print(price)
            
            executiondata = ws.fetch('execution')
            if executiondata:
                tickexe = executiondata
                if tickexe != execution:
                    execution = tickexe
                    print('execution', json.dumps(execution, indent=4, sort_keys=True))

                    if execution[0]['leaves_qty'] == 0:
                        del_trade = None
                        id = execution[0]['order_id']
                        for trade in open_trades:
                            if open_trades[trade]['sl'] == id:
                                print('stop loss hit, cancelling take profit')
                                cancel = {
                                    'symbol': symbol,
                                    'order_id': open_trades[trade]['tp']
                                    }
                                session.cancel_active_order(**cancel)
                                del_trade = trade

                            if open_trades[trade]['tp'] == id:
                                print('take profit hit, cancelling stop loss')
                                cancel = {
                                    'symbol': symbol,
                                    'stop_order_id': open_trades[trade]['sl']
                                    }
                                print('cancellation request\n', session.cancel_conditional_order(**cancel))
                                del_trade = trade
                                
                        if del_trade:
                            del open_trades[del_trade]
                        
                        print(open_trades)

            positiondata = ws.fetch('position')
            if positiondata:
                tickpos = positiondata
                if tickpos != position:
                    position = tickpos
                    print('position\n', json.dumps(position, indent=4, sort_keys=True))

bybit_ws().start()

def limit_maker(pos):
    """Tell it the size of the position that you want and it handles the rest
    Ensures a maker rebate of 0.025% by replacing order until filled
    Returns the position"""

    limit = {
    'symbol': symbol, 
    'order_type': 'Limit', 
    'side': None, 
    'qty': None, 
    'price': None,
    'time_in_force': 'PostOnly'}

    #returns only the position, should error with unexpected outcomes control is key
    #test the new routine
    while True:
        my_position = session.my_position(**{'symbol': symbol})
        print('my position', my_position)
        # print('side', my_position['result']['side'], 'size', my_position['result']['size'])

        if my_position['result'][0]['data']['side'] == 'Sell':
            cur_position = -my_position['result'][0]['data']['size']
        elif my_position['result'][0]['data']['side'] == 'Buy':
            cur_position = my_position['result'][0]['data']['size']
        else:
            cur_position = 0

        if cur_position == pos:
            return pos # this should be the only return

        savedprice = price
        qty = pos - cur_position
        if qty > 0:
            limit['side'] = 'Buy'
            limit['qty'] = qty
            limit['price'] = savedprice - 0.5
        if qty < 0:
            limit['side'] = 'Sell'
            limit['qty'] = -qty 
            limit['price'] = savedprice + 0.5

        order = session.place_active_order(**limit)
        order_id = order['result']['order_id']
        # print('ORDER', order)
        order_query = session.query_active_order(**{'symbol': symbol, 'order_id': order_id})
        # print('ORDER_QUERY', order_query)

        if order_query['result']['order_status'] == 'New':
            print('order confirmed')
            exec_save = execution
            while True:
                if limit['side'] == 'Buy':
                    if price > savedprice:
                        try:
                            session.cancel_active_order(**{'symbol': symbol, 'order_id': order_id})
                        except:
                            pass
                        break
                if limit['side'] == 'Sell':
                    if price < savedprice:
                        try:
                            session.cancel_active_order(**{'symbol': symbol, 'order_id': order_id})
                        except:
                            pass
                        break
                try:
                    if execution[0]['order_id'] == order_id:
                        print('~~~ORDER FILLED~~~')
                        break
                except:
                    pass


def conditional(pos, trigger_price):
    """takes a price and position and return the trade id if successfully placed otherwise error"""
    conditional = [{
        'symbol': symbol, 
        'order_type': 'Market', 
        'side': None, 
        'qty': abs(pos), 
        'base_price': price, #set to current price
        'stop_px': trigger_price, #trigger price
        'time_in_force': 'PostOnly',
        'close_on_trigger': False
    }]

    if pos > 0: #long
        conditional[0]['side'] = "Buy"
        order = session.place_conditional_order_bulk(conditional)
              
    if pos < 0: #short
        conditional[0]['side'] = "Sell"
        order = session.place_conditional_order_bulk(conditional)

    print('conditional order', order)
    order_id = order[0]['result']['stop_order_id']
    # print(order_id)
    return order_id
    

def limit(pos, trigger_price):
    """takes a price and position and return the trade id if successfully placed otherwise error"""
    limit = {
    'symbol': symbol, 
    'order_type': 'Limit', 
    'side': None, 
    'qty': abs(pos), 
    'price': trigger_price,
    'time_in_force': 'PostOnly'
    }

    if pos > 0: #long
        limit['side'] = "Buy"
        order = session.place_active_order(**limit)
              
    if pos < 0: #short
        limit['side'] = "Sell"
        order = session.place_active_order(**limit)

    print('limit order', order)
    order_id = order['result']['order_id'] # query active order
    order_query = session.query_active_order(**{'symbol': symbol, 'order_id': order_id})
    if order_query['result']['order_status'] == 'New':
        print('order confirmed')
        return order_id
    else:
        print('order failed')
        raise Exception('limit order failed')
    

def wallet_balance(): 
    """returns wallet balance in BTC"""
    balance = session.get_wallet_balance()
    print(json.dumps(balance['result']['BTC'], indent=4, sort_keys=True))
    return balance['result']['BTC']['equity']


def market(pos):
    """Market buys to set position to pos"""
    market = {
    'symbol': symbol, 
    'order_type': 'Market', 
    'side': None, 
    'qty': abs(pos), 
    'price': price,
    'time_in_force': 'PostOnly'
    }

    pos_info = session.my_position(symbol=symbol)['result'][0]['data']
    cur_pos = pos_info['size']

    if pos_info['side'] == 'Sell':
        cur_pos = -pos_info['size']

    order = pos-cur_pos # calculate the difference
    if order == 0:
        return 'current position is already ' + str(pos)

    if order > 0:
        market['side'] = 'Buy'
    if order < 0:
        market['side'] = 'Sell'

    market['qty'] = abs(order)
    return session.place_active_order(**market)


def cancel_all():
    cancel_all = {
    'symbol': symbol
    }

    print(session.cancel_all_active_orders(**cancel_all))
    print(session.cancel_all_conditional_orders(**cancel_all))
    print(market(0))


def sl_tp(pos, sl, tp):
    try:
        market(pos)
        sl_id = conditional(-pos, sl)
        print('sl_id', sl_id)
        tp_id = limit(-pos, tp)
        print('tp_id', tp_id)
        open_trades[str(pos)+'/'+datetime.now().strftime("%H:%M:%S")] = {'sl': sl_id, 'tp': tp_id}
    except:
        print('something went wrong with sl_tp, cancelling all')
        cancel_all()


"""Make orders and test code underneath this line"""
while not price:
    continue

wallet_balance()
