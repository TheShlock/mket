#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
from datetime import datetime
import collections
import copy
import talib as ta
import ma_connect

"""This code uses bollinger bands and ATR"""
lencsv = len(pd.read_csv('30minbtc.csv'))
while True:
    df = pd.read_csv('30minbtc.csv')
    if len(df) > lencsv: # a new row has been added
        """Prepare the dataframe with indicators/outputs"""
        bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=2,nbdevdn=2,matype=0)
        df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
        df.columns = ["open", "high", "low", "close", "bb_upper", "bb_middle", "bb_lower"]
        df['RSI'] = ta.RSI(df.close, timeperiod = 14)
        df['MOM'] = ta.MOM(df.close, timeperiod = 14)
        df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)

        print('this is the result:\n', df)

        if df.iloc[-1]["high"] > df.iloc[-1]["bb_upper"]:
            ma_connect.sl_tp(-100, df.iloc[-1]['close'] + df.iloc[-1]["ATR"], df.iloc[-1]['close'] + (df.iloc[-1]["ATR"]*1.5))
            
        if df.iloc[-1]["low"] > df.iloc[-1]["bb_lower"]:
            ma_connect.sl_tp(-100, df.iloc[-1]['close'] - df.iloc[-1]["ATR"], df.iloc[-1]['close'] - (df.iloc[-1]["ATR"]*1.5))



