from pybit import HTTP, WebSocket
import time
from datetime import datetime
from threading import *
import json
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.DEBUG,
    filename='logs.log')


#sub2
api_key = "SLBQDTRZAOQBVPUBLG"
api_secret = "TWQOCGLJAVIKJEUJWKCKZUKKARFNZENLDYMW"
symbol = "BTCUSDZ21"

session = HTTP(
    endpoint='https://api.bybit.com',
    api_key=api_key,
    api_secret=api_secret
)

"""Gets current price"""
def price():
    return session.latest_information_for_symbol(**{'symbol': symbol})['result'][0]['last_price']


def current_position():
    my_position = session.my_position(**{'symbol': symbol})

    if my_position['result'][0]['data']['side'] == 'Sell':
        cur_position = -my_position['result'][0]['data']['size']
    elif my_position['result'][0]['data']['side'] == 'Buy':
        cur_position = my_position['result'][0]['data']['size']
    else:
        cur_position = 0
    
    return cur_position


def conditional(pos, trigger_price):
    """takes a price and position and return the trade id if successfully placed otherwise error"""
    conditional = [{
        'symbol': symbol, 
        'order_type': 'Market', 
        'side': None, 
        'qty': abs(pos), 
        'base_price': price(), #set to current price
        'stop_px': trigger_price, #trigger price
        'time_in_force': 'PostOnly',
        'close_on_trigger': False
    }]

    if pos > 0: #long
        conditional[0]['side'] = "Buy"
        order = session.place_conditional_order_bulk(conditional)
              
    if pos < 0: #short
        conditional[0]['side'] = "Sell"
        order = session.place_conditional_order_bulk(conditional)

    logging.info(f"conditional order {order}")
    order_id = order[0]['result']['stop_order_id']
    return order_id # if order_id is not available in the return then this transaction failed
    

def limit(pos, trigger_price):
    """takes a price and position and return the trade id if successfully placed otherwise error"""
    limit = {
    'symbol': symbol, 
    'order_type': 'Limit', 
    'side': None, 
    'qty': abs(pos), 
    'price': trigger_price,
    'time_in_force': 'PostOnly'
    }

    if pos > 0: #long
        limit['side'] = "Buy"
        order = session.place_active_order(**limit)
              
    if pos < 0: #short
        limit['side'] = "Sell"
        order = session.place_active_order(**limit)
    logging.info(f"limit order: {order}")
    order_id = order['result']['order_id'] # query active order
    order_query = session.query_active_order(**{'symbol': symbol, 'order_id': order_id})
    if order_query['result']['order_status'] == 'New':
        logging.info("order confirmed")
        return order_id
    else:
        logging.warning("order failed")
        raise Exception('limit order failed')
    

def wallet_balance(): 
    """returns wallet balance in BTC"""
    balance = session.get_wallet_balance()
    logging.info(json.dumps(balance['result']['BTC']))
    return balance['result']['BTC']['equity']


def market(pos):
    """Market buys to set position to pos"""
    logging.info(f"market buy to {pos}")
    market = {
    'symbol': symbol, 
    'order_type': 'Market', 
    'side': None, 
    'qty': abs(pos), 
    'price': price(),
    'time_in_force': 'PostOnly'
    }

    pos_info = session.my_position(symbol=symbol)['result'][0]['data']
    cur_pos = pos_info['size']

    if pos_info['side'] == 'Sell':
        cur_pos = -pos_info['size']

    order = pos-cur_pos # calculate the difference
    if order == 0:
        return 'current position is already ' + str(pos)

    if order > 0:
        market['side'] = 'Buy'
    if order < 0:
        market['side'] = 'Sell'

    market['qty'] = abs(order)
    return session.place_active_order(**market)


def cancel_all():
    cancel_all = {
    'symbol': symbol
    }
    logging.info(f"{session.cancel_all_active_orders(**cancel_all)}")
    logging.info(f"{session.cancel_all_conditional_orders(**cancel_all)}")
    logging.info(f"{market(0)}")


def sl_tp(pos, sl, tp):
    # try:
    market(pos)
    sl_id = conditional(-pos, sl)
    logging.info(f"sl_id:  {sl_id}")
    tp_id = limit(-pos, tp)
    logging.info(f"tp_id {tp_id}")
    with open('open_trades.txt', 'r') as f: # this will fail unless open_trades.txt is in directory and contains a dict
        trades = json.load(f)
        trades[str(pos)+'_'+str(datetime.now())] = {'pos':pos, 'sl': sl_id, 'tp': tp_id}

    with open('open_trades.txt', 'w') as f:
        f.write(json.dumps(trades))

    # except:
    #     print('something went wrong with sl_tp, cancelling all')
    #     cancel_all()


def query_orders():
    orders = {}
    response = session.query_active_order(**{'symbol': symbol})
    limit_orders = []
    for item in response['result']:
        limit_orders.append(item['order_id'])
    orders['limit'] = limit_orders

    response = session.query_conditional_order(**{'symbol': symbol})
    conditional_orders = []
    for item in response['result']:
        conditional_orders.append(item['order_id'])
    orders['conditional'] = conditional_orders

    return orders
    
def check_and_settle():
    """will both close when their pair order is filled as well as removing orders not assigned to a trade"""
    logging.info("1. ensure each server order exists locally")
    orders = query_orders()
    print(orders)
    logging.info(orders)
    with open('open_trades.txt', 'r') as f:
        trades = json.load(f)
    logging.info(trades)
    for order in orders['limit']: #server
        exists = False
        for trade in trades: #local
            logging.info(trades[trade])
            if order == trades[trade]['tp']:
                exists = True
                break
        if exists == False:
                cancel = {
                'symbol': symbol,
                'order_id': order
                }
                try:
                    logging.info(f"cancellation request\n {session.cancel_active_order(**cancel)}")
                except:
                    logging.info("cancellation was unsuccesful")
                    continue
                check_and_settle()
                return 0
    
    for order in orders['conditional']: #server
        exists = False
        for trade in trades: #local
            if order == trades[trade]['sl']:
                exists = True
                break
        if exists == False:
                cancel = {
                'symbol': symbol,
                'order_id': order
                }
                try:
                    logging.info(f"cancellation request\n {session.cancel_active_order(**cancel)}")
                except:
                    logging.info("cancellation was unsuccesful")
                    continue
                check_and_settle()
                return 0

    orders = query_orders()
    with open('open_trades.txt', 'r') as f:
        trades = json.load(f)
    with open('open_trades.txt', 'w') as f:
        f.write(json.dumps(trades))

    logging.info("2. ensure each local order exists on the server")
    logging.info(f"conditional orders {orders['conditional']}")
    logging.info(f"limit orders {orders['limit']}")
    for trade in trades:
        logging.info(trades[trade])
        logging.info(trades[trade]['sl'])
        if trades[trade]['sl'] not in orders['conditional']:
            logging.info("stop loss hit, cancelling take profit")
            cancel = {
                'symbol': symbol,
                'order_id': trades[trade]['tp']
                }
            try:
                logging.info(f"cancellation request\n {session.cancel_active_order(**cancel)}")
            except:
                logging.info("cancellation was unsuccesful")
            del trades[trade]
            logging.info(trades)
            with open('open_trades.txt', 'w') as f:
                f.write(json.dumps(trades))
            check_and_settle()
            return 0

        if trades[trade]['tp'] not in orders['limit']:
            logging.info("take profit hit, cancelling stop loss")
            cancel = {
                'symbol': symbol,
                'stop_order_id': trades[trade]['sl']
                }
            try:
                logging.info(f"cancellation request\n {session.cancel_conditional_order(**cancel)}")
            except:
                logging.info("cancellation was unsuccesful")
                continue
            del trades[trade]
            logging.info(trades)
            with open('open_trades.txt', 'w') as f:
                f.write(json.dumps(trades))
            check_and_settle()
            return 0



    logging.info('3. ensure the sum of local positions is the actual position')
    with open('open_trades.txt', 'r') as f:
        trades = json.load(f)
    logging.info(trades)
    cur_pos = current_position()
    logging.info(f"current position {cur_pos}")

    sum_pos = 0
    for trade in trades:
        logging.info(trades[trade])
        sum_pos += int(trades[trade]['pos'])
        
    logging.info(f"sum of trades {sum_pos}")
    if sum_pos != cur_pos:
        logging.info("sum of trades does not equal to the current position, adjusting")
        market(sum_pos)
        check_and_settle()
        return 0
    
    logging.info("check_and_settle completed without error")



"""Make orders and test code underneath this line"""
check_and_settle()

