#Samuel Lockton 2021 ©

# Mathematical packages
import pandas as pd
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

import requests 
import json 
import pandas as pd
import datetime as dt
import time
import connect
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.DEBUG,
    filename='logs.log')


def get_bybit_bars(symbol, interval, startTime, endTime):
    url = "https://api.bybit.com/v2/public/kline/list"
    startTime = str(int(startTime.timestamp()))
    endTime   = str(int(endTime.timestamp()))
    req_params = {"symbol" : symbol, 'interval' : interval, 'from' : startTime, 'to' : endTime}
    df = pd.DataFrame(json.loads(requests.get(url, params = req_params).text)['result'])
    if (len(df.index) == 0):
        return None
    df.index = [dt.datetime.fromtimestamp(x) for x in df.open_time]
 
    return df


stdev = 1.8 #standard deviation of bollinger bands
rr = 10 #risk reward
patr = 0.8 #proportion of ATR
active_order = False
wait = False
logging.info("Process started\n\n\n")
while True:
    try:
        if round(time.time()) % 3600 == 0:
            with open('open_trades.txt', 'r') as f:
                trades = json.load(f)
            if not trades:
                if wait == False:
                    df = get_bybit_bars('BTCUSDZ21', 60, dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=2), dt.datetime.now())
                    """Prepare the dataframe with indicators/outputs"""
                    bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=stdev,nbdevdn=stdev,matype=0)
                    df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
                    df.columns = ['symbol','interval','open_time','open','high','low','close','volume','turnover','bb_upper','bb_middle','bb_lower']
                    df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)
                    df.to_csv('boof_df.csv')

                    if float(df.iloc[-1]['high']) > float(df.iloc[-1]['bb_upper']):
                        # test code
                        ordersize = round(connect.wallet_balance() * float(df.iloc[-1]['close']))
                        #
                        sl = round(float(df.iloc[-1]['close']) + float(df.iloc[-1]['ATR']*patr))
                        tp = round(float(df.iloc[-1]['close']) - float(df.iloc[-1]['ATR']*rr*patr))
                        wait = True
                        with open('boof_results.csv','a') as f:
                            f.write(f"{dt.datetime.now()},{-ordersize},{sl},{tp},{df.iloc[-1]['close']},")
                            connect.sl_tp(-ordersize, sl, tp)
                        logging.info(f"price {df.iloc[-1]['close']} sl {sl} tp {tp}")

                    if float(df.iloc[-1]["low"]) < float(df.iloc[-1]["bb_lower"]):
                        sl = round(float(df.iloc[-1]['close']) - float(df.iloc[-1]["ATR"]*patr))
                        tp = round(float(df.iloc[-1]['close']) + float(df.iloc[-1]["ATR"]*rr*patr))
                        wait = True
                        with open('boof_results.csv','a') as f:
                            f.write(f"{dt.datetime.now()},{ordersize},{sl},{tp},{df.iloc[-1]['close']},")
                            connect.sl_tp(ordersize, sl, tp)
                        logging.info(f"price: {df.iloc[-1]['close']} sl: {sl} tp: {tp}")

                else:
                    logging.info('Cooling off')
                    with open('boof_results.csv','a') as f:
                        f.write(f"{connect.wallet_balance()}\n")
                    wait = False
                    time.sleep(1)
            else:
                logging.info("there is already an active trade")
                wait = True
                time.sleep(1)
        
        if round((time.time()+60) % 600) == 0:
            logging.info(dt.datetime.now())
            connect.check_and_settle()

        time.sleep(0.5)
    except:
        continue
