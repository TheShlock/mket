#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

import requests 
import json 
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import time
import connect


def get_bybit_bars(symbol, interval, startTime, endTime):
 
    url = "https://api.bybit.com/v2/public/kline/list"
 
    startTime = str(int(startTime.timestamp()))
    endTime   = str(int(endTime.timestamp()))
 
    req_params = {"symbol" : symbol, 'interval' : interval, 'from' : startTime, 'to' : endTime}
 
    df = pd.DataFrame(json.loads(requests.get(url, params = req_params).text)['result'])
 
    if (len(df.index) == 0):
        return None
     
    df.index = [dt.datetime.fromtimestamp(x) for x in df.open_time]
 
    return df


stdev = 2 #standard deviation of bollinger bands
rr = 10 #risk reward
patr = 0.5 #proportion of ATR

# print(get_bybit_bars('BTCUSD', 30, dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=2), dt.datetime.now()))
active_order = False
wait = False
while True:
    if round(time.time()) % 1800 == 0:
        if not connect.open_trades:
            if wait == False:
                print(dt.datetime.now())
                time.sleep(2)
                df = get_bybit_bars('BTCUSD', 30, dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=2), dt.datetime.now())

                """Prepare the dataframe with indicators/outputs"""
                bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=stdev,nbdevdn=stdev,matype=0)
                df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
                df.columns = ["symbol","interval","open_time","open","high","low","close","volume","turnover","bb_upper","bb_middle","bb_lower"]
                df['RSI'] = ta.RSI(df.close, timeperiod = 14)
                df['MOM'] = ta.MOM(df.close, timeperiod = 14)
                df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)

                print(df)
                df.to_csv('boof_df.csv')

                if float(df.iloc[-1]["high"]) > float(df.iloc[-1]["bb_upper"]):
                    sl = round(connect.price + (float(df.iloc[-1]["ATR"])*patr))
                    tp = round(connect.price - (float(df.iloc[-1]["ATR"])*rr*patr))
                    wait = True
                    with open('boof_results.csv','a') as f:
                        f.write(str(dt.datetime.now()) + ',-10,' + str(sl)+','+str(tp)+','+str(connect.price)+',')
                        connect.sl_tp(-10, sl, tp)
                        print('price', connect.price, 'sl', sl, 'tp', tp)

                if float(df.iloc[-1]["low"]) < float(df.iloc[-1]["bb_lower"]):
                    sl = round(connect.price - (float(df.iloc[-1]["ATR"])*patr))
                    tp = round(connect.price + (float(df.iloc[-1]["ATR"])*rr*patr))
                    wait = True
                    with open('boof_results.csv','a') as f:
                        f.write(str(dt.datetime.now()) + ',10,' + str(sl)+','+str(tp)+','+str(connect.price)+',')
                        connect.sl_tp(10, sl, tp)
                        print('price', connect.price, 'sl', sl, 'tp', tp)
            else:
                print('Cooling off')
                with open('boof_results.csv','a') as f:
                    f.write(str(connect.wallet_balance())+'\n')
                wait = False
                time.sleep(1)
        else:
            print('there is already an active trade')
            time.sleep(1)
    time.sleep(0.5)

