import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import csv
import time
from datetime import datetime
import collections
import copy
import talib as ta
import ma_connect


def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append([row.open, row.high, row.low, row.close])
    df = pd.DataFrame(condensed, columns=['open', 'high', 'low', 'close'])
    return df

class timeframe(object):
    """Timeframe object used to provide OHLC data for higher timeframes while only being fed smaller TFs"""
    def __init__(self):
        self.open = 0
        self.high = 0
        self.low = 9999999999
        self.close = 0
    
    def feed(self, p):
        if not self.open:
            self.open = p
        if p > self.high:
            self.high = p
        if p < self.low:
            self.low = p

    def reset(self):
        self.open = self.close
        self.high = 0
        self.low = 0
        self.close = 0
        self.dps = 0

tfs = {
    3: timeframe(),
    5: timeframe(),
    10: timeframe(),
    15: timeframe(),
    20: timeframe(),
    30: timeframe(),
}
for t in tfs:

print(tfs[3].low)

cprice = ma_connect.price
while True:
    if ma_connect.price != cprice:
        for t in tfs:
            tfs[t].feed(ma_connect.price)
