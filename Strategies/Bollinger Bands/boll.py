#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
from datetime import datetime
import collections
import copy
import talib as ta

"""This code uses bollinger bands and ATR"""
def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append([row.open, row.high, row.low, row.close])
    df = pd.DataFrame(condensed, columns=['open', 'high', 'low', 'close'])
    return df


class trade(object):
    def __init__(self, pos, entry, sl, tp):
        self.pos = pos
        self.entry = entry
        self.sl = sl
        self.tp = tp
        self.state = 'active'

    def check_trade(self, row):
        if self.pos > 0: # long
            if row['low'] < self.sl:
                self.state = 'stopped_out'
            if row['high'] > self.tp:
                self.state = 'took_profit'
        if self.pos < 0: # short
            if row['high'] > self.sl:
                self.state = 'stopped_out'
            if row['low'] < self.tp:
                self.state = 'took_profit'


def single_tf_test(tf, dataset, stdev, rr, patr):

    """Read the dataset"""
    df = condense(tf, pd.read_csv(dataset))

    """Prepare the dataframe with indicators/outputs"""
    bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=stdev,nbdevdn=stdev,matype=0)

    df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
    df.columns = ["open", "high", "low", "close", "bb_upper", "bb_middle", "bb_lower"]
    df['RSI'] = ta.RSI(df.close, timeperiod = 14)
    df['MOM'] = ta.MOM(df.close, timeperiod = 14)
    df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)
    df["buy"] = NaN
    df["sell"] = NaN
    df["close_pos"] = NaN
    df["capital"] = 1000
    # print('this is the result:\n', df)
    # print(df.info())
    # time.sleep(10)

    # """Add spot for capital"""
    # for s in strategies:
    #     df[s.name] = 0

    """Main loop"""
    active_trade = None
    capital = 1000
    won_trades = 0
    lost_trades = 0
    t = time.time()
    for k, row in df.iterrows():
        # time.sleep(0.1)
        # print(k)
        # print(capital)
        if active_trade: #check on active trade
            active_trade.check_trade(row)
            if active_trade.state == 'stopped_out':
                lost_trades += 1
                capital += ((active_trade.sl-active_trade.entry)/active_trade.entry)*active_trade.pos
                df.loc[k, 'close_pos'] = active_trade.sl
                active_trade = None

            elif active_trade.state == 'took_profit':
                won_trades += 1
                capital += ((active_trade.tp-active_trade.entry)/active_trade.entry)*active_trade.pos
                df.loc[k, 'close_pos'] = active_trade.tp
                active_trade = None

        else: #look for entries
            # if row["high"] > row["bb_upper"] and df.iloc[k]['bb_middle'] - df.iloc[k-1]['bb_middle'] < 0:
            if row["high"] > row["bb_upper"]:
                df.loc[k, "sell"] = row["close"]
                active_trade = trade(-capital, row["close"], row["close"]+row["ATR"]*patr, row["close"]-row["ATR"]*rr*patr)
                # print('sl', row["close"], row["close"]+row["ATR"], row["close"]-(row["ATR"]*1.5))

            if row["low"] < row["bb_lower"]:
                df.loc[k, "buy"] = row["close"]
                active_trade = trade(capital, row["close"], row["close"]-row["ATR"]*patr, row["close"]+row["ATR"]*rr*patr)
                # print('sl', row["close"], row["close"]+row["ATR"], row["close"]-(row["ATR"]*1.5))
        df.loc[k, 'capital'] = capital
        # print(row)

    """Output section"""
    print(df)
    print('won', won_trades, 'lost', lost_trades, 'capital', capital)
    name = 'boll_simple'+'_'+str(tf)+'min_'+'STDEV'+str(stdev)+'_'+'RR'+str(rr)+'_'+'PATR'+str(patr)+'_'+datetime.now().strftime("%H:%M:%S")
    with open('boll.csv', 'a') as boll:
        boll.write(name+','+str(won_trades)+','+str(lost_trades)+','+str(capital)+'\n')
    # df.to_csv(name+'.csv')

    # """Matplotlib section"""
    # fig = plt.figure()
    # gs = fig.add_gridspec(2, hspace=0)
    # ax = gs.subplots(sharex=True)
    # plt.title(str(tf))
    
    # # ax[0].plot(df.index, df['open'], label = 'open')
    # # ax[0].plot(df.index, df['high'], label = 'high')
    # # ax[0].plot(df.index, df['low'], label = 'low')
    # ax[0].plot(df.index, df['close'], label = 'close')
    # ax[0].plot(df.index, df['bb_upper'], label = 'bb_upper')
    # ax[0].plot(df.index, df['bb_middle'], label = 'bb_middle')
    # ax[0].plot(df.index, df['bb_lower'], label = 'bb_lower')

    # ax[0].scatter(df.index, df['buy'], label = 'buy')
    # ax[0].scatter(df.index, df['sell'], label = 'sell')
    # ax[0].scatter(df.index, df['close_pos'], label = 'close_pos')
    # ax[0].legend()

    # # ax[1].plot(df.index, df['ATR'])
    # # ax[1].plot(df.index, df['RSI'])
    # ax[1].plot(df.index, df['capital'])
    # plt.yscale("log")


    # plt.tick_params(
    #     axis='x',          # changes apply to the x-axis
    #     which='both',      # both major and minor ticks are affected
    #     bottom=False,      # ticks along the bottom edge are off
    #     top=False,         # ticks along the top edge are off
    #     labelbottom=False) # labels along the bottom edge are off

    # plt.show()

    # next = input('run next test?')
   

# tfs = [3,5,10,15,20,30,60,120,240]


# tfs = [30]

tfs = [3,5,8,10,13,15,20,21,30,34,55,60,89,120,144,233,240]
stdev = [0.1,0.2,0.3,0.5,0.8,1,1.3,1.5,1.8,2,2.1,2.5,3,3.4,5]
rr = [0.5,1,1.5,2,2.5,3,4,5,8,10]
patr = [0.2,0.5,0.8,1,1.3,1.5,2,3,5,8]
for t in tfs[::-1]:
    for s in stdev:
        for r in rr:
            for p in patr:
                print(t, s, r, p)
                single_tf_test(t, "../.datasets/BTCUSD_live.csv", s, r, p)


