#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
from datetime import datetime
import collections
import copy
import talib as ta

def p_change(pos, entry, exit):
    return ((exit-entry)/entry)*pos

"""This code uses bollinger bands and ATR"""
class trade(object):
    def __init__(self, pos, entry, sl, tp):
        self.init_pos = pos # +-
        self.cur_pos = pos # +-
        self.entry = entry # +
        self.sl = sl # +
        self.tp = tp # +
        self.movesize = self.tp - self.entry # +-

        # self.exit_matrix = [[0.1,[0.1,self.sl]],[0.2,[0.1,self.sl]],[0.3,[0.1,self.sl]],[0.4,[0.1,self.sl]],[0.5,[0.1,self.sl]],[0.6,[0.1,self.sl]],[0.7,[0.1,self.sl]],[0.8,[0.1,self.sl]],[0.9,[0.1,self.sl]], [1,[0.1,False]]]
        # self.exit_matrix = [[0.75,[0, self.sl+(self.movesize/2)]],[1,[1,False]]]
        self.exit_matrix = [[1,[1,False]]]

    def check_trade(self, row):
        capital_change = 0
        action_index = 0
        if self.init_pos > 0: # long
            if row['low'] < self.sl: # check for stops first
                capital_change += p_change(self.cur_pos, self.entry, self.sl) - abs(self.cur_pos)*0.0008 # add taker fee
                self.cur_pos = 0 # update position, this marks for deletion
                self.sl = False
                print('stopped')

            else:
                for action in self.exit_matrix: # check for triggered orders in stages, this reflects the fact that the fractions taken will be executed sequentially
                    if row['high'] > self.entry + self.movesize*action[0]: # if price exceeded the trigger
                        capital_change += p_change(self.init_pos*action[1][0], self.entry, self.entry+(self.movesize*action[0]))+abs(self.init_pos*action[1][0])*0.0002 # add maker rebate
                        self.cur_pos -= self.init_pos*action[1][0] # update position
                        self.sl = action[1][1]
                        action_index += 1
                        print('profited')
        

        elif self.init_pos < 0: # short
            if row['high'] > self.sl: # check for stops first
                capital_change += p_change(self.cur_pos, self.entry, self.sl) - abs(self.cur_pos)*0.0008 # add taker fee
                self.cur_pos = 0 # update position, this marks for deletion
                self.sl = False
                print('stopped')

            else:
                for action in self.exit_matrix: # check for triggered orders
                    if row['low'] < self.entry + self.movesize*action[0]: # if price exceeded the trigger
                        capital_change += p_change(self.init_pos*action[1][0], self.entry, self.entry+(self.movesize*action[0]))+abs(self.init_pos*action[1][0])*0.0002 # add maker rebate
                        self.cur_pos -= self.init_pos*action[1][0] # update position
                        self.sl = action[1][1]
                        action_index += 1
                        print('profited')

        self.exit_matrix = self.exit_matrix[action_index:] # remove the triggers that have been hit
        return capital_change




def single_tf_test(dataset, stdev, rr, patr):

    """Read the dataset"""
    df = pd.read_csv(dataset)

    """Prepare the dataframe with indicators/outputs"""
    bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=stdev,nbdevdn=stdev,matype=0)

    df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
    df.columns = ['timestamp', 'symbol', 'interval', 'open_time', "open", "high", "low", "close", 'volume', 'turnover', "bb_upper", "bb_middle", "bb_lower"]
    df['RSI'] = ta.RSI(df.close, timeperiod = 14)
    df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)
    df["open_trade"] = NaN
    df["stop_loss"] = NaN
    df["take_profit"] = NaN
    df["close_trade"] = NaN
    df["capital"] = 1000

    """Main loop"""
    active_trades = {}
    capital = 1000
    won_trades = 0
    lost_trades = 0
    trades = 0
    cooldown = 0
    chill = 1
    for k, row in df.iterrows():
        print(capital)
        print(row['open'], row['high'], row['low'], row['close'])
        print(active_trades)
        # time.sleep(0.1)
        # print(k)
        # print(capital)
        for at in list(active_trades.keys()): #check on active trades
            capital_change = active_trades[at].check_trade(row)
            print(capital_change)
            capital += capital_change
            print(active_trades[at].__dict__)
            if not active_trades[at].sl: 
                print('trade closed')
                # df.loc[k, "close_trade"] = active_trades[at].sl #show the exit point on the chart
                del active_trades[at]
            else:
                df.loc[k, "stop_loss"] = active_trades[at].sl # show the sl, tp on chart when trade is opened
                df.loc[k, "take_profit"] = active_trades[at].tp
                #put the sl and tp on the chart

        if len(active_trades) <= 0:
            if cooldown <= 0:
                if not(row["high"] > row["bb_upper"] and row["low"] < row["bb_lower"]): # if we break both bands, don't enter position
                # if not(row["high"] > row["bb_upper"] and row["low"] < row["bb_lower"]): # if we break both bands, don't enter position
                    if row["high"] > row["bb_upper"]: # upper band broken
                        new_trade = trade(-capital, row["close"], row["close"]+row["ATR"]*patr, row["close"]-row["ATR"]*rr*patr)
                        capital -= capital*0.0008 # opening fee
                        df.loc[k, "open_trade"] = new_trade.entry
                        df.loc[k, "stop_loss"] = new_trade.sl # show the sl, tp on chart when trade is opened
                        df.loc[k, "take_profit"] = new_trade.tp
                        active_trades[row['timestamp']] = new_trade
                        cooldown = chill
                        trades += 1

                    if row["low"] < row["bb_lower"]: # lower band broken
                        new_trade = trade(capital, row["close"], row["close"]-row["ATR"]*patr, row["close"]+row["ATR"]*rr*patr)
                        capital -= capital*0.0008 # opening fee
                        df.loc[k, "open_trade"] = new_trade.entry
                        df.loc[k, "stop_loss"] = new_trade.sl # show the sl, tp on chart when trade is opened
                        df.loc[k, "take_profit"] = new_trade.tp
                        active_trades[row['timestamp']] = new_trade
                        cooldown = chill
                        trades += 1
            else:
                cooldown -= 1


        df.loc[k, 'capital'] = capital
        # input('next')
        print()

    """Output section"""
    print(df)
    print('won', won_trades, 'lost', lost_trades, 'capital', capital)
    print('trades', trades, 'capital', capital)
    with open('retest.csv', 'a') as f:
        f.write('60,'+'simple'+','+str(stdev)+','+str(rr)+','+str(patr)+','+str(won_trades)+','+str(lost_trades)+','+str(capital)+'\n')
    df.to_csv('notcondensed.csv')


    """Matplotlib section"""
    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)
    plt.title('60')
    
    # ax[0].plot(df.index, df['open'], label = 'open')
    ax[0].plot(df.index, df['high'], label = 'high')
    ax[0].plot(df.index, df['low'], label = 'low')
    ax[0].plot(df.index, df['close'], label = 'close')
    ax[0].plot(df.index, df['bb_upper'], label = 'bb_upper')
    ax[0].plot(df.index, df['bb_middle'], label = 'bb_middle')
    ax[0].plot(df.index, df['bb_lower'], label = 'bb_lower')

    ax[0].scatter(df.index, df['open_trade'], label = 'open_trade')
    ax[0].scatter(df.index, df['stop_loss'], label = 'stop_loss')
    ax[0].scatter(df.index, df['take_profit'], label = 'take_profit')
    ax[0].scatter(df.index, df['close_trade'], label = 'close_trade')

    ax[0].legend()

    ax[1].plot(df.index, df['capital'])
    plt.yscale("log")

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()


# single_tf_test("../live/future_bars1hr.csv", 1.8, 10, 0.8)
single_tf_test("../.datasets/Binance_BTCUSDT_1h_fix.csv", 1.8, 10, 0.8)
