import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time

from pandas.core.indexing import need_slice
print(time.time())

btc = pd.read_csv("../.datasets/btc_minute.csv")[::-1] # import rows
btc = btc[::-1] # reorder the dataframe following the correct sequence of time
print(btc['open'])

btc_1 = pd.read_csv("../.datasets/bitstamp_hourly.csv")[::-1] # import rows


def growth_tracker(input, interval):
    """input is an array"""

    X1 = np.array(input[:-interval])
    X2 = np.array(input[interval:])
    X3 = (X2 - X1) / X1
    return X3

pchange = growth_tracker(btc_1['open'], 24 * 30)

print(len(pchange))
print(pchange)

plt.plot(pchange)
plt.show()

"""TODO
CHART MULTIPLE SECURITIES
DERIVE ACTIONABLE STRATEGY OR BUILD FRONT END CHART
FRONT END MATRIX
- Top 10 securities
- 1hr, 1 day, 7 days, 30 days"""
