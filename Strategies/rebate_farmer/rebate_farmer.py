import connect
import threading

"""
Provides liquidity to a price range through the use of limit orders
"""

class order_node(object):
    """Represents a divisible unit of liquidity provision"""
    def __init__(self, position, price):
        self.node_up = None # node above this one
        self.node_down = None # node below this one
        self.entry_price = price # entry price
        self.order_id = None # id of active trade
        self.position = -1 # held position, updated actively on execution, set negative initially to open a long
        self.force_place(position)

    def force_place(self, order_size):
        print("force place")
        print(self.__dict__)
        offset = -1 # set long pos default offset
        new_position = order_size
        if self.position > 0: # now short
            new_position *= -1
            offset = 1
        try:                                                    # try
            self.order_id = connect.limit(new_position, self.entry_price) # to place order normally
        except:                                                 # else
            while True:                                         # force execution
                print("loop exec")
                try:
                    self.order_id = connect.limit(new_position, connect.price + offset)
                    break
                except:
                    continue
        self.position = new_position


class deque(object):
    """Slave to the farmer"""
    def __init__(self, ):
        self.head = None # the order on top of the queue (closest to price)
        self.tail = None # the order on bottom of the queue (furthest to price)
        self.lengthening = False # mutex variable true while lengthening head of snake
        self.balancing = False # mutex variable true while order replacement is underway

    def create_head(self, order_size, order_price):
        """Create a new head, call on instantiation of object to start building order list"""
        print('create head')
        self.head = self.tail = order_node(order_size, order_price) # set the head and tail to a new order_node object

    def update_head(self, order_size, new_head_price):
        """Update the head"""
        print('update head')
        if not self.head:
            self.create_head(order_size, new_head_price)
        else:
            self.head.node_up = order_node(order_size, new_head_price) # set current head's node_up link to the new head
            self.head.node_up.node_down = self.head # set the new head's node_down link to the old head
            self.head = self.head.node_up # set deque's head to new head

    def shorten_tail(self):
        """Cut off the tail off the beast"""
        connect.cancel_active(self.tail.order_id) # cancel the tail's entry order
        self.tail = self.tail.node_up # move the tail of the deque up
        self.tail.node_down = None # deallocation pointer and mark for garbage collection?
        print("TAIL SHORTENED")

    def shift_up(self, order_size, new_head_price):
        """Moves the head of the tail order snake upwards"""
        self.update_head(order_size, new_head_price)
        self.shorten_tail()
        self.lengthening = False # set the var to show lengthening is complete

    def check_all_orders(self, order_size):
        """Check active orders for corresponding local order"""
        active_orders = connect.query_orders() # get a list of all the active orders on the server
        print(active_orders)
        current_node = self.tail # set current node to tail
        while current_node.node_up: # break loop when we get to head
            print('current node order id', current_node.order_id)
            if current_node.order_id not in active_orders: # the order has been filled
                print('order not matched: ', current_node.order_id)
                current_node.force_place(order_size)
            current_node = current_node.node_up
        if self.head.order_id not in active_orders: # run it for the head which is missed by the break condition
            print('head order id', self.head.order_id)
            self.head.force_place(order_size)
        self.balancing = False


def rebate_farmer(range_percent, divisions, account_size_multiple):
    """Runs the show"""
    while not connect.price: # wait for the first price to be sent through
        continue
    order_manager = deque()                                     # create the order list object
    balance_usd = connect.wallet_balance() * connect.price      # get wallet balance
    order_size = (balance_usd*account_size_multiple)/divisions  # calculate order size
    range_ = connect.price * range_percent                      # evaluate the range

    for i in range(round(connect.price - range_), round(connect.price), round(range_/divisions)):
        try:
            order_manager.update_head(order_size, i) # build up the order from the bottom
        except:
            print('some order failed so just roll with what we got')

    saved_execution = None
    while True:
        """Wait for executions to trigger replace and move the head upwards"""
        if not order_manager.balancing and connect.execution != saved_execution: # some order executedand rebalancing is not in progress
            saved_execution = connect.execution # reset the stored value
            x = threading.Thread(target = order_manager.check_all_orders, args = (order_size, ))
            x.start() # start thread
            order_manager.balancing = True
        
        if not order_manager.lengthening and connect.price > round(order_manager.head.entry_price * (1 + (range_percent/divisions))): # check to place the next order up
            y = threading.Thread(target = order_manager.shift_up, args = (order_size, round(order_manager.head.entry_price * (1 + (range_percent/divisions)))))
            y.start() #start thread
            order_manager.lengthening = True # set the var to indicate that a lengthening is in progress



rebate_farmer(0.025, 64, 2)
