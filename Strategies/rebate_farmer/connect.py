from pybit import HTTP, WebSocket
import time
from datetime import datetime
from threading import *
import json
import logging

logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.DEBUG,
    filename='rebate_farmer.log')

#Shlockton3
api_key = "BYMGAERIDGBYNIGBWG"
api_secret = "RZGZYJAWKWWDAMPZTWXWBRXDRCUXIPJZSEXB"
symbol = "BTCUSD"

session = HTTP(
    endpoint='https://api.bybit.com',
    api_key=api_key,
    api_secret=api_secret
)

ws = WebSocket(
    endpoint='wss://stream.bybit.com/realtime', 
    subscriptions=['execution', 'position', 'instrument_info.100ms.'+symbol], 
    api_key=api_key,
    api_secret=api_secret
)

class bybit_ws(Thread):
    def run(self):
        global price
        price = 0
        global execution
        execution = None
        global position
        position = None
        global open_trades
        open_trades = {}
        
        while True:
            pricedata = ws.fetch('instrument_info.100ms.'+symbol)
            if pricedata:
                tickprice = pricedata['last_price_e4']/10000
                if tickprice != price:
                    price = tickprice
                    print(price)
            
            executiondata = ws.fetch('execution')
            if executiondata:
                tickexe = executiondata
                if tickexe != execution:
                    execution = tickexe
                    print('execution', json.dumps(execution, indent=4, sort_keys=True))

            positiondata = ws.fetch('position')
            if positiondata:
                tickpos = positiondata
                if tickpos != position:
                    position = tickpos
                    print('position\n', json.dumps(position, indent=4, sort_keys=True))

bybit_ws().start()


def limit_maker(pos):
    """Tell it the size of the position that you want and it handles the rest
    Ensures a maker rebate of 0.025% by replacing order until filled
    Returns the price"""

    limit = {
    'symbol': symbol, 
    'order_type': 'Limit', 
    'side': None, 
    'qty': None, 
    'price': None, 
    'time_in_force': 'PostOnly'} # this ensures rebate, otherwise the exchange engine is fuc ked

    while True:
        savedprice = price # global var updated by websocket
        qty = pos
        if qty > 0:
            limit['side'] = 'Buy'
            limit['qty'] = qty
            limit['price'] = savedprice - 0.5
        if qty < 0:
            limit['side'] = 'Sell'
            limit['qty'] = -qty 
            limit['price'] = savedprice + 0.5

        order = session.place_active_order(**limit)
        order_id = order['result']['order_id']
        order_query = session.query_active_order(**{'symbol': symbol, 'order_id': order_id})

        if order_query['result']['order_status'] == 'New':
            print('order confirmed')
            while True:
                # it should probably confirm cancellation
                if limit['side'] == 'Buy':
                    if price > savedprice:
                        try:
                            session.cancel_active_order(**{'symbol': symbol, 'order_id': order_id})
                        except:
                            print('cancellation failed, not good')
                        break
                if limit['side'] == 'Sell':
                    if price < savedprice:
                        try:
                            session.cancel_active_order(**{'symbol': symbol, 'order_id': order_id})
                        except:
                            print('cancellation failed, not good')
                        break
                try:
                    if execution[0]['order_id'] == order_id:
                        print('~~~ORDER FILLED~~~')
                        return price
                except:
                    pass


def limit(pos, trigger_price):
    """takes a price and position and return the trade id if successfully placed otherwise error"""
    limit = {
    'symbol': symbol, 
    'order_type': 'Limit', 
    'side': None, 
    'qty': abs(pos), 
    'price': trigger_price,
    'time_in_force': 'PostOnly'
    }

    if pos > 0: #long
        limit['side'] = "Buy"
        order = session.place_active_order(**limit)
              
    if pos < 0: #short
        limit['side'] = "Sell"
        order = session.place_active_order(**limit)

    print('limit order', order)
    order_id = order['result']['order_id'] # query active order
    order_query = session.query_active_order(**{'symbol': symbol, 'order_id': order_id})
    print('order query', order_query)
    if order_query['result']['order_status'] == 'New':
        print('order confirmed')
        return order_id
    else:
        print('order failed')
        raise Exception('limit order failed')


def wallet_balance(): 
    """returns wallet balance in BTC"""
    balance = session.get_wallet_balance()
    print(json.dumps(balance['result']['BTC'], indent=4, sort_keys=True))
    return balance['result']['BTC']['equity']


def market(pos):
    """Market buys to set position to pos"""
    market = {
    'symbol': symbol, 
    'order_type': 'Market', 
    'side': None, 
    'qty': abs(pos), 
    'price': price,
    'time_in_force': 'PostOnly'
    }

    pos_info = session.my_position(symbol=symbol)['result'][0]['data']
    cur_pos = pos_info['size']

    if pos_info['side'] == 'Sell':
        cur_pos = -pos_info['size']

    order = pos-cur_pos # calculate the difference
    if order == 0:
        return 'current position is already ' + str(pos)

    if order > 0:
        market['side'] = 'Buy'
    if order < 0:
        market['side'] = 'Sell'

    market['qty'] = abs(order)
    return session.place_active_order(**market)


def cancel_active(order_id):
    """Cancels active order given an order id"""
    cancel = {
        'symbol': symbol,
        'order_id': order_id
        }
    try:
        logging.info(f"cancellation request\n {session.cancel_active_order(**cancel)}")
    except:
        logging.info("cancellation was unsuccesful")


def query_orders():
    """Returns a list of active order ids"""
    response = session.query_active_order(**{'symbol': symbol})
    limit_orders = []
    for item in response['result']:
        limit_orders.append(item['order_id'])

    return limit_orders


def cancel_all():
    cancel_all = {
    'symbol': symbol
    }

    print(session.cancel_all_active_orders(**cancel_all))
    print(session.cancel_all_conditional_orders(**cancel_all))

"""Make orders and test code underneath this line"""
cancel_all()
# limit_maker(0)
# if __name__ == "__main__":
#     while not price:
#         continue

#     while True:
#         limit_maker(10)
#         limit(-10, price+3)
#         time.sleep(5)
