#Samuel Lockton 2021

"""The framework used to execute the Double Top Double Bottom Strategy"""

# Import mathematical packages
from os import linesep
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema


class trade(object):
    """Object representing a trade"""
    def __init__(self, entry, stoploss, takeprofit, trade_type):
        self.entry = entry
        self.stoploss = stoploss
        self.takeprofit = takeprofit
        self.trade_type = trade_type
        self.outcome = False


class retrace_closeclose(object):
    """Object containing conditions to open a trade
    close as in small margin and close and in shut"""
    def __init__(self, point, point_type, retrace_threshold, close_sens):
        self.retraced = False # conditional represents if price has exceeded retrace_threshold
        self.point = point # price value
        self.point_type = point_type # min or max
        self.retrace_threshold = retrace_threshold # percentage price must move from point to satisfy retracement condition
        self.retracement_percent = 0 # percentage the price has moved from point
        self.close_sens = close_sens # percentage of retracement with which price needs to close within to open trade

        """So how does this work? It has its conditions define statically upon creation
        point is created from the peak finding algorithm
        point_type same
        retrace threshold is a static parameter defined on creation
        retrace_percent is updated dynamically
        close sens is static on creation
        """

    def price_routine(self, price):
        self.calc_retracement(price)
        if not self.retraced:
            self.check_retracement_threshold()
        if self.retraced and self.closeclose(price):
            if self.point_type == 'min':
                self.trade = trade(price, float(price - price * (self.retracement_percent*self.sl_val)), float(price + price * self.retracement_percent*self.tp_val), "Buy")
            if self.point_type == 'max':
                self.trade = trade(price, float(price + price * (self.retracement_percent*self.sl_val)), float(price - price * self.retracement_percent*self.tp_val), "Sell")
        else:
            self.age += 1
            self.invalidation(price)

    def invalidation(self, price):
        if self.point_type == 'max' and price > self.point:
            self.valid = False
        if self.point_type == 'min' and price < self.point:
            self.valid = False

    def calc_retracement(self, price):
        if self.point_type == 'max':
            self.retracement_percent = max(1 - (price / self.point), self.retracement_percent)
        if self.point_type == 'min':
            self.retracement_percent = max(1 - (self.point / price), self.retracement_percent)
    
    def check_retracement_threshold(self):
        if self.retracement_percent > self.retrace_threshold:
            self.retraced = True
    
    def closeclose(self, price):
        range_size = self.point * self.retracement_percent * self.close_sens
        if price > self.point - range_size and price < self.point + range_size:
            return True
        return False


class dubtop(object):
    """Object Representing Double Top, Double Bottom strategy. Takes price and finds min and max points.
    Returns retrace_closeclose objects for each of these points"""
    def __init__(self, retrace_percent, close_sens, pointsens, sl_val, tp_val):
        self.retrace_percent = retrace_percent # how far the price must retrace to be valid
        self.close_sens = close_sens # sensitivity of second close
        self.datawindow = []
        self.pointsens = pointsens # sensitivity of finding points
        self.sl_val = sl_val # stop loss at this percent of retracement
        self.tp_val = tp_val # take profit at this percent of retracement

    def newprice(self, price):
        if len(self.datawindow) >= 2*self.pointsens:
            self.datawindow.pop(0)
            self.datawindow.append(price)
        else:
            self.datawindow.append(price)
        
        df = pd.DataFrame(self.datawindow, columns=['data'])

        if len(df)==2*self.pointsens:
            df['min'] = df.iloc[argrelextrema(df.data.values, np.less_equal,
                            order=self.pointsens)[0]]['data']
            df['max'] = df.iloc[argrelextrema(df.data.values, np.greater_equal,
                            order=self.pointsens)[0]]['data']
        
        if len(df) == 2*self.pointsens:
            if not pd.isna(df.iloc[self.pointsens]['min']):
                return retrace_closeclose(df.iloc[self.pointsens]['min'], 'min', self.retrace_percent, self.close_sens, self.sl_val, self.tp_val)
            if not pd.isna(df.iloc[self.pointsens]['max']):
                return retrace_closeclose(df.iloc[self.pointsens]['max'], 'max', self.retrace_percent, self.close_sens, self.sl_val, self.tp_val)
        
        return False