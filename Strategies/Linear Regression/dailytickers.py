#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

import requests 
import json 
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import time

def get_bybit_bars(symbol, interval, startTime, endTime):
 
    url = "https://api.bybit.com/v2/public/kline/list"
 
    startTime = str(int(startTime.timestamp()))
    endTime   = str(int(endTime.timestamp()))
 
    req_params = {"symbol" : symbol, 'interval' : interval, 'from' : startTime, 'to' : endTime}

    df = pd.DataFrame(json.loads(requests.get(url, params = req_params).text)['result'])
 
    if (len(df.index) == 0):
        return None
     
    df.index = [dt.datetime.fromtimestamp(x) for x in df.open_time]
 
    return df

def get(ticker, time, start_prev):
    timecode = time
    if time == 1440:
        timecode = "D"
    start = dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=start_prev)
    df_list = []
    while True:
        print(start)
        new_df = get_bybit_bars(ticker, timecode, start, start+dt.timedelta(minutes=200*time))
        if new_df is None:
            break
        df_list.append(new_df)
        start += dt.timedelta(minutes=200*time)

    df = pd.concat(df_list)
    return df['close']


df = pd.DataFrame()
tickers = ["BTCUSD", "ETHUSD"] # specify here

for ticker in tickers:
    df[ticker] = get(ticker, 60, 900)

df.to_csv("dailytickers.csv")