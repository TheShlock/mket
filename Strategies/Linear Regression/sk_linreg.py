import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import sklearn.model_selection as model_selection
import warnings
warnings.filterwarnings('ignore') 

"""Read Data"""
df = pd.read_csv("dailytickers.csv")

X = df[['BTCUSD']]
Y = df.ETHUSD

plt.plot(df.ETHUSD/df.BTCUSD. df.index)
plt.show()

def lin_reg(X, Y):
    """Split data"""
    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, train_size=0.80, shuffle = True)
    # print(X_train)
    # print(y_train)

    """Create and Train the model"""
    model = LinearRegression(fit_intercept=True)
    model.fit(X_train, Y_train)

    """Print Useful outputs"""
    print("coefficients\n", model.coef_)
    print("r2", model.score(X_train, Y_train))
    print("MSE", mean_squared_error(Y_train, model.predict(X_train)))
    print("r2", model.score(X_test, Y_test))
    print("MSE", mean_squared_error(Y_test, model.predict(X_test)))

lin_reg(X, Y)





# """Multiple Linear Regression"""
# X = df[['lcavol', 'lweight', 'age', 'lbph', 'svi', 'lcp', 'gleason', 'pgg45']]
# Y = df.lpsa

# """Single Linear Regression"""
# X = df[['lcavol']]
# Y = df.lpsa





# """Add a column for predicted values"""
# df['predicted'] = 0.

# """Predict lpsa values in test set"""
# for i, row in df.iterrows():
#     df.at[i, 'predicted'] = model.predict([row[1:-3]])[0]

# """Write results to csv"""
# df.to_csv('df_test.csv')

# """Calculate mean squared error"""
# print("MSE", mean_squared_error(X, df.loc[:, 'predicted'].values))
