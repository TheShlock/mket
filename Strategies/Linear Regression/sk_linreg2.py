import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import math
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import sklearn.model_selection as model_selection
import warnings
warnings.filterwarnings('ignore') 


def lin_reg(X, Y):
    """Split data"""
    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, train_size=0.80, shuffle = False)

    """Create and Train the model"""
    model = LinearRegression(fit_intercept=True)
    model.fit(X_train, Y_train)

    """Print Useful outputs"""
    print("coefficients", model.coef_)
    print("intercept", model.intercept_)
    print("r2", model.score(X_train, Y_train))
    print("RMSE", math.sqrt(mean_squared_error(Y_train, model.predict(X_train))))
    print("r2", model.score(X_test, Y_test))
    print("RMSE", math.sqrt(mean_squared_error(Y_test, model.predict(X_test))))

    plt.plot([*Y_test], label = 'actual')
    plt.plot(model.predict(X_test), label = 'predicted')
    plt.legend()
    plt.show()

    return model

def slices(X,Y,s):
    l = round(len(X)/s)
    for i in range(s):
        lin_reg(X[i*l:(i+1)*l], Y[i*l:(i+1)*l])

"""Read Data"""
df = pd.read_csv("dailytickers.csv")

X = df[['ETHUSD']]
Y = df.BTCUSD

# train_and_plot(X,Y)
slices(X,Y,40)

