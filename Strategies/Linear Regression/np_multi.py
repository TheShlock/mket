import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

"""Wrap this whole thing in a function that accepts the train and test datasets pre split
and the names of the features in a list as well as the target
def lin_regr(train, test, features, target)"""

df_train = pd.read_csv("q4data.txt", sep = "\t")[:66]
df_test = pd.read_csv("q4data.txt", sep = "\t")[66:]

# load the data into numpy arrays
X = df_train.values # 2d

df_train['ones'] = 1. # this acts as the bias
Y = df_train['lpsa'] # target
X = df_train[['lcavol', 'lweight', 'age', 'lbph', 'svi', 'lcp', 'gleason', 'pgg45', 'ones']] # features

def solve(X, Y): # r squared error function
    """w is a vector with the biases for the model"""
    w = np.linalg.solve( X.T.dot(X), X.T.dot(Y))
    print("the weights are \n", w)

    Yhat = X.dot(w)

    # determine how good the model is by computing the r-squared
    d1 = Y - Yhat
    d2 = Y - Y.mean()
    r2 = 1 - d1.dot(d1) / d2.dot(d2)
    print("r-squared", r2)
    return w[:-1] # remove the bias term (not sure if this is normal)


"""Get the model weights"""
weights = np.array([[*solve(X, Y)]])

"""Predict the test values"""
df_test['predicted'] = weights.dot(df_test[['lcavol', 'lweight', 'age', 'lbph', 'svi', 'lcp', 'gleason', 'pgg45']].T).T

"""Write the predicted test values to csv"""
df_test.to_csv("df_test_numpy.csv")

"""Calculate the MSE for the test values"""
MSE = np.square(np.array(df_test[['lpsa']]).T - np.array(df_test[['predicted']].T)).mean()
print("MSE of df_test is",MSE)

# print(mse)

