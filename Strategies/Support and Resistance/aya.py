"""
Create scoring for reversals
    Define reversals

Storing the scores
    An ordered dict

Define invalidations
    pass throughs and their effect on the relative score

Devise error function
    price passing through is bad
    price reversing is good

    price randomly moves around
    the null hypothesis is that support/resistance has no effect on the movements
    therefore we should see the distribution of price movement near levels to break through the level

    will we see a reversal?
    what is the size of the move after the reversal?!?nahhh>?

"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import talib as ta
from scipy.signal import argrelextrema

df = pd.read_csv("../stats/bitstamp_hourly.csv")[40000:]

min = min(df.close)
print(min)
max = max(df.close)
print(max)

"""Set up the scores object to store scores"""
# conventionally when adding values round them to the nearest 10
# we are not going with a band model for ease of use and not having to worry about rounding
scores = {}
step = 10
spread = 10

for i in range(int(round(min-(step/2)-(step*spread), -1)), int(round(max+(step/2)+(step*spread), -1)), 10):
    scores[i] = 0

"""Find minima and maxima"""
n = 5  # number of points to be checked before and after
df['min'] = df.iloc[argrelextrema(np.array(df.close), np.less_equal,
                    order=n)]['close']
df['max'] = df.iloc[argrelextrema(np.array(df.close), np.greater_equal,
                    order=n)]['close']


"""Fill in the scores based on minima and maxima
Different modes that are worth testing is the bleed into surrounding steps
Will be a little easier to figure this one out with an error function
For now do it with loops"""

def scores_func(point):
    for i in range(-9, 9):
        scores[point+(i*step)] += score_per - abs(i)

score_per = 10

for key, val in df.iterrows():
    if not pd.isnull(val['min']):
        scores_func(round(val['min'], -1))
        # scores[round(val['min'], -1)] += score_per

    if not pd.isnull(val['max']):
        scores_func(round(val['max'], -1))
        # scores[round(val['max'], -1)] += score_per

# final task before done for today: show resistance levels as a histogram
print(np.array(scores))
print(scores.keys())
# exit()
plt.plot(scores.keys(), scores.values())
plt.show()


# plt.plot(df.close)
# plt.scatter(df.index, df['min'])
# plt.scatter(df.index, df['max'])
# plt.show()

exit()