# Support and Resistance levels

## What?

Support and Resistance levels are defined by certain prices at which elements of randomness are removed from the market

## Why?
Humans connect emotionally to certain prices
They look at a chart and decide if levels are dropped through then they will take some action in the market
They also like round numbers
They also have emotional attachment to certain prices

## How?

By buying on support and selling on resistance
Buy low Sell high


Levels are defined by closes
Closes are defined by timeframes
timeframes are defined by humans

Efficiency of the strategy is defined by profit

Lets say you have some levels defined

A level at any given time can be support or resistance
It can be both on different time frames

Strategy gives higher time frames precedence over lower ones

Regardless of timeframe, the price at which a level occurs is consistent

On any given timeframe, if the previous close occured at below a level it is considered resistance

If the previous close occurred above a level it is considered support
## Results

## Conclusion
