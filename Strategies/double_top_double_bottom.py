#Samuel Lockton 2021

"""The framework used to execute the Double Top Double Bottom Strategy"""

import trade


def pct_change(X1, X2):
    return (X2 - X1)/X1

class trade(object):
    """Object representing a trade"""
    def __init__(self, entry, stoploss, takeprofit, trade_type):
        self.entry = entry
        self.stoploss = stoploss
        self.takeprofit = takeprofit
        self.trade_type = trade_type
        self.outcome = False


class reversal(object):
    """Object containing instance of a reversal"""
    def __init__(self, point, point_type, close_sens):
        self.point = point # price value
        self.point_type = point_type # min or max
        self.close_sens = close_sens # percentage of retracement with which price needs to close within to open trade

        """So how does this work? It has its conditions define statically upon creation
        point is created from the peak finding algorithm
        point_type same
        retrace threshold is a static parameter defined on creation
        retrace_percent is updated dynamically
        close sens is static on creation
        """

    def check(self, price):
        """Checks if price has retraced far enough to validate trade"""
        
        self.calc_retracement(price)
        if self.closeclose(price):
            if self.point_type == "min":
                self.trade = trade(price, float(price - price * (self.retracement_percent*self.sl_val)), float(price + price * self.retracement_percent*self.tp_val), "Buy")
                return True
            if self.point_type == "max":
                self.trade = trade(price, float(price + price * (self.retracement_percent*self.sl_val)), float(price - price * self.retracement_percent*self.tp_val), "Sell")
                return True
        else:
            self.age += 1
            self.invalidation(price)
            return False

    def invalidation(self, price):
        if self.point_type == "max" and price > self.point:
            self.valid = False
        if self.point_type == "min" and price < self.point:
            self.valid = False
    
    def check_retracement_threshold(self):
        """Checks if price has exceeded the retracement threshold"""
        if self.retracement_percent > self.retrace_threshold:
            self.retraced = True
    
    def closeclose(self, price):
        range_size = self.point * self.retracement_percent * self.close_sens
        if price > self.point - range_size and price < self.point + range_size:
            return True
        return False


class price_buffer(object):
    """Circular List of size 3 to check points"""
    def __init__(self):
        self.list = [None] * 3

    def add_item(self, price):
        self.list.insert(0,price)
        self.list.pop(-1)

    def check_for_reversal(self):
        if self.list[-1] == None: # wait til the buffer is full
            return None
        if self.list[1] > self.list[0] and self.list[1] > self.list[2]:
            return "max" # previous price is a maxima
        if self.list[1] < self.list[0] and self.list[1] < self.list[2]:
            return "min" # previous price is a minima
        else:
            return None

class double_top_double_bottom(object):
    """Object Representing Double Top, Double Bottom strategy. 
    Takes candles and:
    - Finds reversal points
    - Checks reversal points for trades
    """
    def __init__(self, close_sens):
        self.close_sens = close_sens # proximity to initial reversal to initiate trade
        # self.sl_val = sl_val # stop loss at this percent of retracement
        # self.tp_val = tp_val # take profit at this percent of retracement
        self.price_buffer = [None] * 3 # holds and checks prices for reversals
        self.reversals = []

    def add_item_to_price_buffer(self, price):
        self.price_buffer.insert(0, price)
        self.price_buffer.pop(-1)

    def check_for_reversals(self, price):
        """Scan the candle buffer for minima and maxima"""
        if self.price_buffer[-1] == None:
            return
        if self.price_buffer[1] > self.price_buffer[0] and self.price_buffer[1] > self.price_buffer[2]: # . ' .
            self.reversals.append(reversal(price, "max", self.close_sens))
        if self.price_buffer[1] < self.price_buffer[0] and self.price_buffer[1] < self.price_buffer[2]: # ' . '
            self.reversals.append(reversal(price, "min", self.close_sens))

    def feed_candle(self, price):
        self.add_item_to_price_buffer(price)
        self.check_for_reversals(price)
        self.check_reversals_for_trades(price)
        
    def check_reversals_for_trades(self, price):
        for reversal in list(self.reversals):
            if pct_change(reversal.point, price) < self.close_sens: # price closed close to reversal point
                return trade()
                print("open trade")




d = double_top_double_bottom(0.01)
d.feed_candle(9)
print(d.price_buffer)
print(d.reversals)
d.feed_candle(10)
print(d.price_buffer)
print(d.reversals)
d.feed_candle(9)


