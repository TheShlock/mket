#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

#Project files
import connect


# def condense(period):
#     """condenses minute data into minute*period data
#     which can then be run by runTest"""
#     df = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
#     df = df[::-1].reset_index() # reverse the rows and set the index
#     condensed = []
#     count = period-1
#     for row in df.itertuples():
#         count += 1
#         if count == period:
#             count=0
#             condensed.append(row.close)
#     df = pd.DataFrame(condensed, columns=['close'])
#     return df


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0
        self.won_trades = 0
        self.lost_trades = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = ((price-self.price)/self.price)
            d_cap = p_change*self.position # change in capital
            if d_cap > 0:
                self.won_trades += 1
            if d_cap < 0:
                self.lost_trades += 1
            self.price = price
            self.position = position
            return d_cap
        return 0


window = 100 # how many values do we need to get all of our indicators live?

def sim():
    while True:
        length = len(pd.read_csv("../.datasets/BTCUSD_live.csv"))
        while True:
            btc = pd.read_csv("../.datasets/BTCUSD_live.csv")
            if len(btc) > length:
                break
        btc = btc.iloc[-window:]

        btc['2SMA'] = ta.SMA(btc.close, timeperiod=2)
        btc['3SMA'] = ta.SMA(btc.close, timeperiod=3)
        btc['5SMA'] = ta.SMA(btc.close, timeperiod=5)
        btc['8SMA'] = ta.SMA(btc.close, timeperiod=8)
        btc['13SMA'] = ta.SMA(btc.close, timeperiod=13)
        print(btc.info())
        t = trade()
        capital = 1000
        drawdown = capital
        for row in btc.itertuples():
            pos = 0
            if row[10]>row[9]:
                pos += capital
            else:
                pos -= capital

            capital += t.c_pos(row[5], pos)
            print('capital: ', capital)
        print('drawdown: ', drawdown)
        print(t.__dict__)


sim()

# fibs = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
