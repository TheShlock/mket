#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

"""This code calculates the daily percentage return for strategies"""

def eval(tf):
    df = pd.read_csv('tf'+str(tf)+'.csv')
    daily_counter = 0
    print(df)
    print(df.head())

    s1c = 1000
    s2c = 1000
    s3c = 1000

    for k, row in df.iterrows():
        if daily_counter > 1440 / tf:
            with open('eval'+str(tf)+'.csv', 'a') as f:
                f.write(str((row[0]-s1c)/s1c)+','+str((row[1]-s2c)/s2c)+','+str((row[2]-s3c)/s3c)+'\n')
            s1c = row[0]
            s2c = row[1]
            s3c = row[2]

            daily_counter = 0
        daily_counter += 1

p = [1,3,5,10,15,30,60,240,1440,10080]
for i in range(len(p)):
    eval(p[i])
        
