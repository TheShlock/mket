from pybit import HTTP, WebSocket
import time
from threading import *

api_key = "mHCTSZeiN6H5pESU2Q"
api_secret = "wu0Dy1M7QHNSZARONySCWwE3yjuHLE827rYW"

session = HTTP(
    endpoint='https://api.bybit.com',
    api_key=api_key,
    api_secret=api_secret
)

ws = WebSocket(
    endpoint='wss://stream.bybit.com/realtime', 
    subscriptions=['execution', 'position', 'instrument_info.100ms.BTCUSD'], 
    api_key=api_key,
    api_secret=api_secret
)

class bybit_ws(Thread):
    def run(self):
        global price
        price = 0
        global execution
        execution = None
        global position
        position = None
        
        while True:
            pricedata = ws.fetch('instrument_info.100ms.BTCUSD')
            if pricedata:
                tickprice = pricedata['last_price_e4']/10000
                if tickprice != price:
                    price = tickprice
                    print(price)
            
            executiondata = ws.fetch('execution')
            if executiondata:
                tickexe = executiondata
                if tickexe != execution:
                    execution = tickexe
                    print('execution', execution)

            positiondata = ws.fetch('position')
            if positiondata:
                tickpos = positiondata
                if tickpos != position:
                    position = tickpos
                    print('position:', position)
bybit_ws().start()

def limit_maker(pos):
    """Tell it the size of the position that you want and it handles the rest
    Returns the position"""

    limit = {
    'symbol': 'BTCUSD', 
    'order_type': 'Limit', 
    'side': None, 
    'qty': None, 
    'price': None,
    'time_in_force': 'PostOnly'}

    #returns only the position, should error with unexpected outcomes control is key
    #test the new routine
    while True:
        my_position = session.my_position(**{'symbol': 'BTCUSD'})
        print('my position', my_position)
        print('side', my_position['result']['side'], 'size', my_position['result']['size'])

        if my_position['result']['side'] == 'Sell':
            cur_position = -my_position['result']['size']
        elif my_position['result']['side'] == 'Buy':
            cur_position = my_position['result']['size']
        else:
            cur_position = 0

        if cur_position == pos:
            return pos # this should be the only return

        savedprice = price
        qty = pos - cur_position
        if qty > 0:
            limit['side'] = 'Buy'
            limit['qty'] = qty
            limit['price'] = savedprice - 0.5
        if qty < 0:
            limit['side'] = 'Sell'
            limit['qty'] = -qty 
            limit['price'] = savedprice + 0.5

        order = session.place_active_order(**limit)
        order_id = order['result']['order_id']
        print('ORDER', order)
        order_query = session.query_active_order(**{'symbol': 'BTCUSD', 'order_id': order_id})
        print('ORDER_QUERY', order_query)

        if order_query['result']['order_status'] == 'New':
            print('order confirmed')
            exec_save = execution
            while True:
                if limit['side'] == 'Buy':
                    if price >= savedprice + 0.5:
                        try:
                            session.cancel_active_order(**{'symbol': 'BTCUSD', 'order_id': order_id})
                        except:
                            pass
                        break
                if limit['side'] == 'Sell':
                    if price <= savedprice - 0.5:
                        try:
                            session.cancel_active_order(**{'symbol': 'BTCUSD', 'order_id': order_id})
                        except:
                            pass
                        break
                try:
                    if execution[0]['order_id'] == order_id:
                        print('~~~ORDER FILLED~~~')
                        break
                except:
                    pass


while not price:
    continue
print('limit maker return: '+ str(limit_maker(1)))
print('limit maker return: '+ str(limit_maker(0)))
# print('limit maker return: '+ str(limit_maker(-1)))


#conditional
conditional = [{
    'symbol': 'BTCUSD', 
    'order_type': 'Market', 
    'side': 'Sell', 
    'qty': 1, 
    'base_price': 32500, #set to current price
    'stop_px': 30000, #trigger price
    'time_in_force': 'PostOnly',
    'close_on_trigger': True
}]
# print(session.place_conditional_order_bulk(conditional))

#order query
order_query = {
    'symbol': 'BTCUSD',
    'order_id': '6eeb17da-0fd1-4cd4-9b64-c2d7065c1436'
}
# print(session.query_active_order(**order_query))

#cancel order
cancel = {
    'symbol': 'BTCUSD',
    'order_id': '18b202ed-4e46-4b35-a86c-1515df47bf63'
}
# print(session.cancel_active_order(**cancel))

#cancel all active orders
cancel_all = {
    'symbol': 'BTCUSD'
}
# print(session.cancel_all_active_orders(**cancel_all))
# print(session.cancel_all_conditional_orders(**cancel_all))

# Check on your order and position through WebSocket.
# print(ws.fetch('order'))
# print(ws.fetch('position'))
