#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

#Project files
import logicnode

"""This code tests moving average cross strategies and outputs the results to a graph and to a csv file"""
def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append(row.close)
    df = pd.DataFrame(condensed, columns=['close'])
    return df


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = (price-self.price)/self.price # percentage change in price
            d_cap = p_change*self.position # change in capital
            self.price = price # reset price
            self.position = position # reset position
            return d_cap
        return 0

class strategy(object):
    def __init__(self, name, input, func):
        self.name = name
        self.input = input
        self.obj = func
        self.trade_obj = trade()
        self.call = None
        self.capital = 1000
        self.max_cap = self.capital
        self.max_p_lost = [0]

    def feed(self, input):
        if self.capital < 0:
            self.capital = 1
        call = self.obj(input[1:]) #input[0] always price
        shift = None
        if not self.call:
            shift = True
        if self.call == 'long' and call == 'short':
            shift = True
        if self.call == 'short' and call == 'long':
            shift = True
        if shift:
            if self.call == 'long':
                self.capital += self.trade_obj.c_pos(input[0], self.capital)
            if self.call == 'short':
                self.capital += self.trade_obj.c_pos(input[0], -self.capital)

        if self.capital > self.max_cap:
            self.max_cap = self.capital

        if (self.max_cap-self.capital)/self.max_cap > self.max_p_lost[0]:
            self.max_p_lost = [(self.max_cap-self.capital)/self.max_cap, self.capital, self.max_cap] 

        if self.capital < 0:
            self.capital = 1
        self.call = call


def cross_2(input):
    if input[0] < input[1]:
        return 'long'
    else: 
        return 'short'

fibs = [2,3,5,8,13,21,34,51,89]
strategies = []
for f_1 in fibs:
    for f_2 in fibs:
        if f_1 != f_2:
            strategies.append(strategy(str(f_1)+'>'+str(f_2), [str(f_1)+'SMA', str(f_2)+'SMA'], cross_2))

for s in strategies:
    print(s.name, s.input)


def single_tf_test(tf, dataset, strategies):

    """Read the dataset"""
    df = condense(tf, pd.read_csv(dataset))

    """Add in indicators"""
    df['2SMA'] = ta.SMA(df.close, timeperiod=2)
    df['3SMA'] = ta.SMA(df.close, timeperiod=3)
    df['5SMA'] = ta.SMA(df.close, timeperiod=5)
    df['8SMA'] = ta.SMA(df.close, timeperiod=8)
    df['13SMA'] = ta.SMA(df.close, timeperiod=13)
    df['21SMA'] = ta.SMA(df.close, timeperiod=21)
    df['34SMA'] = ta.SMA(df.close, timeperiod=34)
    df['51SMA'] = ta.SMA(df.close, timeperiod=51)
    df['89SMA'] = ta.SMA(df.close, timeperiod=89)

    """Add spot for capital"""
    for s in strategies:
        df[s.name] = 0

    """Main loop"""
    t = time.time()
    for k, row in df.iterrows():
        print(k)
        for s in strategies:
            food = [row['close']]
            for i in s.input:
                food.append(row[i])
            s.feed(food)
            df.loc[k, s.name] = s.capital

    """Output section"""
    print('loop time', time.time()-t)
    
    df.to_csv('wut.csv')
    with open('ma_test.csv', 'a') as f:
        for s in strategies:
            f.write(s.name+','+str(tf)+','+dataset+','+str((1-s.max_p_lost[0])*s.capital)+','+str(s.capital)+','+str(s.max_p_lost[0])+'\n')

    """Matplotlib section"""
    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)
    plt.title(str(tf))
    ax[0].plot(df.index, df['close'], label = 'close')
    ax[0].plot(df.index, df['8SMA'], label = '8SMA')
    ax[0].plot(df.index, df['2SMA'], label = '2SMA')
    ax[0].legend()

    for s in strategies:
        ax[1].plot(df.index, df[s.name], label = s.name)
    plt.yscale("log")
    ax[1].legend()

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
   

single_tf_test(3, "../.datasets/BTCUSD_live.csv", strategies)

# fibs = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
