#Samuel Lockton 2021 ©™

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
#Project files
import str_GRMadient



class mketbot(object):
    """Manages sending price data to a strategy, trade opportunities and the opening of trades"""
    def __init__(self, capital, risk, graph, averages):
        self.capital = capital
        self.wontrades = 0
        self.losttrades = 0
        self.feespaid = 0
        self.risk = risk
        self.position = 0 # number of contracts
        self.entryPrice = None
        self.exitCondition = None
        self.risk = risk # %of capital you will lose on stoploss hit
        self.averages = averages
        self.strategy = str_GRMadient.MAnager(averages)
        self.finishedtrades = 0
        self.datapoints = 0
        self.graph = graph
        self.averages = averages
        # self.sumdMAsfibw = str_GRMadient.RMAD(20,1)
        self.pol = None
        self.price = 0

    def closePosition(self):
        if self.position:
            priceDelta = (self.price-self.entryPrice)/self.entryPrice # % change in price
            if priceDelta > 0:
                self.wontrades += 1
            else:
                self.losttrades += 1
            print('change in capital', self.position * priceDelta)
            self.capital += self.position * priceDelta # redeem position for capital
            self.position = 0  # reset position
            self.entryPrice = None # reset entry price
            self.graph[2] = self.price # close
        else:
            print('No position to close...')

    def openPosition(self, pos, exitCondition):
        self.closePosition()
        # self.capital -= abs(pos)
        self.position = pos
        self.entryPrice = self.price
        if pos > 0: #buy
            self.graph[0] = self.price
        if pos < 0: #buy
            self.graph[1] = self.price
        self.exitCondition = exitCondition

    def checkTrade(self):
        if self.graph:
            self.closePosition()


    def newprice(self, price, l):
        # self.checkTrade() # check for exit conditions, hardcoded for now
        self.price = price
        self.graph = l
        self.datapoints += 1
        self.strategy.addVals(price)
        self.graph = self.graph[0:3] + self.strategy.output(self.graph[3:]) # add all the averages
        # self.sumdMAs() # appends the value to the end of the list
        # if self.sumdMAsfibw.average:
        #     self.checkPolFlip()


    # def sumdMAs(self):
    #     sum = 0
    #     for i in range(len(self.averages)):
    #         if self.graph[3+len(self.averages)+i]:
    #             sum += self.graph[3+len(self.averages)+i] * math.sqrt(self.averages[i].period)
    #     self.sumdMAsfibw.addVal(sum)
    #     if self.sumdMAsfibw.average:
    #         self.graph.append(self.sumdMAsfibw.average)

    def checkPolFlip(self): # checks if the change in the sum of moving averages fibonnaci weighted
        if self.pol != None: # The new average will already be calculated, we are checking it against the old average
            if self.sumdMAsfibw.average >= 0 and self.pol == 'Neg':
                self.openPosition(-self.capital, None)
            if self.sumdMAsfibw.average < 0 and self.pol == 'Pos':
                self.openPosition(self.capital, None)

        #update the polarity of the new average
        if self.sumdMAsfibw.average >= 0:
            self.pol = 'Pos'
        if self.sumdMAsfibw.average < 0:
            self.pol = 'Neg'


"""Shows results on a graph"""
def run_simulation_g(points):
    # [2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811]
    averages = [987]
    bot = mketbot(1000, 0.01, True, averages)
    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    rsp = random.randrange(points, len(btc)) #random starting point
    l = []
    aves = list(map(lambda x: None, averages))
    print(aves)
    dAves = list(map(lambda x: None, averages))
    print(dAves)
    ddAves = list(map(lambda x: None, averages))
    print(ddAves)
    tradeData = [None,None,None] #Buy, Sell, Close
    for i in range(points): 
        dataRow = btc.iloc[rsp - i]
        bot.newprice(dataRow['open'], tradeData + aves + dAves + ddAves) # feed the bot candles
        l.append([dataRow['open'], dataRow['close'], dataRow['high'], dataRow['low']] + bot.graph) #add the row to the graph array

    output_dict = bot.__dict__

    del output_dict['strategy']
    del output_dict['graph']

    with open('c20_result.csv', 'a') as result:
        w = csv.DictWriter(result, output_dict.keys())
        w.writerow(output_dict)
    print(output_dict)

    print('MAS:', tuple(map(lambda x: str(x.period), averages)))
    df = pd.DataFrame(l, columns=('open', 'close', 'high', 'low', 'buy', 'sell', 'close') + tuple(map(lambda x: 'aP:'+ str(x.period), averages)) + tuple(map(lambda x: 'daP:' + str(x.period), averages)) + tuple(map(lambda x: 'ddaP:' + str(x.period), averages)))
    # df = pd.DataFrame(l, columns=('open', 'close', 'high', 'low') + tuple(map(lambda x: 'aP:'+ str(x.period), averages)) + tuple(map(lambda x: 'daP:' + str(x.period), averages)) + tuple(map(lambda x: 'ddaP:' + str(x.period), averages)))

    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)

    # fig, ax = plt.subplots(2)

    fig.suptitle("Moving Averages")



    ax[0].plot(df.index, df['high'], label = 'high')
    ax[0].plot(df.index, df['low'], label = 'low')
    ax[0].scatter(df.index, df['buy'], label = 'buy')
    ax[0].scatter(df.index, df['sell'], label = 'sell')
    # ax[0].plot(df.index, df['close'], label = 'close')

    for m in averages:
        ax[0].plot(df.index, df['aP:' + str(m.period)], label = 'aP:' + str(m.period))

    ax[0].legend()
    ax[0].grid()

    for m in averages:
        ax[1].plot(df.index, df['daP:' + str(m.period)], label = 'daP:' + str(m.period))
    # ax[1].plot(df.index, df['sumdMAs'], label = 'sumdMAs')
    ax[1].legend()
    ax[1].grid()

    for m in averages:
        ax[1].plot(df.index, df['ddaP:' + str(m.period)], label = 'ddaP:' + str(m.period))
    # ax[2].legend()
    # ax[2].grid()

    # ax.figure()
    
    plt.show()
    # width=1
    # width2=0.1
    # pricesup=df[df.close>=df.open]
    # pricesdown=df[df.close<df.open]

    # plt.bar(pricesup.index,pricesup.close-pricesup.open,width,bottom=pricesup.open,color='g')
    # plt.bar(pricesup.index,pricesup.high-pricesup.close,width2,bottom=pricesup.close,color='g')
    # plt.bar(pricesup.index,pricesup.low-pricesup.open,width2,bottom=pricesup.open,color='g')

    # plt.bar(pricesdown.index,pricesdown.close-pricesdown.open,width,bottom=pricesdown.open,color='r')
    # plt.bar(pricesdown.index,pricesdown.high-pricesdown.open,width2,bottom=pricesdown.open,color='r')
    # plt.bar(pricesdown.index,pricesdown.low-pricesdown.close,width2, bottom=pricesdown.close,color='r')

    # plt.plot(df.index, df['open'], label = 'open')
    # plt.plot(df.index, df['close'], label = 'close')

def random_simulation(params, trials):
    for i in params.__dict__.keys():
        params.__dict__[i].randomise()
    
    run_simulation_g(params, trials)



"""Command Centre"""


run_simulation_g(100000)

