#Samuel Lockton 2021 ©
import bybit
import json
import websocket
from BybitWebsocket import BybitWebsocket
from threading import *
from time import sleep, time

"""HTTPS Client"""
client  = bybit.bybit(test=True, api_key="RZQjIWm6yy8jyUTZzw", api_secret="UEmNI2A7ve8D5hagUYooZtiAiMTIrZkb84r1")

"""Websocket Client"""
ws = BybitWebsocket(wsURL="wss://stream-testnet.bybit.com/realtime", api_key="RZQjIWm6yy8jyUTZzw", api_secret="UEmNI2A7ve8D5hagUYooZtiAiMTIrZkb84r1")
ws.subscribe_instrument_info('BTCUSD')
ws.subscribe_position()
ws.subscribe_execution()
ws.subscribe_order()


ws = WebSocket(
    endpoint='wss://stream.bybit.com/realtime', 
    subscriptions=['order', 'position', 'execution'], 
    api_key=api_key,
    api_secret=api_secret
)

"""Live Price Thread
Runs concurrently, access a price by calling pricedict['ticker']
Can be extended to incorporate more data sources or configured"""
class livePrices(Thread):
    def run(self):
        global balance
        balance = None
        global pricedict
        pricedict = {}
        global positioninfo
        positioninfo = None
        global executioninfo
        executioninfo = None
        global activeExits
        activeExits = []
        while True:
            pricedata = ws.get_data("instrument_info.100ms.BTCUSD")
            if pricedata:
                try:
                    pricedict['BTCUSD'] = pricedata['update'][0]['last_price_e4']/10000
                    
                except:
                    pass

            position = ws.get_data("position")
            if position:
                try:
                    positioninfo = position[0]
                    balance = position[0]['available_balance']
                except:
                    pass

            execution = ws.get_data("execution")
            if execution:
                try:
                    executioninfo = execution
                    print('execution information:\n', execution)
                    sleep(0.01)
                    executioninfo = None
                    checkActiveExits(execution[0]['order_id'])
                except:
                    pass
livePrices().start()


def makeTrade(side, symbol, qty, stoploss, takeprofit, minEntry, maxEntry, timeLimit, tokens):
    """Function that handles the entry and exits for a trade
    side: "Buy" or "Sell"
    symbol: "BTCUSD"
    qty: size of order: str | int
    stoploss: str | int
    takeprofit: str | int
    minEntry: minimum entry price: int
    MaxEntry: maximum entry price: int
    time: number of seconds to fill order otherwise cancel: int
    tokens: limit of request tokens to consume: int"""
    
    #Set up variables
    side = side
    if side == "Buy":
        otherSide = "Sell"
        offset = -0.5
    else:
        otherSide = "Buy"
        offset = 0.5
    tokens = tokens

    orderPlaced = False
    orderFilled = False
    stopLossComplete = False
    takeProfitComplete = False

    startTime = time()
    #Main loop
    while tokens > 0:
        price = pricedict[symbol]

        #Confirm validity of entry
        if price > maxEntry or price < minEntry:
            print('price moved out of range')
        if time() - startTime > timeLimit:
            print('time limit expired')

        if not orderPlaced and not orderFilled:
            #Place entry order
            try:
                entry = client.Order.Order_new(order_type="Limit", side=side, time_in_force="PostOnly", qty=str(qty), price=price+offset, symbol=symbol).result()
                tokens -= 1
                entry_id = entry[0]['result']['order_id']
                orderQuery = client.Order.Order_query(symbol=symbol, order_id=entry_id).result()
                tokens -= 1
                if orderQuery[0]['result']['order_status'] == "New":
                    orderPlaced = True
                else:
                    print('entry not confirmed')
                    print('entry order:\n', entry)
                    print('entry order confirmation:\n', orderQuery)
                    print(client.Order.Order_cancel(symbol=symbol, order_id=entry_id).result(), '\n\n')
            except:
                print('ERROR!')
                print('entry order:\n', entry)
                print('entry order confirmation:\n', orderQuery)
                print('cautionary cancel:\n', client.Order.Order_cancel(symbol=symbol, order_id=entry_id).result(), '\n\n')


        if orderPlaced and not orderFilled:
            print('order placed succesfully, pls fill')

            #Wait for execution
            while not executioninfo:
                if time() - startTime > timeLimit: #the trade has expired
                    client.Order.Order_cancel(symbol=symbol, order_id=entry_id).result()
                    tokens -= 1
                    orderPlaced = False
                    break

                if pricedict[symbol] >= price + 0.5 or pricedict[symbol] <= price -0.5: #the price has changed
                    print('price changed, replace order')
                    client.Order.Order_cancel(symbol=symbol, order_id=entry_id).result()
                    tokens -= 1
                    orderPlaced = False
                    break

                continue

            #Confirm Fill
            if executioninfo:
                try:
                    if executioninfo[0]['order_id'] == entry_id:
                        orderFilled = True
                    else:
                        print('this is not the order you are looking for')

                except:
                    print('ERROR!')
                    print('entry order:\n', entry)
                    print('entry order confirmation:\n', orderQuery)
                    print('execution info:\n', executioninfo, '\n\n')


        if orderPlaced and orderFilled:
            print('~~~ORDER FILLED~~~')

        #Place exit orders
        if not stopLossComplete:
            try:
                stopLoss = client.Conditional.Conditional_new(order_type="Market",side=otherSide,symbol=symbol,qty=str(qty),base_price=str(price),stop_px=str(stoploss),time_in_force="GoodTillCancel",close_on_trigger=True).result()
                sl_id = stopLoss[0]['result']['stop_order_id']
                stopLossQuery = client.Order.Order_query(symbol=symbol, order_id=sl_id).result()
                if stopLossQuery[0]['result']['order_status'] == "Untriggered" or stopLossQuery[0]['result']['order_status'] == "New":
                    stopLossComplete = True
                else:
                    print('stop loss not confirmed')
                    print('stop loss order:\n', stopLoss)
                    print('stop loss order confirmation:\n', stopLossQuery)
                    print(client.Order.Order_cancel(symbol=symbol, order_id=sl_id).result(), '\n\n')

            except:
                print('ERROR!')
                print('stop loss order:\n', stopLoss)
                print('stop loss order confirmation:\n', stopLossQuery)
                print('cautionary cancel:\n', client.Order.Order_cancel(symbol=symbol, order_id=sl_id).result(), '\n\n')

        if not takeProfitComplete:
            try:
                takeProfit = client.Order.Order_new(order_type="Limit", side=otherSide, time_in_force="GoodTillCancel", qty=qty, price=takeprofit, symbol=symbol, close_on_trigger=True).result()
                tp_id = takeProfit[0]['result']['order_id']
                takeProfitQuery = client.Order.Order_query(symbol=symbol, order_id=tp_id).result()
                if takeProfitQuery[0]['result']['order_status'] == "New":
                    takeProfitComplete = True
                else:
                    print('take profit not confirmed')
                    print('take profit order:\n', takeProfit)
                    print('take profit order confirmation:\n', takeProfitQuery)
                    print(client.Order.Order_cancel(symbol=symbol, order_id=tp_id).result(), '\n\n')

            except:
                print('ERROR!')
                print('take profit placement failed:\n', takeProfitQuery)
                print('take profit order:\n', takeProfit)
                print('cautionary cancel\n', client.Order.Order_cancel(symbol=symbol, order_id=tp_id).result(), '\n\n')


        if orderPlaced and orderFilled and stopLossComplete and takeProfitComplete:
            activeExits.append((sl_id, tp_id))
            print('active exits:\n', activeExits, '\n')
            return '~~~TRADE COMPLETE~~~'


        print('tokens remaining:', tokens)

    print('cancelling active orders')
    try:
        print(client.Order.Order_cancel(symbol=symbol, order_id=entry_id).result())
    except:
        pass
    try:
        print(client.Order.Order_cancel(symbol=symbol, order_id=sl_id).result())
    except:
        pass
    try:
        print(client.Order.Order_cancel(symbol=symbol, order_id=tp_id).result())
    except:
        pass
    return 'trade cancelled, wait for next opportunity'


def checkActiveExits(completedOrder):
    # print('completed order id\n', completedOrder)
    # print('activeExits\n', activeExits)
    for aE in activeExits:
        if aE[0] == completedOrder:
            return client.Order.Order_cancel(symbol="BTCUSD", order_id=aE[1]).result()
        if aE[1] == completedOrder:
            return client.Conditional.Conditional_cancel(symbol="BTCUSD", stop_order_id=aE[0]).result()


"""additional functions needed:
Close all positions: kill switch in case of emergency
Long term testing
"""

