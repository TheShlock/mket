#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy

#Project files
import str_ma
import logicnode



class mketbot(object):
    """Manages sending price data to strategies, and the opening and closing of trades"""
    def __init__(self, strategies):
        self.capital = 1000
        self.wontrades = 0
        self.losttrades = 0
        self.feespaid = 0
        self.finishedtrades = 0
        self.datapoints = 0

        self.risk = 0.01
        self.position = 0 # number of contracts
        self.entryprice = None
        self.exitCondition = None

        self.strategies = strategies
        self.datamap = {}
        self.datarowtemplate = {"open": 0, "high": 0, "low": 0, "close": 0, 'buy': None, 'sell': None}

        for s in self.strategies: # for each of the strategies
            for o in s.outputs:
                self.datarowtemplate[o] = None # add their outputs to the datarowtemplate

        print('datarow:',self.datarowtemplate)
        print(self.__dict__)



    def closePosition(self, timestamp):
        if self.position:
            priceDelta = (self.datamap[timestamp]['open']-self.entryprice)/self.entryprice # % change in price
            if priceDelta > 0:
                self.wontrades += 1
            else:
                self.losttrades += 1
            print('close')
            if self.position > 0:
                self.datamap[timestamp]['sell'] = self.datamap[timestamp]['open']
            if self.position < 0:
                self.datamap[timestamp]['buy'] = self.datamap[timestamp]['open']

            print('change in capital', self.position * priceDelta)
            self.capital += self.position * priceDelta # update balance
            self.position = 0  # reset position
            self.entryprice = None # reset entry price

        else:
            print('No position to close...')

    def openPosition(self, pos, timestamp):
        if self.position:
            self.closePosition(timestamp)
        self.position = pos
        self.entryprice = self.datamap[timestamp]['open']

        if pos > 0:
            self.datamap[timestamp]['buy'] = self.datamap[timestamp]['open']
            print('buy')
        if pos < 0:
            self.datamap[timestamp]['sell'] = self.datamap[timestamp]['open']
            print('sell')

    def checkTrade(self, timestamp):
        pass

    def tradeMaker(self, timestamp):
        if self.datamap[timestamp]['8MA'] and self.datamap[timestamp]['2MA'] and self.datamap[timestamp]['8MAD'] and self.datamap[timestamp]['15MAD']:
            if self.datamap[timestamp]['8MA'] > self.datamap[timestamp]['2MA'] and self.datamap[timestamp]['15MAD'] > 0:
                if not self.position > 0:
                    self.openPosition(self.capital, timestamp)

            if self.datamap[timestamp]['8MA'] < self.datamap[timestamp]['2MA'] and self.datamap[timestamp]['15MAD'] < 0:
                if not self.position < 0:
                    self.openPosition(-self.capital, timestamp)

        else:
            print('no data yet')

    def interval(self, timestamp, open_, high, low, close):
        datarow = copy.deepcopy(self.datarowtemplate)
        datarow['open'] = open_
        datarow['high'] = high
        datarow['low'] = low
        datarow['close'] = close

        self.datamap[timestamp] = datarow
        print('self.datamap[timestamp]',self.datamap[timestamp])

        for s in self.strategies:
            if self.datamap[timestamp][s.inputs]:
                self.datamap = s.interval(self.datamap, timestamp) #send the datamap to be modified by each strategy
        
        if self.position:
            self.checkTrade(timestamp)

        self.tradeMaker(timestamp)
        self.datapoints += 1





def sim(ma1, ma2, ma3):
    strategies = [str_ma.RMAD('open', [str(ma1)+'MA', str(ma1)+'MAD'], ma1), str_ma.RMAD('open', [str(ma2)+'MA'], ma2), str_ma.RMAD('open', [str(ma3)+'MA', str(ma3)+'MAD'], ma3)]
    bot = mketbot(strategies)

    points = 200000

    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    rsp = random.randrange(points, len(btc)) #random starting point

    print('starting bot.datamap', bot.datamap)

    for i in range(points):
        dataRow = btc.iloc[rsp - i]
        bot.interval(i, dataRow['open'], dataRow['high'], dataRow['low'], dataRow['close']) # feed the bot candles

    print(bot.capital)
    print(bot.wontrades)
    print(bot.losttrades)
    """Graphical Display"""
    # Make the dataframe indexed by the date

    df = pd.DataFrame(bot.datamap).transpose()
    # df = df.transpose()

    print(df)
    print(df.index.dtype)

    fig = plt.figure()
    gs = fig.add_gridspec(1, hspace=0)
    ax = gs.subplots(sharex=True)


    plt.plot(df.index, df[strategies[0].outputs[0]])
    plt.plot(df.index, df[strategies[1].outputs[0]])
    plt.plot(df.index, df['open'], label = 'open')
    plt.scatter(df.index, df['buy'], c = 'g')
    plt.scatter(df.index, df['sell'], c = 'r')


    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()

sim(8,2,15)

def sim_g(period, MADsmooth, threshold, signalthreshold):
    ma = str_ma.RMAD('open', [str(period)+'MA', str(period)+'MAD', str(period)+'MADP'], period)
    smad = str_ma.RMAD(str(period)+'MAD', [str(period)+'SMA', str(period)+'SMAD', str(period)+'SMADP'], MADsmooth)
    strategies = [ma, smad]
    bot = mketbot(1000, 0.01, strategies, str(period)+"MAD", threshold, signalthreshold)

    points = 500000

    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    rsp = random.randrange(points, len(btc)) #random starting point

    print('starting bot.datamap', bot.datamap)

    for i in range(points):
        dataRow = btc.iloc[rsp - i]
        bot.interval(i, dataRow['open'], dataRow['high'], dataRow['low'], dataRow['close']) # feed the bot candles
        

    
    # output_dict = bot.__dict__
    # del output_dict['datamap']
    # del output_dict['feespaid']
    # del output_dict['finishedtrades']
    # del output_dict['strategies']
    # del output_dict['datarowtemplate']
    # del output_dict['exitCondition']
    # print(output_dict)

    # with open('ma_random.csv', 'a') as result:
    #     w = csv.DictWriter(result, output_dict.keys())
    #     w.writerow(output_dict)

    print(bot.capital)
    """Graphical Display"""
    # Make the dataframe indexed by the date

    df = pd.DataFrame(bot.datamap).transpose()
    # df = df.transpose()

    print(df)
    print(df.index.dtype)

    fig = plt.figure()
    gs = fig.add_gridspec(3, hspace=0)
    ax = gs.subplots(sharex=True)


    ax[0].plot(df.index, df[str(period)+'MA'])
    ax[0].plot(df.index, df['open'], label = 'open')
    ax[0].scatter(df.index, df['buy'], c = 'g')
    ax[0].scatter(df.index, df['sell'], c = 'r')

    ax[1].plot(df.index, df[str(period)+'MAD'])

    ax[2].plot(df.index, df[str(period)+'SMA'])

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()


# fibs = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
# for i1 in fibs:
#     for i2 in fibs:
#         if i1 != i2:
#             sim(i1, i2)