#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

#Project files
import logicnode

"""This code tests moving average cross strategies and outputs the results to a graph and to a csv file"""
def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append(row.close)
    df = pd.DataFrame(condensed, columns=['close'])
    return df


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = (price-self.price)/self.price # percentage change in price
            d_cap = p_change*self.position # change in capital
            self.price = price # reset price
            self.position = position # reset position
            return d_cap
        return 0

class strategy(object):
    def __init__(self, name, input, func):
        self.name = name
        self.input = input
        self.obj = func
        self.trade_obj = trade()
        self.bias = None
        self.capital = 1000
        self.max_cap = self.capital
        self.max_p_lost = 0

    def feed(self, price, input):
        bias = self.obj(input)
        if bias != self.bias:
            self.bias = bias
            self.capital += self.trade_obj.c_pos(price, bias*self.capital)

        if self.capital > self.max_cap:
            self.max_cap = self.capital

        if (self.max_cap-self.capital)/self.max_cap > self.max_p_lost:
            self.max_p_lost = (self.max_cap-self.capital)/self.max_cap

def cross_2(input):
    value1 = input[0]
    value2 = input[1]
    if value1 > value2:
        return 1
    else:
        return -1

strats = {
    1: [[8,2], [3,2], [8,3]],
    3: [[3,2], [5,2], [13,8]],
    5: [[8,5], [3,2], [8,3]],
    10: [[3,2], [5,2], [13,89]],
    15: [[3,2], [5,2], [21,89]],
    30: [[21,34], [3,55], [21,55]],
    60: [[13,21], [5,55], [3,89]],
    240: [[3,5], [2,55], [8,21]],
    1440: [[2,55], [5,55], [5,34]],
    10080: [[3,34], [3,8], [13,21]],
}

strategies = []
for f in strats[10080]:
    strategies.append(strategy(str(f[0])+'>'+str(f[1]), [str(f[0])+'SMA', str(f[1])+'SMA'], cross_2))

for s in strategies:
    print(s.name, s.input)


def single_tf_test(tf, dataset, strategies):

    """Read the dataset"""
    df = condense(tf, pd.read_csv(dataset))

    """Add in indicators"""
    # df['2SMA'] = ta.SMA(df.close, timeperiod=2)
    df['3SMA'] = ta.SMA(df.close, timeperiod=3)
    # df['5SMA'] = ta.SMA(df.close, timeperiod=5)
    df['8SMA'] = ta.SMA(df.close, timeperiod=8)
    df['13SMA'] = ta.SMA(df.close, timeperiod=13)
    df['21SMA'] = ta.SMA(df.close, timeperiod=21)
    df['34SMA'] = ta.SMA(df.close, timeperiod=34)
    # df['55SMA'] = ta.SMA(df.close, timeperiod=55)
    # df['89SMA'] = ta.SMA(df.close, timeperiod=89)

    print(df)
    """Add spot for capital"""
    for s in strategies:
        df[s.name] = 0

    """Main loop"""
    t = time.time()
    for k, row in df.iterrows():
        print(k)
        for s in strategies:
            s.feed(row['close'], (row[s.input[0]], row[s.input[1]]))
            df.loc[k, s.name] = s.capital

    """Output section"""
    print('loop time', time.time()-t)
    
    df.to_csv('tf10080.csv')

    # """Matplotlib section"""
    # fig = plt.figure()
    # gs = fig.add_gridspec(2, hspace=0)
    # ax = gs.subplots(sharex=True)
    # plt.title(str(tf))
    # ax[0].plot(df.index, df['close'], label = 'close')
    # ax[0].plot(df.index, df['8SMA'], label = '8SMA')
    # ax[0].plot(df.index, df['2SMA'], label = '2SMA')
    # ax[0].legend()

    # for s in strategies:
    #     ax[1].plot(df.index, df[s.name], label = s.name)
    # plt.yscale("log")
    # ax[1].legend()

    # plt.tick_params(
    #     axis='x',          # changes apply to the x-axis
    #     which='both',      # both major and minor ticks are affected
    #     bottom=False,      # ticks along the bottom edge are off
    #     top=False,         # ticks along the top edge are off
    #     labelbottom=False) # labels along the bottom edge are off

    # plt.show()
   

single_tf_test(10080, "../.datasets/BTCUSD_live.csv", strategies)


