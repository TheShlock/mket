class indicator(object):
    def __init__(self, inputs, output):
        self.inputs = inputs # a dict of strings
        self.outputs = output # a dict of strings

"""Each strategy will define the names of the keys of its inputs and its output
each strategy will have a method called interval and will take ohlc as argument
It will then return if it has something to return and the bot will complete the datarow
returns [self.output, value] else none

The strategy should return an object that the bot knows what to do with - This is a generic statement
For now, the bot will only deal with strategies that output one returnVal
"""

## The output is just a signature that the bot can use to fill the datamap