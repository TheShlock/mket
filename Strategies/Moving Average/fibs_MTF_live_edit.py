#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

#Project files
import logicnode

"""This code aims to test multiple concurrent moving average cross strategies
The goal is to reduce drawdown by combining position sizing"""

class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = (price-self.price)/self.price # percentage change in price
            d_cap = p_change*self.position # change in capital
            self.price = price # reset price
            self.position = position # reset position
            return d_cap
        return 0

class strategy(object):
    def __init__(self, name, input, func):
        self.name = name
        self.input = input
        self.obj = func
        self.trade_obj = trade()
        self.bias = None
        self.capital = 1000
        self.max_cap = self.capital
        self.max_p_lost = 0

    def feed(self, price, input):
        bias = self.obj(input)
        if bias != self.bias:
            self.bias = bias
            self.capital += self.trade_obj.c_pos(price, bias*self.capital)

        if self.capital > self.max_cap:
            self.max_cap = self.capital

        if (self.max_cap-self.capital)/self.max_cap > self.max_p_lost:
            self.max_p_lost = (self.max_cap-self.capital)/self.max_cap
        

def cross_2(input):
    value1 = input[0]
    value2 = input[1]
    if value1 < value2:
        return 1
    elif value1 > value2: 
        return -1
    else:
        return 0


strats = {
    1: [[8,2], [3,2], [8,3]],
    3: [[3,2], [5,2], [13,8]],
    5: [[8,5], [3,2], [8,3]],
    10: [[3,2], [5,2], [13,89]],
    15: [[3,2], [5,2], [21,89]],
    30: [[21,34], [3,51], [21,51]],
    60: [[13,21], [5,51], [3,89]],
    240: [[3,5], [2,55], [8,21]],
    1440: [[2,51], [5,51], [5,34]],
    10080: [[3,34], [3,8], [13,21]],
}

strategies = {}
for key in strats:
    print(key, strats[key])
    s_ = []
    for s in strats[key]:
        s_.append(strategy(str(key)+str(s), [str(s[0])+'SMA', str(s[1])+'SMA'], cross_2))

    strategies[key] = s_

print(strategies)


class timeframe(object):
    """Timeframe object used to provide OHLC data for higher timeframes while only being fed smaller TFs"""
    def __init__(self, interval):
        self.interval = interval
        self.close = 0
        self.dps = 0
    
    def point(self, close):
        self.dps += 1
        if self.interval == self.dps:
            self.close = close
            return self

    def reset(self):
        self.close = 0
        self.dps = 0

def fractionbias_between_ones(input):
    """takes 2 numbers, first one is the result of keeping a count for short and long calls
    Second number is the total amount of short and long calls
    With these 2 numbers it will return with a number between -1 and 1 which will determine how bullish or bearish we are"""
    numerator = input[0]
    denominator = input[1]
    if denominator == 0:
        return 0
    return (numerator-(denominator/2)) / (denominator/2)

s_combine = strategy('s_combine', 'short and long sum and total calls', fractionbias_between_ones)
w_combine = strategy('w_combine', 'sum and total positions', fractionbias_between_ones)

# time.sleep(10)

def single_tf_test(dataset, strategies):

    """Read the dataset"""
    df = pd.read_csv(dataset)

    """Add spot in dataframe for strategy capital"""
    for t in strategies:
        for s in strategies[t]:
            df[s.name] = 0

    """Combination strategies"""
    df['s_combine'] = 0
    df['w_combine'] = 0
    print(df.info())
    
    """Create timeframe objects"""
    timeframes = []
    tf_data = {} 
    for t in strategies:
        timeframes.append(timeframe(t))
        tf_data[t] = []

    # time.sleep(10)
    """Main loop"""
    tm = time.time()
    for k, row in df.iterrows():
        for t in timeframes:
            tfres = t.point(row['close'])
            if tfres: # the timeframe just completed
                tf_data[t.interval].append(tfres.close) # add the close to the timeframe data array
                t.reset() # reset the timeframe
                tf_df = pd.DataFrame(tf_data[t.interval], columns = ['close']) #convert the data array to a dataframe
                ma_list = []

                for i in strats[t.interval]: #put the MAs needed in a list
                    for e in i:
                        if e not in ma_list:
                            ma_list.append(e)

                for m in ma_list:
                    tf_df[str(m)+'SMA'] = ta.SMA(df.close, timeperiod=m) #calculate the MAs for the dataframe

                for s in strategies[t.interval]:
                    s.feed(row['close'], (tf_df.iloc[-1][s.input[0]], tf_df.iloc[-1][s.input[1]]) ) #for each of the strategies if their MAs are available feed them to the strategies
                    df.loc[k,s.name] = s.capital #add the strategies capital to the big dataframe
        
        sum_calls = 0
        sum_positions = 0
        total_positions = 0
        count = 0
        for t in strategies:
            for s in strategies[t]:
                count += 1
                if s.trade_obj.position > 0:
                    sum_calls += 1
                if s.trade_obj.position < 0:
                    sum_calls -= 1
                sum_positions += s.trade_obj.position
                total_positions += abs(s.trade_obj.position)
        
        s_combine.feed(row['close'], (sum_calls, count))
        df.loc[k, s_combine.name] = s_combine.capital
        w_combine.feed(row['close'], (sum_positions, total_positions))
        df.loc[k, w_combine.name] = w_combine.capital

        print('sum calls', sum_calls)
        print('sum positions', sum_positions)
        print('total positions', total_positions)
        print('count', count)




        print(k)
        # time.sleep(1)


    """Output section"""
    print('loop time', time.time()-tm)
    
    df.to_csv('wut.csv')
    with open('mtf_ma_cross.csv', 'a') as f:
        for key in strategies:
            for s in strategies[key]:
                f.write(s.name+','+dataset+','+str((1-s.max_p_lost)*s.capital)+','+str(s.capital)+','+str(s.max_p_lost)+'\n')


    """Matplotlib section"""
    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)
    plt.title(str("fibs_mtf"))
    ax[0].plot(df.index, df['close'], label = 'close')

    print('this is the dataframe', df.info())
    for s in strategies:
        print(s)
        for strat in strategies[s]:
            ax[1].scatter(df.index, df[strat.name], label = strat.name)
    ax[1].plot(df.index, df['s_combine'], label = 's_combine')
    ax[1].plot(df.index, df['w_combine'], label = 'w_combine')

    plt.yscale("log")
    ax[1].legend()

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
   

single_tf_test("../.datasets/BTCUSD_live.csv", strategies)

# fibs = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
