#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

#Project files
import logicnode


def condense(period):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    df = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    df = df[::-1].reset_index() # reverse the rows and set the index
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append(row.close)
    df = pd.DataFrame(condensed, columns=['close'])
    return df


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0
        self.won_trades = 0
        self.lost_trades = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = ((price-self.price)/self.price)
            d_cap = p_change*self.position # change in capital
            if d_cap > 0:
                self.won_trades += 1
            if d_cap < 0:
                self.lost_trades += 1
            self.price = price
            self.position = position
            return d_cap
        return 0

def sim():
    btc = pd.read_csv("../.datasets/BTCUSD_live.csv")
    btc['2SMA'] = ta.SMA(btc.close, timeperiod=2)
    btc['3SMA'] = ta.SMA(btc.close, timeperiod=3)
    btc['5SMA'] = ta.SMA(btc.close, timeperiod=5)
    btc['8SMA'] = ta.SMA(btc.close, timeperiod=8)
    btc['13SMA'] = ta.SMA(btc.close, timeperiod=13)
    print(btc.info())
    t = trade()
    capital = 1000
    drawdown = capital
    for row in btc.itertuples():
        pos = 0
        if row[9]>row[6]:
            pos += capital
        else:
            pos -= capital
        if row[7]>row[6]:
            pos += capital
        else:
            pos -= capital
        if row[9]>row[7]:
            pos += capital
        else:
            pos -= capital
        if row[10]>row[9]:
            pos += capital
        else:
            pos -= capital
        if row[10]>row[8]:
            pos += capital
        else:
            pos -= capital


        capital += t.c_pos(row[5], pos)
        if capital < drawdown:
            drawdown = capital
        print('capital: ', capital)
    print('drawdown: ', drawdown)
    print(t.__dict__)

    # fig = plt.figure()
    # gs = fig.add_gridspec(1, hspace=0)
    # ax = gs.subplots(sharex=True)
    # plt.plot(btc.index, btc['open'], label = 'open')
    # plt.plot(btc.index, btc['close'], label = 'close')
    # plt.plot(btc.index, btc['15SMA'], label = '15SMA')
    # plt.plot(btc.index, btc['8SMA'], label = '8SMA')
    # plt.plot(btc.index, btc['2SMA'], label = '2SMA')
    # plt.scatter(btc.index, btc['buy'], c = 'g')
    # plt.scatter(df.index, df['sell'], c = 'r')
    # plt.tick_params(
    #     axis='x',          # changes apply to the x-axis
    #     which='both',      # both major and minor ticks are affected
    #     bottom=False,      # ticks along the bottom edge are off
    #     top=False,         # ticks along the top edge are off
    #     labelbottom=False) # labels along the bottom edge are off

    # plt.show()

sim()

# fibs = [1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
# for i1 in fibs:
#     for i2 in fibs:
#         if i1 != i2:
#             sim(i1, i2)