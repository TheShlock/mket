#Samuel Lockton 2021 ©™

# Utilities
from time import time, sleep

#Project files
import str_dubs
import bybitz


class mketbot(object):
    """Manages sending price data to a strategy, trade opportunities and the opening of trades"""
    def __init__(self, risk, retraceSens, closeSens, stopLoss, takeProf, pointSens, tradeLim, opTimeLim):
        self.tradeLim = tradeLim # limit of active trades open concurrently
        self.opTimeLim = opTimeLim  # after this many datapoints peaks will no longer be considered for entries

        self.risk = risk # %of capital you will lose on stoploss hit
        self.strategy = str_dubs.dubtop(retraceSens, closeSens, pointSens, stopLoss, takeProf)
        self.activetrades = 1 # create a function to get this from bybitz
        self.tradeops = []
        self.deadops = 0
        self.datapoints = 0


    def check_ops(self, price):
        for op in self.tradeops: # check trading opportunities
            op.price_routine(price)
            if op.trade: #valid trade setup
                if not self.activetrades + 1 > self.tradeLim: #if the limit isnt already reached
                    self.opentrade(op.trade, price)
                self.tradeops.remove(op)

            elif not op.valid or op.age > self.opTimeLim: # remove the tradeop
                self.deadops += 1
                self.tradeops.remove(op)        

    def feed_strategy(self, price):
        strat_out = self.strategy.newprice(price)
        if strat_out:
            self.tradeops.append(strat_out) #a retrace_closeclose object

    def opentrade(self, trade, price):
        print(trade.__dict__)
        print('account value in USD', float(bybitz.balance)*price)
        print('leverage', self.risk/(abs(trade.stoploss-trade.entry)/trade.entry))
        print('trade.trade_type', trade.trade_type)
        print('qty', int((float(bybitz.balance)*price) * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry))))
        print(bybitz.makeTrade(trade.trade_type, "BTCUSD", int((float(bybitz.balance)*price) * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry))), trade.stoploss, trade.takeprofit, price-10, price+10, 60, 10))


    def newprice(self, price):
        self.datapoints += 1

        self.check_ops(price) # Look for valid trade openings

        self.feed_strategy(price) # create new opening opportunities


"""Bot Instance"""
bot = mketbot(0.01, 0.00110097758681489, 0.338012931617768, 0.710139900584224, 0.145143434796625, 2, 9, 189)


"""Test Routine"""
price = None
while True:
    try:
        price = bybitz.pricedict["BTCUSD"]
    except:
        pass
    if price:
        print('test routine')
        print(bybitz.makeTrade("Buy", "BTCUSD", 1000, price-20, price+20, price-20, price+20, 60, 30))
        break

print('bybitz.balance', bybitz.balance)
"""Price loop"""
while True:
    try:
        price = bybitz.pricedict["BTCUSD"]
    except:
        pass
    
    if round(time())%60 == 0: #every 10 seconds
        print('price', price)
        if price:
            bot.newprice(price)
            print(bot.__dict__)
            for t in bot.tradeops:
                print(t.__dict__)
        sleep(1)

