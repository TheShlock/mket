#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
from datetime import datetime
import collections
import copy
import talib as ta

class trade(object):
    def __init__(self, pos, entry, sl, tp):
        self.pos = pos
        self.entry = entry
        self.sl = sl
        self.tp = tp
        self.state = 'active'

    def check_trade(self, row):
        if self.pos > 0: # long
            if row['low'] < self.sl:
                self.state = 'stopped_out'
            if row['high'] > self.tp:
                self.state = 'took_profit'
        if self.pos < 0: # short
            if row['high'] > self.sl:
                self.state = 'stopped_out'
            if row['low'] < self.tp:
                self.state = 'took_profit'


def single_tf_test(tf, dataset, cond, stdev, rr, patr):

    """Read the dataset"""
    df = pd.read_csv(dataset)[:100]
    print(df)

    """Prepare the dataframe with indicators/outputs"""
    df["buy"] = NaN
    df["sell"] = NaN
    df["close_pos"] = NaN
    df["capital"] = 1000
    df['wick_min'] = NaN
    df['body_min'] = NaN
    df['wick_max'] = NaN
    df['body_max'] = NaN

    """Find min and max points for wicks and bodies and write to dataframe"""
    for k, row in df[1:-1].iterrows():
        if df.loc[k-1]['high'] < df.loc[k]['high'] and df.loc[k+1]['high'] < df.loc[k]['high']:
            df.loc[k, 'wick_max'] = row['high']
    for k, row in df[1:-1].iterrows():
        if df.loc[k-1]['low'] > df.loc[k]['low'] and df.loc[k+1]['low'] > df.loc[k]['low']:
            df.loc[k, 'wick_min'] = row['low']
    for k, row in df[1:-1].iterrows():
        if df.loc[k-1]['close'] < df.loc[k]['close'] and df.loc[k+1]['close'] < df.loc[k]['close']:
            df.loc[k, 'body_max'] = row['close']
    for k, row in df[1:-1].iterrows():
        if df.loc[k-1]['close'] > df.loc[k]['close'] and df.loc[k+1]['close'] > df.loc[k]['close']:
            df.loc[k, 'body_min'] = row['close']
    df.to_csv('res.csv')


    max_points = {} # maximum origin points for start of move
    min_points = {} # minimum origin points for start of move
    for k, row in df.iterrows():
        input('next')
        for key in list(max_points.keys()): # remove invalid maximums
            if row['high'] > key:
                del max_points[key]

        for key in list(min_points.keys()): # remove invalid minimums
            if row['low'] < key:
                del min_points[key]

        # print('max', max_points)
        # print('min', min_points)

        poi = [] # points of interest
        if not pd.isna(row['wick_max']):
            poi.append([row['wick_max'], 'wick_max', row[0]])
        if not pd.isna(row['wick_min']):
            poi.append([row['wick_min'], 'wick_min', row[0]])
        if not pd.isna(row['body_max']):
            poi.append([row['body_max'], 'body_max', row[0]])
        if not pd.isna(row['body_min']):
            poi.append([row['body_min'], 'body_min', row[0]])

        for p in poi: # add poi to origin points
            for o in max_points:
                max_points[o].append(p)
            for o in min_points: 
                min_points[o].append(p)

        if not pd.isna(row['wick_max']): # add new origins
            max_points[row['wick_max']] = []
        if not pd.isna(row['wick_min']):
            min_points[row['wick_min']] = []

        for point in max_points:
            check_fibs(point, max_points[point])
        
def check_fibs(origin, points):
    for p1 in points:
        for p2 in points:
            if abs(origin-p1[0])/abs(origin-p2[0]) > 0.615 and abs(origin-p1[0])/abs(origin-p2[0]) < 0.621:
                print('origin', origin, 'p1', p1, 'p2', p2, 'ratio', abs(origin-p1[0])/abs(origin-p2[0]))
                # print("match!")


    # """Output section"""
    # # print(df)
    # # print('won', won_trades, 'lost', lost_trades, 'capital', capital)
    # # with open('retest.csv', 'a') as f:
    # #     f.write(str(tf)+','+cond+','+str(stdev)+','+str(rr)+','+str(patr)+','+str(won_trades)+','+str(lost_trades)+','+str(capital)+'\n')
    # # df.to_csv(name+'.csv')

    # """Matplotlib section"""
    # fig = plt.figure()
    # # gs = fig.add_gridspec(1, hspace=0)
    # # ax = gs.subplots(sharex=True)
    
    # # plt.plot(df.index, df['open'], label = 'open')
    # plt.plot(df.index, df['high'], label = 'high')
    # plt.plot(df.index, df['low'], label = 'low')
    # # plt.plot(df.index, df['close'], label = 'close')
    # plt.scatter(df.index, df['wick_min'], label = 'wick_min')
    # plt.scatter(df.index, df['wick_max'], label = 'wick_max')
    # plt.scatter(df.index, df['body_min'], label = 'body_min')
    # plt.scatter(df.index, df['body_max'], label = 'body_max')
    # # plt.plot(df.index, df['bb_upper'], label = 'bb_upper')
    # # plt.plot(df.index, df['bb_middle'], label = 'bb_middle')
    # # plt.plot(df.index, df['bb_lower'], label = 'bb_lower')

    # # plt.scatter(df.index, df['buy'], label = 'buy')
    # # plt.scatter(df.index, df['sell'], label = 'sell')
    # # plt.scatter(df.index, df['close_pos'], label = 'close_pos')
    # # ax.legend()

    # # ax[2].plot(df.index, df['ATR'])
    # # # ax[1].plot(df.index, df['RSI'])
    # # ax[1].plot(df.index, df['capital'])
    # # plt.yscale("log")


    # # plt.tick_params(
    # #     axis='x',          # changes apply to the x-axis
    # #     which='both',      # both major and minor ticks are affected
    # #     bottom=False,      # ticks along the bottom edge are off
    # #     top=False,         # ticks along the top edge are off
    # #     labelbottom=False) # labels along the bottom edge are off




    # plt.show()


single_tf_test(60, "../live/future_bars1hr.csv", 'simple', 1.8, 10, 0.8)

