#Samuel Lockton 2021

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import talib as ta

# Utilities
import csv
import time
import collections
import copy

"""This code was used to package the ta.RSI dataframe function to be able to be used online
such that the values can be computed for live data. It was achieved by storing the last 14 values and 
using the function as usual. This turned out to be a lot slower for a large dataset but for an online application
which i have planned to used on a 1 minute time interval it should be fast enough"""

class RSI_Point(object):
    '''created with an RSI value, price and time period
    fed subsequent prices and responds with a factor for how close the price is to the expected value'''
    def __init__(self, price, RSIvalue, period) -> None:
        self.price = price
        if RSIvalue == 100: 
            RSIvalue -= 1 # bug fix
        self.statarr = RSI_stat_dict[period][str(int(round((RSIvalue/5) - 0.5)))]  # a reference to the statistics sheet object that this RSI value and time period correspond to
        self.pnts = -1

    def feed(self, price):
        '''feed with the current price'''
        self.pnts += 1
        pchange = (price-self.price)/self.price
        # print('pchange', pchange)
        expected = float(self.statarr[self.pnts])
        # print('expected', expected)
        return (expected-pchange)*expected




RSI_stat_dict = {}  # {period: {band: [nextn]}}

with open ('RSI_stats.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        if not row[0] in RSI_stat_dict:
            RSI_stat_dict[row[0]] = {}
        if not row[1] in RSI_stat_dict[row[0]]:
            RSI_stat_dict[row[0]][row[1]] = row[2:-1]

print(RSI_stat_dict)
a = RSI_Point(10000, 9.5, '1min')
b = RSI_Point(10000, 47.5, '1min')



class mketbot(object):
    """Manages sending price data to indicators, and the opening and closing of trades"""
    def __init__(self, timeframes, initialohlc):
        self.capital = 1000
        self.wontrades = 0
        self.losttrades = 0
        self.feespaid = 0
        self.finishedtrades = 0
        self.datapoints = 0

        self.risk = 0.01
        self.position = 0 # number of contracts
        self.entryprice = None
        self.exitCondition = None


        self.datamap = {}
        datarowtemplate = {"open": initialohlc[0], "high": initialohlc[1], "low": initialohlc[2], "close": initialohlc[3], 'buy': None, 'sell': None}

        self.timeframes = []
        for i in range(len(timeframes)):
            self.timeframes.append(timeframe(timeframes[i]))
            datarowtemplate[str(timeframes[i])+'minRSI'] = None
            datarowtemplate[str(timeframes[i])+'minRSISTR'] = None

        self.datamap[0] = datarowtemplate

        self.timeframedict = {}
        for t in self.timeframes:
            self.timeframedict[t.interval] = pd.DataFrame(columns = ['open', 'high', 'low', 'close'])

        self.RSISTRdict = {} #{timeframe{}}




    def interval(self, timestamp, open_, high, low, close):
        datarow = copy.deepcopy(self.datamap[timestamp-1]) #copy the datarow template down
        datarow['open'] = open_ #add in the candle data
        datarow['high'] = high
        datarow['low'] = low
        datarow['close'] = close
        self.datamap[timestamp] = datarow #add the partially filled datarow to the datamap

        for t in self.timeframes:
            tfres = t.point(datarow['open'], datarow['high'], datarow['low'], datarow['close']) # feed timeframe object
            if tfres: # if the interval just finished
                self.timeframedict[t.interval] = self.timeframedict[t.interval].append({'open': t.open,'high': t.high,'low': t.low,'close': t.close}, ignore_index=True) # add the data to the dataframe
                t.reset() # reset the timeframe
                self.timeframedict[t.interval]['RSI'] = ta.RSI(self.timeframedict[t.interval].close, timeperiod=14) # try calculate rsi
                tfrsi = self.timeframedict[t.interval].iloc[-1].RSI # assign to variable
                if not pd.isna(tfrsi): # there is enough data to calculate the rsi
                    RSISTR = 0
                    del_point = None
                    if t.interval in self.RSISTRdict:
                        # print(self.RSISTRdict[t.interval])
                        for rsi_point in self.RSISTRdict[t.interval]:
                            # print(self.RSISTRdict[rsi_point].__dict__)
                            RSISTR += self.RSISTRdict[t.interval][rsi_point].feed(close)
                            if self.RSISTRdict[t.interval][rsi_point].pnts == 18:
                                del_point = rsi_point
                    if del_point: # delete the done ones
                        self.RSISTRdict[t.interval].pop(del_point) # would be faster to use a linked list
                    if not t.interval in self.RSISTRdict:
                        self.RSISTRdict[t.interval] = {}
                    self.RSISTRdict[t.interval][timestamp] = RSI_Point(close, tfrsi, str(t.interval)+'min') # create a new point obj for the current rsi

                    self.datamap[timestamp][str(t.interval)+'minRSI'] = tfrsi # add the rsi value to the datamap for this time period
                    self.datamap[timestamp][str(t.interval)+'minRSISTR'] = RSISTR # add the RSISTR value to the datamap for this time period
                    self.timeframedict[t.interval] = self.timeframedict[t.interval].iloc[1:] # remove the first row

        
        if self.position:
            self.checkTrade(timestamp)

        # self.tradeMaker(timestamp)
        self.datapoints += 1

class timeframe(object):
    """Timeframe object used to provide OHLC data for higher timeframes while only being fed smaller TFs"""
    def __init__(self, interval):
        self.interval = interval
        self.open = 0
        self.high = 0
        self.low = 0
        self.close = 0
        self.dps = 0
    
    def point(self, open, high, low, close):
        self.dps += 1
        if not self.open:
            self.open = open
        if not self.high or high > self.high:
            self.high = high
        if not self.low or low < self.low:
            self.low = low
        if self.interval == self.dps:
            self.close = close
            return self

    def reset(self):
        self.open = 0
        self.high = 0
        self.low = 0
        self.close = 0
        self.dps = 0

def sim():
    timeframes = [1,3,5,10,15,20,30,60,120,180,240,480,720,1440]
    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    points = 10000
    rsp = random.randrange(points, len(btc)) #random starting point
    initialohlc = [btc.iloc[rsp]['open'], btc.iloc[rsp]['high'], btc.iloc[rsp]['low'], btc.iloc[rsp]['close']]

    bot = mketbot(timeframes, initialohlc)
    for i in range(1, points):
        print(i)
        dataRow = btc.iloc[rsp - i]
        bot.interval(i, dataRow['open'], dataRow['high'], dataRow['low'], dataRow['close']) # feed the bot candles

    df = pd.DataFrame(bot.datamap).transpose()
    print(df)

    fig = plt.figure()
    gs = fig.add_gridspec(3, hspace=0)
    ax = gs.subplots(sharex=True)
    ax[0].plot(df.index, df['open'], label = 'open')
    ax[0].plot(df.index, df['close'], label = 'close')
    ax[0].plot(df.index, df['high'], label = 'high')
    ax[0].plot(df.index, df['low'], label = 'low')
    ax[0].legend()

    # for t in timeframes:
    #     ax[2].plot(df.index, df[str(t)+'minRSI'], label = str(t)+'minRSI')
    # ax[2].legend()

    for t in timeframes:
        ax[1].plot(df.index, df[str(t)+'minRSISTR'], label = str(t)+'minRSISTR')
    ax[1].legend()

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
sim()



