#Samuel Lockton 2021

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import talib as ta

# Utilities
import csv
import time
import collections
import copy

"""This code was used to record the RSI value for 20 bands in the RSI range 0-100 and take a statistical test
of the subsequent values. These values which were percentage change were averaged out.
The test was performed for many timeframes and the results of which were then used to inform an RSISTR value
which was then used to test a trading strategy"""

class RSI_ATest(object):
    '''created with an RSI value, price and number of subsequent prices
    is fed subsequent prices and records the change from the first price
    is then fed to data_collector to have its values interpreted'''
    def __init__(self, RSIvalue, price, nextn) -> None:
        self.RSIvalue = RSIvalue
        self.price = price
        self.nextn = nextn
        self.ddict = {}
        self.pnts = 0

    def feed(self, price):
        self.ddict[self.pnts] = self.pChange(price)
        self.pnts += 1
        if self.pnts == self.nextn:
            return self
            
    def pChange(self, price):
        return round(((price / self.price)-1), 5)

class data_collector(object):
    '''Is created with a name, number of subsequent prices and number of ranges
    is fed RSI_ATest objects and stores their values in its dictionary
    output is called to average the changes in price for each rsi range'''
    def __init__(self, name, nextn, ranges):
        self.name = name #just a name
        self.nextn = nextn # number of prices to check after
        self.ranges = ranges # number of ranges

        deltaObj = []
        for n in range(nextn): 
            deltaObj.append(0)

        self.datadict = {}
        for i in range(ranges): 
            self.datadict[i] = copy.deepcopy(deltaObj)
        

    def feed(self, RSIobj):
        r = round((RSIobj.RSIvalue / (100/self.ranges)) - 0.5)
        for i in range(len(RSIobj.ddict)-1):
            self.datadict[r][i] += RSIobj.ddict[i] #cum sum the changes
        self.datadict[r][-1] += 1 # counter for number of objs
        
    def output(self):
        for i in self.datadict:
            if self.datadict[i][-1] != 0:
                for v in range(len(self.datadict[i])-1):
                    self.datadict[i][v] = self.datadict[i][v] / self.datadict[i][-1] #compute averages


def condense(period):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    df = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    df = df[::-1].reset_index() # reverse the rows and set the index
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append(row.close)
    df = pd.DataFrame(condensed, columns=['close'])
    return df


def runTest(time, df_):
    df = df_
    # df = df[::-1].reset_index() # reverse the rows and set the index
    df['RSI14'] = ta.RSI(df.close, timeperiod=14)
    nextn = 20
    ranges = 20
    dc = data_collector(str(time)+'min', nextn, ranges)
    rtd = []
    for row in df.itertuples():
        # print(row.index)
        if not pd.isna(row.RSI14):
            for r in rtd:
                pout = r.feed(row.close)
                if pout:
                    dc.feed(r)
                    rtd.remove(r)
            rtd.append(RSI_ATest(row.RSI14, row.close, nextn))

    dc.output()
    with open('RSI_stats.csv', 'a') as file:
        for i in dc.datadict:
            line = dc.name+','+str(i)
            for d in range(len(dc.datadict[i])):
                line+=','+ str(dc.datadict[i][d])
            file.write(line+'\n')


# df = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
l = [1,3,5,10,15,20,30,60,120,180,240,480,720,1440]
for t in l:
    runTest(t, condense(t))

