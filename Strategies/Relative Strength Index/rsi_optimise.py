#Samuel Lockton 2021

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import talib as ta

# Utilities
import csv
import time
import collections
import copy

"""This piece of code is used to optimise the parameters for a given RSISTR value stream for a timeframe
It does so by randomising parameters based on the minimum and maximum values supplied.
It then runs a simulation by opening positions at certain prices and recording changes in capital
It will generate 100 profitable parameters then take the most successful run and try to use gradient descent
on each of the 4 parameters to optimise the profit return
In practice it was found that the gradient descent only optimised by around 10% and perhaps in practice it
would be better to perform more random simulations.
Also the first instance of the tests where I personally tried the starting parameters seemed to go a lot better
than when i automated the finding and optimisation of parameters, this can be seen in the results for the 3minute
time frame. """


template = ['index', 'index_','open','high','low','close','buy','sell',
'1minRSI','1minRSISTR','3minRSI','3minRSISTR','5minRSI','5minRSISTR',
'10minRSI','10minRSISTR','15minRSI','15minRSISTR','20minRSI','20minRSISTR',
'30minRSI','30minRSISTR','60minRSI','60minRSISTR','120minRSI','120minRSISTR',
'180minRSI','180minRSISTR','240minRSI','240minRSISTR','480minRSI','480minRSISTR',
'720minRSI','720minRSISTR','1440minRSI','1440minRSISTR']
for i in range(len(template)):
    print(i, template[i])


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0

    def c_pos(self, price, position): # change position
        p_change = ((price-self.price)/self.price)
        d_cap = p_change*self.position # change in capital
        
        self.price = price
        self.position = position

        return d_cap

def machine_res(buy_, buy_close_, sell_, sell_close_):
    print(buy_, buy_close_, sell_, sell_close_)
    capital = 0
    state = None
    t = trade()
    if buy_close_ > buy_:
        print('Invalid parameters: buy_close_ > buy_')
        return 0
    if sell_close_ < sell_:
        print('Invalid parameters: sell_close_ < sell_')
        return 0

    df = pd.read_csv("computed_values.csv")
    for row in df.itertuples(): # threshold buy/sell and clos
        if state == None: 
            if row[35] > buy_: # open long
                capital += t.c_pos(row[5], 1000)
                state = 'long'

        if state == 'long':
            if row[35] < buy_close_: # close long
                capital += t.c_pos(row[5], 0)
                state = None

        if state == None:
            if row[35] < sell_: # open short
                capital += t.c_pos(row[5], -1000)
                state = 'short'

        if state == 'short':
            if row[35] > sell_close_: # close short
                capital += t.c_pos(row[5], 0)
                state = None
    with open('parameters1440min.csv', 'a') as file_:
        file_.write(str(buy_) + ',' + str(buy_close_) +  ',' + str(sell_) +  ',' + str(sell_close_) +  ',' +str(capital) + '\n')
    return capital


class param(object):
    def __init__(self, value, min, max, change_val, min_change_val):
        self.value = value
        self.min = min
        self.max = max
        self.change_val = change_val
        self.min_change_val = min_change_val

    def random_val(self):
        return random.uniform(self.min, self.max)

def optimise(params):
    print(params)
    random_profit = 0
    rand_iter = 0
    while rand_iter < 100:
        print(rand_iter)
        buy_rand = params['buy'].random_val()
        buy_close_rand = params['buy_close'].random_val()
        sell_rand = params['sell'].random_val()
        sell_close_rand = params['sell_close'].random_val()
        out = machine_res(buy_rand, buy_close_rand, sell_rand, sell_close_rand)
        print(out)
        if out <= 0: # invalid result
            rand_iter -= 1
        if out > random_profit:
            params['buy'].value = buy_rand
            params['buy_close'].value = buy_close_rand
            params['sell'].value = sell_rand
            params['sell_close'].value = sell_close_rand
            random_profit = out
        rand_iter += 1
        

    while True:
        done_params = 0
        for p in params:
            print(p, params[p].__dict__)
            if params[p].change_val > params[p].min_change_val: 
                profit_benchmark = machine_res(params['buy'].value, params['buy_close'].value, params['sell'].value, params['sell_close'].value)
                print('profit benchmark', profit_benchmark)

                params[p].value += params[p].change_val # increase the value and test
                profit_incr_param = machine_res(params['buy'].value, params['buy_close'].value, params['sell'].value, params['sell_close'].value)
                print('increase param profit', profit_incr_param)
                
                if profit_benchmark > profit_incr_param: # increasing the parameter made the test perform worse
                    params[p].value -= params[p].change_val # restore the parameter
                    
                    params[p].value -= params[p].change_val # decrease the value and test
                    profit_decr_param = machine_res(params['buy'].value, params['buy_close'].value, params['sell'].value, params['sell_close'].value)
                    print('decrease param profit', profit_decr_param)

                    if profit_benchmark > profit_decr_param: # decreasing the parameter made also the test perform worse
                        params[p].value += params[p].change_val # restore the parameter
                        params[p].change_val /= 1.5 # reduce the change_val
            else:
                done_params += 1
        
        print('done_params', done_params)
        if done_params == 4:
            break


optimise({'buy': param(-0.000426616153512, -0.05, 0.2, 0.0001, 0.0000001), 
'buy_close': param(-0.000580264015535, -0.05, 0.2, 0.0001, 0.0000001), 
'sell': param(-0.000419493658236, -0.2, 0.05, 0.0001, 0.0000001), 
'sell_close': param(0.000553740672618, -0.2, 0.05, 0.0001, 0.0000001)})


def human_res(buy, buy_close, sell, sell_close):
    capital = 0
    longs = 0
    shorts = 0
    state = None
    t = trade()
    df = pd.read_csv("computed_values.csv")
    print(df)

    for row in df.itertuples(): # threshold buy/sell and clos
        if state == None: 
            if row[9] > buy: # open long
                capital += t.c_pos(row[5], 1000)
                df.at[row[0], 'buy'] = row[5]
                state = 'long'

        if state == 'long':
            if row[9] < buy_close: # close long
                capital += t.c_pos(row[5], 0)
                df.at[row[0], 'sell'] = row[5]
                longs += 1
                state = None

        if state == None:
            if row[9] < sell: # open short
                capital += t.c_pos(row[5], -1000)
                df.at[row[0], 'sell'] = row[5]
                state = 'short'

        if state == 'short':
            if row[9] > sell_close: # close short
                capital += t.c_pos(row[5], 0)
                df.at[row[0], 'buy'] = row[5]
                state = None
                shorts += 1

    print(capital)
    print('longs', longs)
    print('shorts', shorts)
    print('total', longs + shorts)

    # df = df.iloc[800000:]
    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)
    ax[0].plot(df.index, df['open'], label = 'open')
    ax[0].plot(df.index, df['close'], label = 'close')
    ax[0].plot(df.index, df['high'], label = 'high')
    ax[0].plot(df.index, df['low'], label = 'low')
    # ax[0].scatter(df.index, df['buy'], label = 'buy')
    # ax[0].scatter(df.index, df['sell'], label = 'sell')
    ax[0].legend()


    # ax[1].plot(df.index, df['1minRSI'], label = '1minRSI')
    ax[1].plot(df.index, df['1440minRSISTR'])

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
# human_res(0,0,0,0)