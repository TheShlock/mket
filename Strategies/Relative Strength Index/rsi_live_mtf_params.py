#Samuel Lockton 2021

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import talib as ta

# Utilities
import csv
import time
import collections
import copy

"""This code was used to combine the online RSI computation of rsi_live_mtf with the rsi_stats values as well
as the best parameters found from rsi_optimise to create a trading strategy
It was found that the individual expected profits from each time frame could be combined by simply summing up the
position size recommendations for each time frame.
This is a promising result and now it can hopefully be used on its own or combined with a strategy."""

class RSI_Point(object):
    '''created with an RSI value, price and time period
    fed subsequent prices and responds with a factor for how close the price is to the expected value'''
    def __init__(self, price, RSIvalue, period) -> None:
        self.price = price
        if RSIvalue == 100: 
            RSIvalue -= 1 # bug fix
        self.statarr = RSI_stat_dict[period][str(int(round((RSIvalue/5) - 0.5)))]  # a reference to the statistics sheet object that this RSI value and time period correspond to
        self.pnts = -1

    def feed(self, price):
        '''feed with the current price'''
        self.pnts += 1
        pchange = (price-self.price)/self.price
        expected = float(self.statarr[self.pnts])
        return (expected-pchange)*expected


RSI_stat_dict = {}  # {period: {band: [nextn]}}

with open ('RSI_stats.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    for row in reader:
        if not row[0] in RSI_stat_dict:
            RSI_stat_dict[row[0]] = {}
        if not row[1] in RSI_stat_dict[row[0]]:
            RSI_stat_dict[row[0]][row[1]] = row[2:-1]



class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = ((price-self.price)/self.price)
            d_cap = p_change*self.position # change in capital
            self.price = price
            self.position = position
            return d_cap
        return 0

class tf_str_pos(object): # timeframe strength -> position
    def __init__(self, timeframe, buy, buy_close, sell, sell_close):
        self.state = None
        self.timeframe = timeframe
        self.buy = buy
        self.buy_close = buy_close
        self.sell = sell
        self.sell_close = sell_close

    def feed(self, str_val):
        #takes a strength val and determines if it would be in a position or not
        if self.state == None: 
            if str_val > self.buy: # open long
                self.state = 'long'

        if self.state == 'long':
            if str_val < self.buy_close: # close long
                self.state = None

        if self.state == None:
            if str_val < self.sell: # open short
                self.state = 'short'

        if self.state == 'short':
            if str_val > self.sell_close: # close short
                self.state = None

        return self.state


class mketbot(object):
    """Manages sending price data to indicators, and the opening and closing of trades"""
    def __init__(self, timeframes, initialohlc):
        self.datamap = {}
        datarowtemplate = {"open": initialohlc[0], "high": initialohlc[1], "low": initialohlc[2], "close": initialohlc[3], 'buy': None, 'sell': None}

        self.timeframes = []

        for i in range(len(timeframes)):
            self.timeframes.append(timeframe(timeframes[i]))
            datarowtemplate[str(timeframes[i])+'minRSI'] = None
            datarowtemplate[str(timeframes[i])+'minRSISTR'] = None


        self.position = 0
        datarowtemplate['position'] = 0
        self.capital = 0
        datarowtemplate['capital'] = 0

        self.trade = trade()

        self.datamap[0] = datarowtemplate

        self.timeframedict = {}
        for t in self.timeframes:
            self.timeframedict[t.interval] = pd.DataFrame(columns = ['open', 'high', 'low', 'close'])

        self.RSISTRdict = {} #{timeframe{timestamp: RSI_Point}}
        self.tf_pos_dict = {
        1: tf_str_pos(1, 0.0012539816675324904,0.00032407727911455426,0.0006693019811651888,0.0008029634573806621),
        3: tf_str_pos(3, 0.0008416508789525633,-0.0007452845916666875,-0.0001432356733362906,0.0004520592065662653),
        5: tf_str_pos(5, 0.003029052042904638,-0.0005344425420175933,-0.0011592837228607208,0.00031381069436994726),
        10: tf_str_pos(10, -0.00014226599969010756,-0.000158216961810929,-0.00016985206734122994,-0.00016859124218740433),
        15: tf_str_pos(15, 7.534293529479162e-05,-0.00018018969004022006,-5.759601474232208e-05,0.00010930412886772835),
        20: tf_str_pos(20, 0.00013293792143279299,-3.07060532042427e-05,9.367785960450162e-05,0.00021262360096786686),
        30: tf_str_pos(30, 0.00010772469566960226,5.0273243068007695e-05,2.3758333617377046e-05,0.00032348401934903954),
        60: tf_str_pos(60, 0.0018831439072902242,0.001677339630904823,-0.0023093989180006875,-0.0021094952728009635),
        120: tf_str_pos(120, -0.000899903128935218,-0.0009073342496569968,-0.010175083935351695,0.0025090368945284965),
        180: tf_str_pos(180, 0.0035966104896105943,0.0007572865512789027,-0.009274537149950836,-0.009013606030813501),
        240: tf_str_pos(240, 0.0019339369134533035,-0.0012566565342931994,-0.008070900492894342,0.000517011646780956),
        480: tf_str_pos(480, 0.011375985360137668,0.0014695278079851536,-0.0274334141219112,-0.01763231178265408),
        720: tf_str_pos(720, 0.05137368169651613,-0.040619031809176726,-0.12949567786433114,0.007659499312842418),
        1440: tf_str_pos(1440, -0.02319084592822879,-0.027699687609332207,-0.10220029600743004,-0.07881653768625327)
        }


    def interval(self, timestamp, open_, high, low, close):
        datarow = copy.deepcopy(self.datamap[timestamp-1]) #copy the datarow template down
        datarow['open'] = open_ #add in the candle data
        datarow['high'] = high
        datarow['low'] = low
        datarow['close'] = close
        self.datamap[timestamp] = datarow #add the partially filled datarow to the datamap

        for t in self.timeframes:
            tfres = t.point(datarow['open'], datarow['high'], datarow['low'], datarow['close']) # feed timeframe object
            if tfres: # if the interval just finished
                self.timeframedict[t.interval] = self.timeframedict[t.interval].append({'open': t.open,'high': t.high,'low': t.low,'close': t.close}, ignore_index=True) # add the data to the dataframe
                t.reset() # reset the timeframe
                self.timeframedict[t.interval]['RSI'] = ta.RSI(self.timeframedict[t.interval].close, timeperiod=14) # try calculate rsi
                tfrsi = self.timeframedict[t.interval].iloc[-1].RSI # assign to variable
                if not pd.isna(tfrsi): # there is enough data to calculate the rsi
                    RSISTR = 0
                    del_point = None
                    if t.interval in self.RSISTRdict:
                        for rsi_point in self.RSISTRdict[t.interval]:
                            RSISTR += self.RSISTRdict[t.interval][rsi_point].feed(close)
                            if self.RSISTRdict[t.interval][rsi_point].pnts == 18:
                                del_point = rsi_point
                    if del_point: # delete the done ones
                        self.RSISTRdict[t.interval].pop(del_point) # would be faster to use a linked list
                    if not t.interval in self.RSISTRdict:
                        self.RSISTRdict[t.interval] = {}
                    self.RSISTRdict[t.interval][timestamp] = RSI_Point(close, tfrsi, str(t.interval)+'min') # create a new point obj for the current rsi

                    self.datamap[timestamp][str(t.interval)+'minRSI'] = tfrsi # add the rsi value to the datamap for this time period
                    self.datamap[timestamp][str(t.interval)+'minRSISTR'] = RSISTR # add the RSISTR value to the datamap for this time period
                    self.timeframedict[t.interval] = self.timeframedict[t.interval].iloc[1:] # remove the first row
        
        pos = 0
        for i in self.timeframes:
            if self.datamap[timestamp][str(i.interval)+'minRSISTR']:
                out = self.tf_pos_dict[i.interval].feed(self.datamap[timestamp][str(i.interval)+'minRSISTR']) # feed the position dict with the strength value
                if out == 'long': # sum up the position amounts
                    pos += 1000 
                if out == 'short':
                    pos -= 1000 
        
        self.datamap[timestamp]['position'] = pos
        
        self.capital += self.trade.c_pos(close, pos) # calculate change in capital

        self.datamap[timestamp]['capital'] = self.capital # add to the datamap


class timeframe(object):
    """Timeframe object used to provide OHLC data for higher timeframes while only being fed smaller TFs"""
    def __init__(self, interval):
        self.interval = interval
        self.open = 0
        self.high = 0
        self.low = 0
        self.close = 0
        self.dps = 0
    
    def point(self, open, high, low, close):
        self.dps += 1
        if not self.open:
            self.open = open
        if not self.high or high > self.high:
            self.high = high
        if not self.low or low < self.low:
            self.low = low
        if self.interval == self.dps:
            self.close = close
            return self

    def reset(self):
        self.open = 0
        self.high = 0
        self.low = 0
        self.close = 0
        self.dps = 0


def sim():
    timeframes = [1,3,5,10,15,20,30,60,120,180,240,480,720,1440]
    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    points = 500000
    rsp = random.randrange(points, len(btc)) #random starting point
    initialohlc = [btc.iloc[rsp]['open'], btc.iloc[rsp]['high'], btc.iloc[rsp]['low'], btc.iloc[rsp]['close']]

    bot = mketbot(timeframes, initialohlc)
    for i in range(1, points):
        print(i)
        dataRow = btc.iloc[rsp - i]
        bot.interval(i, dataRow['open'], dataRow['high'], dataRow['low'], dataRow['close']) # feed the bot candles

    df = pd.DataFrame(bot.datamap).transpose()
    print(df)

    fig = plt.figure()
    gs = fig.add_gridspec(4, hspace=0)
    ax = gs.subplots(sharex=True)
    ax[0].plot(df.index, df['open'], label = 'open')
    ax[0].plot(df.index, df['close'], label = 'close')
    ax[0].plot(df.index, df['high'], label = 'high')
    ax[0].plot(df.index, df['low'], label = 'low')
    ax[0].legend()

    for t in timeframes:
        ax[1].plot(df.index, df[str(t)+'minRSISTR'], label = str(t)+'minRSISTR')
    ax[1].legend()

    # for t in timeframes:
    #     ax[2].plot(df.index, df[str(t)+'minRSI'], label = str(t)+'minRSI')
    # ax[2].legend()

    ax[3].plot(df.index, df['position'], label = 'position')
    ax[3].plot(df.index, df['capital'], label = 'capital')
    ax[3].legend()


    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
sim()



