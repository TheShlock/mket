#Samuel Lockton 2021

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math
import talib as ta

# Utilities
import csv
import time
import collections
import copy


class trade(object):
    def __init__(self):
        self.price = 1
        self.position = 0
        self.won_trades = 0
        self.lost_trades = 0

    def c_pos(self, price, position): # change position
        if self.position != position: # if position size changes
            p_change = ((price-self.price)/self.price)
            d_cap = p_change*self.position # change in capital
            if d_cap > 0:
                self.won_trades += 1
            if d_cap < 0:
                self.lost_trades += 1
            self.price = price
            self.position = position
            return d_cap
        return 0

def sim():
    t = trade()
    capital = 1000
    drawdown = capital
    btc = pd.read_csv("computed_values.csv")
    print(btc)
    print(btc.info())
    for row in btc.itertuples():
        if not pd.isna(row[11]):
            capital += t.c_pos(row[5], capital * row[11]*1000)
        print(capital)
        if capital < drawdown:
            drawdown = capital

    print('dd:', drawdown)
    fig = plt.figure()
    gs = fig.add_gridspec(2, hspace=0)
    ax = gs.subplots(sharex=True)
    ax[0].plot(btc.index, btc['close'], label = 'close')
    ax[0].legend()

    # for t in timeframes:
    #     ax[2].plot(df.index, df[str(t)+'minRSI'], label = str(t)+'minRSI')
    # ax[2].legend()

    ax[1].plot(btc.index, btc['3minRSISTR'], label = '3minRSISTR')
    ax[1].legend()

    plt.tick_params(
        axis='x',          # changes apply to the x-axis
        which='both',      # both major and minor ticks are affected
        bottom=False,      # ticks along the bottom edge are off
        top=False,         # ticks along the top edge are off
        labelbottom=False) # labels along the bottom edge are off

    plt.show()
sim()



