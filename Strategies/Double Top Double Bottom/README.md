# Double Top Double Bottom Strategy

## What?
https://www.investopedia.com/terms/d/double-top-and-bottom.asp
An instance of support and resistance levels

## Why?
Large groups of limit orders exist at notable levels due to investment patterns

## How?

str_dubs.py is the framework to execute the strategy
mketbot.py is the engine that feeds the data to the strategy object and its methods
mketbot_backtest_dubs.py is used to backtest parameterised strategies
bybit_netcode.py is used to execute live orders

The results of the multiple backtests are stored in dubs_data


## Results

## Conclusion
