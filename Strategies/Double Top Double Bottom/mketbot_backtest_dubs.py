#Samuel Lockton 2021 ©™

"""This code is used to backtest a Double top Double bottom strategy with specific parameters"""

# Mathematical packages
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import random

# Utilities
import csv
import time

#Project files
import str_dubs



class mketbot(object):
    """Manages sending price data to a strategy, trade opportunities and the opening of trades"""
    def __init__(self, capital, risk, graph, retraceSens, closeSens, stopLoss, takeProf, pointSens, tradeLim, opTimeLim):
        self.capital = capital
        self.won_trades = 0
        self.lost_trades = 0
        self.feespaid = 0

        self.retraceSens = retraceSens #retracement from peak as %of price
        self.closeSens = closeSens #sensitivity of close as %of retracement
        self.stopLoss = stopLoss # stop loss value as percentage of retracement
        self.takeProf = takeProf # take profit value of percentage of retracement
        self.pointSens = pointSens # number of points for each data point this one must be larger/smaller than to be a max/min
        self.tradeLim = tradeLim # limit of active trades open concurrently
        self.opTimeLim = opTimeLim  # after this many datapoints peaks will no longer be considered for entries
        
        self.risk = risk # %of capital you will lose on stoploss hit
        self.strategy = str_dubs.dubtop(retraceSens, closeSens, pointSens, stopLoss, takeProf)
        self.activetrades = []
        self.tradeops = [] 
        self.deadops = 0 
        self.finishedtrades = 0 
        self.datapoints = 0
        self.graph = graph


    def check_trades(self, candle):
        for trade in self.activetrades:
            if trade.trade_type == 'Sell':
                if candle['high'] > trade.stoploss:
                    print('close')
                    trade.outcome = 'loss'
                    self.lost_trades += 1
                    self.finishedtrades += 1
                    capvar = self.capital # temporary capital variable to calculate trade and fee capital delta
                    tradeDelta = capvar * self.risk
                    feeDelta = capvar * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry)) * 0.00080 #taker fee plus 5% slippage
                    self.feespaid += feeDelta
                    self.capital = self.capital - tradeDelta - feeDelta
                    if self.graph:
                        self.graph[7] = trade.stoploss
                        print('added to graph')
                    self.activetrades = []
                    
                elif candle['low'] < trade.takeprofit:
                    print('close')
                    trade.outcome = 'profit'
                    self.won_trades += 1
                    self.finishedtrades += 1
                    capvar = self.capital
                    tradeDelta = capvar * self.risk * self.takeProf / self.stopLoss
                    feeDelta = capvar * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry)) * 0.00025 #maker rebate
                    self.feespaid -= feeDelta
                    self.capital = self.capital + tradeDelta + feeDelta
                    if self.graph:
                        self.graph[7] = trade.takeprofit
                        print('added to graph')
                    self.activetrades = []

            elif trade.trade_type == 'Buy':
                if candle['low'] < trade.stoploss:
                    print('close')
                    trade.outcome = 'loss'
                    self.lost_trades += 1
                    self.finishedtrades += 1
                    capvar = self.capital
                    tradeDelta = capvar * self.risk
                    feeDelta = capvar * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry)) * 0.00080 #taker fee plus 5% slippage
                    self.feespaid += feeDelta
                    self.capital = self.capital - tradeDelta - feeDelta
                    if self.graph:
                        self.graph[7] = trade.stoploss
                        print('added to graph')
                    self.activetrades = []

                elif candle['high'] > trade.takeprofit:
                    print('close')
                    trade.outcome = 'profit'
                    self.won_trades += 1
                    self.finishedtrades += 1
                    capvar = self.capital
                    tradeDelta = capvar * self.risk * self.takeProf / self.stopLoss
                    feeDelta = capvar * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry)) * 0.00025 #maker rebate
                    self.feespaid -= feeDelta
                    self.capital = self.capital + tradeDelta + feeDelta
                    if self.graph:
                        self.graph[7] = trade.takeprofit
                        print('added to graph')
                    self.activetrades = []

    def check_ops(self, price):
        for op in self.tradeops: # check trading opportunities
            op.price_routine(price)
            if op.trade: #open trade
                if not len(self.activetrades) + 1 > self.tradeLim: #if the limit isnt already reached
                    self.opentrade(op.trade)
                self.tradeops.remove(op)

            elif not op.valid or op.age > self.opTimeLim: # remove the tradeop
                self.deadops += 1
                self.tradeops.remove(op)        

    def feed_strategy(self, price):
        strat_out = self.strategy.newprice(price)
        if strat_out:
            if self.graph:
                self.graph[3] = strat_out.point # mark min/max for graph
            self.tradeops.append(strat_out) #a retrace_closeclose object

    def opentrade(self, trade):
        if self.graph:
            if trade.trade_type == "Buy":
                self.graph[4] = trade.entry # add to the chart
            if trade.trade_type == "Sell":
                self.graph[4] = trade.entry # add to the chart
            self.graph[5] = trade.stoploss # add to the chart
            self.graph[6] = trade.takeprofit # add to the chart
        else:
            print('This is where you open the trade')

        self.activetrades.append(trade)
        print('capital before', self.capital)
        self.capital += self.capital * (self.risk/(abs(trade.stoploss-trade.entry)/trade.entry)) * 0.00025 #maker rebate
        print('capital after', self.capital)

    def newprice(self, candle):
        if self.graph:
            self.graph = [candle['open'], candle['high'], candle['low'], None, None, None, None, None] # reset the graph array
        self.datapoints += 1

        self.check_ops(candle['open']) # Look for valid trade openings
        self.check_trades(candle) # check the active trades for closes
        self.feed_strategy(candle['open']) # create new opening opportunities



class parameter(object):
    def __init__(self, val, min, max, lv, lv_min):
        self.val = val # value for parameter
        self.min = min # minimum value for parameter
        self.max = max # maximum value for parameter
        self.lv = lv # learning value
        self.lv_min = lv_min # minimum learning value
    
    def randomise(self):
        if isinstance(self.lv_min, int):
            self.val = random.randrange(self.min, self.max+1)
        elif isinstance(self.lv_min, float):
            self.val = random.uniform(self.min, self.max)
        else:
            print('parameter randomisation unsuccessful', self)


"""Master parameters: store the best ones to date here"""
rs = parameter(0.00110097758681489, 0.00001, 0.1, 0.01, 0.000001) # retrace sensitivity
cs = parameter(0.338012931617768, 0.0001, 0.5, 0.01, 0.000001) # close sensitivity
sl = parameter(0.710139900584224, 0.01, 1, 0.01, 0.0001) # stop loss as % of retrace
tp = parameter(0.145143434796625, 0.01, 1, 0.01, 0.0001) # take profit as % of retrace
ps = parameter(3, 2, 20, 0.01, 1) # peak detection sensitivity
tl = parameter(1, 1, 10, 0.01, 1) # max number of active trades
otl = parameter(100, 2, 200, 0.01, 1) # max age of trade opportunity



class prms_list(object):
    def __init__(self, rs, cs, sl, tp, ps, tl, otl):
        self.retraceSens = rs
        self.closeSens = cs
        self.stopLoss = sl
        self.takeProf = tp
        self.pointSens = ps
        self.tradeLim = tl
        self.opTimeLim = otl


params = prms_list(rs, cs, sl, tp, ps, tl, otl)



def run_simulation(params, points):
    bot = mketbot(capital = 1000, risk = 0.01, graph = False, **{k:v.val for (k,v) in params.__dict__.items()})

    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    rsp = random.randrange(points, len(btc)) #random starting point

    for i in range(points): # a weeks worth of minute datapoints
        bot.newprice(btc.iloc[rsp - i]) # feed the bot candles
        if i % 100 == 0:
            print(i)
            print(btc.iloc[rsp - i]['close'])
            print(bot.capital)


    output_dict = bot.__dict__
    print(type(output_dict))
    del output_dict['strategy']
    del output_dict['activetrades']
    del output_dict['tradeops']
    del output_dict['graph']

    with open('fresh_random.csv', 'a') as result:
        w = csv.DictWriter(result, output_dict.keys())
        w.writerow(output_dict)
    print(output_dict)

"""Shows results on a graph"""
def run_simulation_g(params, points): 
    bot = mketbot(capital = 1000, risk = 0.001, graph=True, **{k:v.val for (k,v) in params.__dict__.items()})
    btc = pd.read_csv("../.datasets/Binance_BTCUSDT_minute.csv")
    rsp = random.randrange(points, len(btc)) #random starting point
    l = []

    for i in range(points): # a weeks worth of minute datapoints
        
        bot.newprice(btc.iloc[rsp - i]) # feed the bot candles
        if i % 100 == 0:
            print(i)
            print(btc.iloc[rsp - i]['close'])
            print(bot.capital)

        if bot.graph[3]:
            l[-(bot.pointSens)+1][3] = bot.graph[3] #display the peak on graph minus detection period
            bot.graph[3] = None

        print(bot.graph)
        l.append(bot.graph) #add the row to the graph array

    output_dict = bot.__dict__
    print(type(output_dict))

    del output_dict['strategy']
    del output_dict['activetrades']
    del output_dict['tradeops']
    del output_dict['graph']

    with open('c20_result.csv', 'a') as result:
        w = csv.DictWriter(result, output_dict.keys())
        w.writerow(output_dict)
    print(output_dict)

    df = pd.DataFrame(l, columns=('price', 'high', 'low', 'min/max', 'entry', 'stopLoss', 'takeProfit', 'exit'))
    plt.title("btc bot performance")
    plt.plot(df.index, df['price'])
    plt.plot(df.index, df['high'])
    plt.plot(df.index, df['low'])
    plt.scatter(df.index, df['min/max'], c='dodgerblue')
    plt.scatter(df.index, df['entry'], c='orange')
    plt.scatter(df.index, df['stopLoss'], c='red')
    plt.scatter(df.index, df['takeProfit'], c='green')
    plt.scatter(df.index, df['exit'], c='purple')
    plt.show()


def random_simulation(params, trials):
    for i in params.__dict__.keys():
        params.__dict__[i].randomise()
    
    run_simulation(params, trials)



"""Command Centre"""


run_simulation_g(params, 100000)

# with open("fresh_random.csv", "r") as csvfile:
#     reader = csv.DictReader(csvfile)

#     for p in reader:
#         rs = parameter(float(p['retraceSens']), 0.00001, 0.1, 0.01, 0.000001) # retrace sensitivity
#         cs = parameter(float(p['closeSens']), 0.0001, 0.5, 0.01, 0.000001) # close sensitivity
#         sl = parameter(float(p['stopLoss']), 0.01, 1, 0.01, 0.0001) # stop loss as % of retrace
#         tp = parameter(float(p['takeProf']), 0.01, 1, 0.01, 0.0001) # take profit as % of retrace
#         ps = parameter(int(p['pointSens']), 2, 20, 0.01, 1) # peak detection sensitivity
#         tl = parameter(int(p['tradeLim']), 1, 10, 0.01, 1) # max number of active trades
#         otl = parameter(int(p['opTimeLim']), 2, 200, 0.01, 1) # max age of trade opportunity
        
#         params = prms_list(rs, cs, sl, tp, ps, tl, otl)
     
#         start_time = time.time()

#         run_simulation_g(params, 100000)

#         print("--- %s seconds ---" % (time.time() - start_time))
