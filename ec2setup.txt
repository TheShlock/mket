mkdir -p ~/miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ~/miniconda3/miniconda.sh
bash ~/miniconda3/miniconda.sh -b -u -p ~/miniconda3
rm -rf ~/miniconda3/miniconda.sh
~/miniconda3/bin/conda init bash
~/miniconda3/bin/conda init zsh

conda install -c conda-forge ta-lib

conda create --name env python=3
conda active env
pip install matplotlib
pip install pandas
pip install pandas_datareader
conda install -c conda-forge ta-lib
