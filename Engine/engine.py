from Strategies.Double_Top_Double_Bottom_v2 import str_dubs

def engine(name, strategy, timeframe, liveness, dataset, logging):
    """
    string name
    object strategy : strategy object
    int timeframe : specified as a multiple of a single minute
    bool liveness
    bool logging
    """

    for row in dataset:
        strategy.feed(row)