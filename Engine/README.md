# Double Top Double Bottom Strategy

## What?
Code that runs backtesting and live execution through an interface

Results are saved into a csv file

## Why?
To increase development speed and consistency

## How?

A strategy is nominated to be run
The strategy defines its order type
The order type informs the position at any given time this is implemented with orders on the server though so engine manages that side of it

If the order type is SLTP then positions are opened with a trigger

Every strategy is designed as an online strategy
This means that they are fed candles one at a time
## Results

## Conclusion
