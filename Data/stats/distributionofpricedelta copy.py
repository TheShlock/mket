"""
Specify quanta
log price delta into quanta
plot frequency
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import talib as ta

df = pd.read_csv("bitstamp_hourly.csv")
df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)
df['diff'] = np.append(np.diff(df.close), 0)
df['rdiff'] = df['diff']/df['ATR']

df.to_csv('test.csv')

plt.hist(df['rdiff'], bins = 200)
plt.show()

# TODO normalise difference with respect to ATR

