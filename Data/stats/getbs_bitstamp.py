#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

import requests 
import json 
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import time
# import connect


def condense(period, df):
    """condenses minute data into minute*period data
    which can then be run by runTest"""
    condensed = []
    count = period-1
    for row in df.itertuples():
        count += 1
        if count == period:
            count=0
            condensed.append([row.open, row.high, row.low, row.close])
    df = pd.DataFrame(condensed, columns=['open', 'high', 'low', 'close'])
    return df

def get_bybit_bars(startTime, endTime):
 
    url = "https://www.bitstamp.net/api/v2/ohlc/btcusd/"
 
    startTime = str(int(startTime.timestamp()))
    endTime   = str(int(endTime.timestamp()))
 
    req_params = {'start': startTime, 'end': endTime, 'step' : 3600, 'limit' : 1000}

    """"""
    df0 = pd.DataFrame(json.loads(requests.get(url, params = req_params).content)).transpose()
    df = pd.DataFrame(df0.ohlc.data)
    """"""
    if (len(df.index) == 0):
        return None
     
    # df.index = [dt.datetime.fromtimestamp(x) for x in df.open_time]
 
    return df


print(dt.date.today())
# print(dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=1))

# print(get_bybit_bars('BTCUSD', 1, dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days=1), dt.datetime.now()))
# print(get_bybit_bars('BTCUSD', 1, dt.datetime.now()-dt.timedelta(minutes=60), dt.datetime.now()))

# while True:
#     if round(time.time()) % 60 == 0:
#         time.sleep(1)
#         print(get_bybit_bars('BTCUSD', 1, dt.datetime.now()-dt.timedelta(minutes=60), dt.datetime.now()))
#     time.sleep(0.5)

start = dt.datetime.now().replace(hour=10,minute=0,second=0,microsecond=0)-dt.timedelta(days = 3570)
df_list = []
while True:
    print(start)
    new_df = get_bybit_bars(start, start+dt.timedelta(minutes=1000*60))
    if new_df.size <= 6:
        break
    df_list.append(new_df)
    start += dt.timedelta(minutes=1000*60)

df = pd.concat(df_list)
df.to_csv('bitstamp_hourly.csv')
print(df)



"""Prepare the dataframe with indicators/outputs"""
# df = condense(30, df)
# bb_upper, bb_middle, bb_lower = ta.BBANDS(df.close,timeperiod=20,nbdevup=2,nbdevdn=2,matype=0)
# df = pd.concat([df, bb_upper, bb_middle, bb_lower], axis = 1)
# df.columns = ["open","high","low","close","bb_upper","bb_middle","bb_lower"]
# df['RSI'] = ta.RSI(df.close, timeperiod = 14)
# df['MOM'] = ta.MOM(df.close, timeperiod = 14)
# df["ATR"] = ta.ATR(df.high, df.low, df.close, timeperiod = 14)

# print('this is the result:\n', df)
# df.to_csv('beans.csv')

