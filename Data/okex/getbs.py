#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
from numpy.core.numeric import NaN
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
import time
import collections
import copy
import talib as ta

import requests 
import json 
import pandas as pd
import datetime as dt
import matplotlib.pyplot as plt
import time



 

"""Futures (More info @ https://www.okex.com/markets/futures-coin-list)"""
# r = json.loads(requests.get("https://www.okex.com/api/futures/v3/instruments/").text)
# df = pd.DataFrame(r)
# print(df)
# df.to_csv('futures.csv')


"""Get BTC Klines"""
# r = json.loads(requests.get("https://www.okex.com/api/futures/v3/instruments/BTC-USD-211231/history/candles?start=2021-07-25T02:31:00.000Z&end=2021-07-24T02:55:00.000Z&granularity=60").text) this is the format

start = dt.datetime.now().replace(hour=10, minute=0, second=0, microsecond=0)-dt.timedelta(days=1) # start yesterday at 10 am
end = start-dt.timedelta(minutes=60*300)

df_list = []
while True:
    print(start)
    r = pd.DataFrame(json.loads(requests.get("https://www.okex.com/api/futures/v3/instruments/BTC-USD-211231/history/candles?start="+start.isoformat()+"Z&end="+end.isoformat()+"Z&granularity=1800").text))
    time.sleep(1)
    if r.empty:
        break
    df_list.append(r)
    start -= dt.timedelta(minutes=30*300)
    end -= dt.timedelta(minutes=30*300)
    print(df_list)

df = pd.concat(df_list)
print(df)
df.to_csv('btc_kline.csv')
