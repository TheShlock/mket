#Samuel Lockton 2021 ©

# Mathematical packages
import matplotlib
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
import random
import math

# Utilities
import csv
from time import time, sleep
from datetime import datetime
import collections
import copy
import talib as ta

df = pd.read_csv('live_log.csv')
print(df)
pd.to_datetime(df['datetime_actual'])
print(df.info())
for k, row in df.iterrows(): # this code segment removes duplicate rows

    if k != 0:
        # print(float(row['datetime_attempt']) - float(row['datetime_actual']))

        if not ((pd.to_datetime(row['datetime_actual'])-pd.to_datetime(row['datetime_attempt'])).total_seconds()) > 0.5:
            df.drop(k, inplace=True)
        

print(df)
df.to_csv('delet.csv')