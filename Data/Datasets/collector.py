#Samuel Lockton 2021 ©
import bybit
import json
import websocket
from BybitWebsocket import BybitWebsocket
from threading import *
from time import sleep, time
from datetime import datetime
import csv


api_key = "98wy9HVGx411FqWflH"
api_secret = "nVwRJDxM7b582XgMVjdmIdYxlsTv4Gl1YVFF"

"""Change Symbol Here"""
symbol = "BTCUSD"

"""Websocket Client"""
ws = BybitWebsocket(wsURL="wss://stream.bytick.com/realtime", api_key=api_key, api_secret=api_secret)
ws.subscribe_instrument_info(symbol)
ws.subscribe_position()
ws.subscribe_execution()
ws.subscribe_order()

"""Live Price Thread
Runs concurrently, access a price by calling pricedict['ticker']
Can be extended to incorporate more data sources or configured"""

class livePrices(Thread):
    def run(self):
        global pricedict
        pricedict = {} # holds the most recently sent price
        while True:
            pricedata = ws.get_data("instrument_info.100ms."+symbol)
            if pricedata:
                try:
                    pricedict[symbol] = pricedata['update'][0]['last_price_e4']/10000
                    print(pricedict[symbol])  
                except:
                    pass

livePrices().start()
print('start')

def dataCollector(period):
    while pricedict:
        print('datacollector() running')
        data = {}
        data['high'] = pricedict[symbol]
        data['low'] = pricedict[symbol]
        data['open'] = pricedict[symbol]
        data['close'] = pricedict[symbol]
        price = pricedict[symbol]
        storetime = 0
        while True:
            if pricedict[symbol] != price:
                price = pricedict[symbol]
                if price > data['high']:
                    data['high'] = price
                if price < data['low']:
                    data['low'] = price
            if time() - storetime > 1:
                if round(time()-0.5)%period == 0:
                    storetime = time()
                    data['close'] = pricedict[symbol]
                    data['timestamp'] = str(datetime.now())
                    print(data)
                    with open(symbol+'_live.csv', 'a') as file:
                        file.write(str(data['timestamp']) + ',' + str(data['open']) + ',' + str(data['high']) + ',' + str(data['low']) + ',' + str(data['close']) + '\n' )
                    data['high'] = pricedict[symbol]
                    data['low'] = pricedict[symbol]
                    data['open'] = pricedict[symbol]

while not pricedict: # code will error if this is gone
    pass

dataCollector(60)

