from __future__ import print_function
import websocket

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.create_connection("wss://www.bitmex.com/realtime?subscribe=instrument,orderBookL2_25:XBTUSD")
    print("Sending 'Hello, World'...")
    ws.send("Hello, World")
    ws.send("help")
    print("Sent")
    print("Receiving...")
    result = ws.recv()
    print("Received '%s'" % result)
    ws.close()