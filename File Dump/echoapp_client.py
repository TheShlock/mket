import websocket
# from __future__ import print_function
from threading import Thread
import time
import sys


import websocket

if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.create_connection("wss://www.bitmex.com/realtime?subscribe=instrument,orderBookL2_25:XBTUSD")
    print("Sending 'Hello, World'...")
    ws.send("Hello, World")
    ws.send("help")
    print("Sent")
    print("Receiving...")
    result = ws.recv()
    print("Received '%s'" % result)
    ws.close()


if __name__ == "__main__":
    websocket.enableTrace(True)
    if len(sys.argv) < 2:
        host = "wss://www.bitmex.com/realtime?subscribe=instrument,orderBookL2_25:XBTUSD"
    else:
        host = sys.argv[1]
    ws = websocket.WebSocketApp(host,
                                on_message=on_message,
                                on_error=on_error,
                                on_close=on_close)
    ws.on_open = on_open
    ws.run_forever()


def on_message(ws, message):
    print(message)


def on_error(ws, error):
    print(error)


def on_close(ws):
    print("### closed ###")


def on_open(ws):
    def run(*args):
        for i in range(3):
            # send the message, then wait
            # so thread doesn't exit and socket
            # isn't closed
            ws.send("Hello %d" % i)
            time.sleep(1)

        time.sleep(1)
        ws.close()
        print("Thread terminating...")

    Thread(target=run).start()


