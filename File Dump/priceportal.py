from BybitWebsocket import BybitWebsocket
from time import sleep


if __name__ == "__main__":

    # inverse perpetual
    ws = BybitWebsocket(wsURL="wss://stream-testnet.bybit.com/realtime",
                         api_key="", api_secret="")

    ws.subscribe_instrument_info('BTCUSD')
    pricedata = None
    while(1):
        data = ws.get_data("instrument_info.100ms.BTCUSD")
        if data:
            # print(data)
            try:
                pricedata = data['update'][0]['bid1_price_e4']/10000
            except:
                pass
