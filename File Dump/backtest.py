
# should use classes so that you can say entry_exit.delta_p instead of entry_exit[3]
# will make it easier for yourself for extensibility

def backtest(pd, entry, exit):
    """Returns a list of (entry, exit, type) lists
    entry and exit will be timestamps, type is short/long"""


"""Run backtest to get entries and exits
Calculate profit from these entries and """

def trade_outcomes(pd, entry_exit):
    """calculate based on percentage so that risk appetite can be factored in
    will return the entry_exit array with delta_p(change in %) appended to each item."""
    trades = []
    for trade in entry_exit:
        entry_price = pd[entry_exit[trade][0]]
        exit_price = pd[entry_exit[trade][0]]
        if entry_exit[trade][2] == 'long':
            delta_p = (pd(entry)-pd(exit)) / pd(entry)
            entry_exit[trade].append(delta_p)
        if entry_exit[trade][2] == 'short':
            delta_p = (pd(exit)-pd(entry)) / pd(entry)
            entry_exit[trade].append(delta_p)
    
    return entry_exit

def prof_loss(entry_exit):
    capital = 100
    for trade in entry_exit:
        capital *= entry_exit[trade][3]
    
    return capital