# Import WebSocket client library (and others)
import websocket
import json
import _thread
import time
pricedict = {}
pairs = {}
# Define WebSocket callback functions
def ws_message(ws, message):
    parsed = json.loads(message)
    # print(json.dumps(parsed, indent=4, sort_keys=True))
    if 'data' in parsed:
        # print('=====================================================================================')
        # print(parsed['data'][0]['symbol'])
        # print(parsed['data'][0]['price'])
        pricedict[parsed['data'][0]['symbol']] = parsed['data'][0]['price']
        # print(pricedict)


def ws_open(ws):
    ws.send('{"event":"subscribe", "subscription":{"name":"trade"}, "pair":["XBT/USD","XRP/USD"]}')

def ws_thread(*args):
    ws = websocket.WebSocketApp("wss://www.bitmex.com/realtime?subscribe=trade", on_open = ws_open, on_message = ws_message)
    ws.run_forever()

# Start a new thread for the WebSocket interface
_thread.start_new_thread(ws_thread, ())

# Continue other (non WebSocket) tasks in the main thread
xbt_price = 0
while True:
    time.sleep(1)
    print("Main thread: %d" % time.time())
    try:
        print(pricedict['XBTUSD'])
    except:
        print('price data not available yet')






