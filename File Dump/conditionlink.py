"""Was going to use this as a general logic node to encompass all strategies though I decided it would be unnecessary, still good for inspiration"""
class conditionlink(object):
    def __init__(self, validation_condition, invalidation_condition, child, parent, state):
        self.validation_condition = validation_condition
        self.invalidation_condition = invalidation_condition
        self.child = child
        self.parent = parent
        self.state = 'valid'

    def newprice(self, price):
        """Returns action for containing class to perform on the chain"""
        invalid_out = self.invalidation_condition(price)
        valid_out =  self.validation_condition(price)
        return (invalid_out, valid_out)

        # feed price into invalidation first then validation condition
        # these functions should automatically update the state of the link

