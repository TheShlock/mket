# Import mathematical packages
from os import linesep
import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema

# Import WebSocket client library (and others)
import websocket
import json
import _thread
import time
import csv


class trade(object):
    def __init__(self, entry, stoploss, takeprofit, trade_type):
        self.entry = entry
        self.stoploss = stoploss
        self.takeprofit = takeprofit
        self.trade_type = trade_type
        self.outcome = False
        self.risk = 0
        self.reward = 0

        if self.trade_type == 'short':
            self.risk = (self.stoploss-self.entry)/self.entry
            self.reward = (self.entry-self.takeprofit)/self.entry
        if self.trade_type == 'long':
            self.risk = (self.entry-self.stoploss)/self.entry
            self.reward = (self.entry-self.takeprofit)/self.entry


    def close(self, outcome):
        self.outcome = outcome


class retrace_closeclose(object):
    """Singular object to contain conditional logic to open trade
    point: price
    point_type: 'min' or 'max'
    retrace_threshold: how many percent the price has to move from the point to validate the trade
    close_sens: within what percent of the retracement the price has to close to open trade"""
    def __init__(self, point, point_type, retrace_threshold, close_sens):

        self.retraced = False
        self.point = point
        self.point_type = point_type
        self.retrace_threshold = retrace_threshold
        self.retracement_percent = 0
        self.close_sens = close_sens 
        self.valid = True
        self.trade = False

    def price_routine(self, price):
        self.calc_retracement(price)
        if not self.retraced:
            self.check_retracement_threshold()
        if self.retraced and self.closeclose(price): #return trade object
            if self.point_type == 'min':
                self.trade = trade(price, price + price * (self.retracement_percent/4), price - price * self.retracement_percent/2, 'short')
            if self.point_type == 'max':
                self.trade = trade(price, price - price * (self.retracement_percent/4), price + price * self.retracement_percent/2, 'long')
        else:
            self.invalidation(price)

    def invalidation(self, price):
        if self.point_type == 'max' and price > self.point:
            # print('self.point_type: ', self.point_type, 'price: ', price, 'self.point: ', self.point)
            self.valid = False
        if self.point_type == 'min' and price < self.point:
            # print('self.point_type: ', self.point_type, 'price: ', price, 'self.point: ', self.point)
            self.valid = False

    def calc_retracement(self, price):
        if self.point_type == 'max':
            self.retracement_percent = max(1 - (price / self.point), self.retracement_percent)
        if self.point_type == 'min':
            self.retracement_percent = max(1 - (self.point / price), self.retracement_percent)
    
    def check_retracement_threshold(self):
        if self.retracement_percent > self.retrace_threshold:
            # print('=================================================================================')
            # print('RETRACED = TRUE')
            self.retraced = True
    
    def closeclose(self, price):
        range_size = self.point * self.retracement_percent * self.close_sens
        if price > self.point - range_size and price < self.point + range_size:
            return True
        return False


class dubtop(object):
    """Double Top, Double Bottom strategy. Takes price and finds min and max points.
    Returns retrace_closeclose objects for each of these points"""
    def __init__(self, retrace_percent, close_sens, pointsens):
        self.retrace_percent = retrace_percent # how far the price must retrace to be valid
        self.close_sens = close_sens # sensitivity of second close
        self.datawindow = []
        self.pointsens = pointsens # sensitivity of finding points

    def newprice(self, price):
        if len(self.datawindow) >= 15:
            self.datawindow.pop(0)
            self.datawindow.append(price)
        else:
            self.datawindow.append(price)
        
        df = pd.DataFrame(self.datawindow, columns=['data'])

        if len(df)==15:
            df['min'] = df.iloc[argrelextrema(df.data.values, np.less_equal,
                            order=self.pointsens)[0]]['data']
            df['max'] = df.iloc[argrelextrema(df.data.values, np.greater_equal,
                            order=self.pointsens)[0]]['data']
        
        if len(df) == 15:
            if not pd.isna(df.iloc[14]['min']):
                return retrace_closeclose(df.iloc[14]['min'], 'min', self.retrace_percent, self.close_sens)
            if not pd.isna(df.iloc[14]['max']):
                return retrace_closeclose(df.iloc[14]['max'], 'max', self.retrace_percent, self.close_sens)
        
        return False


class mketbot(object):
    """Manages sending price data to a strategy, condition chains and the opening of trades"""
    def __init__(self, capital, risk, retrace, closesens):

        self.capital = capital
        self.tradevalue = 0
        self.equity = 0

        self.tradevalue = 0
        self.position = 0

        self.risk = risk # %of capital you will lose on stoploss hit
        
        self.strategy = dubtop(retrace, closesens, 10)

        self.activetrades = []
        self.won_trades = 0
        self.lost_trades = 0

        self.tradeops = [] # valid and current trades
        self.deadops = [] # invalidated trades
        self.finishedtrades = [] # executed trades
        self.trades_not_executed = [] #valid, but unexecuted trades


    def check_trades(self, price):
        for trade in self.activetrades:
            if trade.trade_type == 'short':
                if price > trade.stoploss:
                    trade.outcome = 'loss'
                    self.lost_trades += 1
                    self.finishedtrades.append(trade)
                    self.capital *= 1-self.risk
                    print('trade closed @ ', price, 'at a loss')
                    self.activetrades.remove(trade)
                    
                elif price < trade.takeprofit:
                    trade.outcome = 'profit'
                    self.won_trades += 1
                    self.finishedtrades.append(trade)
                    self.capital *= 1 + (2 * self.risk)
                    print('trade closed @ ', price, 'at a profit')
                    self.activetrades.remove(trade)

            elif trade.trade_type == 'long':
                if price < trade.stoploss:
                    trade.outcome = 'loss'
                    self.lost_trades += 1
                    self.finishedtrades.append(trade)
                    self.capital *= 1-self.risk
                    print('trade closed @ ', price, 'at a loss')
                    self.activetrades.remove(trade)

                elif price > trade.takeprofit:
                    trade.outcome = 'profit'
                    self.won_trades += 1
                    self.finishedtrades.append(trade)
                    self.capital *= 1 + (2 * self.risk)
                    print('trade closed @ ', price, 'at a profit')
                    self.activetrades.remove(trade)


                
    def feed_strategy(self, price):
        strat_out = self.strategy.newprice(price)
        if strat_out:
            self.tradeops.append(strat_out) #a retrace_closeclose object


    def newprice(self, price):

        self.check_trades(price)
            
        for op in self.tradeops: # check trading opportunities
            op.price_routine(price)
            if op.trade: #open trade
                self.activetrades.append(op.trade)
                self.tradeops.remove(op)

            elif not op.valid: # remove the tradeop
                # print('removing trade op, op.valid = ', op.valid)
                self.deadops.append(op)
                self.tradeops.remove(op)

        self.feed_strategy(price)
        
        # print('================================')
        # print('price: ', price)
        # print('capital: ', self.capital)
        # print('position: ', self.position)
        # print('active trades: ', self.activetrades)
        # print('equity: ', self.equity)
        # print('won trades:', self.won_trades)
        # print('lost_trades', self.lost_trades)
        # # print('finished_trades: ', self.finishedtrades)
        # print('trade ops: ', len(self.tradeops))
        # print('dead ops: ', len(self.deadops))

        # if self.activetrades:
        #     for trade in self.activetrades:
        #         print(trade.__dict__)


starting_capital = 1000








# WEB HOOKED INSTANCE
# pricedict = {}
# pairs = {}

# # Define WebSocket callback functions
# def ws_message(ws, message):
#     parsed = json.loads(message)
#     if 'data' in parsed:
#         pricedict[parsed['data'][0]['symbol']] = parsed['data'][0]['price']

# def ws_open(ws):
#     ws.send('{"event":"subscribe", "subscription":{"name":"trade"}, "pair":["XBT/USD","XRP/USD"]}')

# def ws_thread(*args):
#     ws = websocket.WebSocketApp("wss://www.bitmex.com/realtime?subscribe=trade", on_open = ws_open, on_message = ws_message)
#     ws.run_forever()

# # Start a new thread for the WebSocket interface
# _thread.start_new_thread(ws_thread, ())

# while True:
#     time.sleep(1)
#     print("Main thread: %d" % time.time())
#     pricedata = False
#     try:
#         cur_price = pricedict['XBTUSD'] 
#         pricedata = True
#     except:
#         print('waiting')
#     if pricedata:
#         bot.newprice(pricedict['XBTUSD'])

# BACKTEST INSTANCE
btc = pd.read_csv("btc_minute.csv")
eth = pd.read_csv("eth_minute.csv")
ltc = pd.read_csv("ltc_minute.csv")
xrp = pd.read_csv("xrp_minute.csv")

# bot = mketbot(1000, 0.01, 0.01, 0.005)

def testfunc(retracesens, closesens, coin):
    bot = mketbot(1000, 0.01, retracesens, closesens)
    for i in range(10000):
        # print('\n')
        # print('i: ', i, 'price: ', btc.iloc[1001 - i]['close'])

        bot.newprice(coin.iloc[10001 - i]['close'])
    
    return [b, r, c, bot.won_trades, bot.lost_trades, bot.capital]



header = ['coin', 'retrace %', 'close sens %', 'won trades', 'lost trades', 'capital']
retracelist = [0.1, 0.05, 0.01, 0.005, 0.001]
closesenslist = [0.1, 0.05, 0.01, 0.005, 0.001, 0.0005, 0.0001]
coinlist = [btc, eth, ltc, xrp]

with open('bigtest.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)

    # write the header
    writer.writerow(header)
    
    #write the data
    for r in retracelist:
        for c in closesenslist:
            for b in coinlist:
                writer.writerow(testfunc(r, c, b))

