import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema

print('getting data')
stocks = ['GOOG', 'AMZN']
data = pdr.get_data_yahoo(stocks, start = '2020-01-01')['Close']

print('data gotten')
xs = data.AMZN.tolist()
df = pd.DataFrame(xs, columns=['data'])



n = 10  # number of points to be checked before and after this adjusts sensitivity of highs and low

# Find local peaks

#argrelextrema will only take an array as input
df['min'] = df.iloc[argrelextrema(df.data.values, np.less_equal,
                    order=n)[0]]['data']
df['max'] = df.iloc[argrelextrema(df.data.values, np.greater_equal,
                    order=n)[0]]['data']

# this one looks nice
print(df.to_string())


def check_for_entries(price, minlist, maxlist):
    """avoid double checking just entered values in a different way
    current method will ignore perfect double tops/bottoms"""
    for min in minlist:
        if (price-min)/price < 0.01 and price != min:
            return ('long')
    for max in maxlist:
        if (price-max)/price < 0.01 and price != max:
            return ('short')
    return False

def validate_min_and_max(price, minlist, maxlist):
    for min in minlist:
        if price < min:
            print('remove min')
            minlist.remove(min)

    for max in maxlist:
        if price > max:
            print('remove max')
            maxlist.remove(max)


class trade(object):
    def __init__(self, entry_price, trade_type):
        self.entry_price = entry_price
        self.trade_type = trade_type
        if trade_type == 'short':
            self.stoploss = entry_price * 1.01
            self.takeprofit = entry_price * 0.98
        if trade_type == 'long':
            self.stoploss = entry_price * 0.99
            self.takeprofit = entry_price * 1.02
        self.outcome = False
    
    def close(self, outcome):
        self.outcome = outcome


def checktrade(price, trade):
    if trade.trade_type == 'short':
        if price > trade.stoploss:
            trade.close('loss')
            print('trade closed @ ', price, trade.outcome)
        if price < trade.takeprofit:
            trade.close('profit')
            print('trade closed @ ', price, trade.outcome)

    if trade.trade_type == 'long':
        if price < trade.stoploss:
            trade.close('loss')
            print('trade closed @ ', price, trade.outcome)

        if price > trade.takeprofit:
            trade.close('profit')
            print('trade closed @ ', price, trade.outcome)

minlist = []
maxlist = []
trades = []

for row in df.itertuples():
    print('======================================================')
    print(row)
    print('minlist', minlist)
    print('maxlist', maxlist)

    if len(trades) != 0 and trades[-1].outcome is False: #if active trade
        # print(trades[-1].entry_price)
        checktrade(row.data, trades[-1])
    
    if len(trades) == 0: # if there isnt an active trade
        print('checking for entries')
        if check_for_entries(row.data, minlist, maxlist): # check for entries
            print('trade opened: ', row.data, check_for_entries(row.data, minlist, maxlist))
            trades.append(trade(row.data, check_for_entries(row.data, minlist, maxlist))) #open trade

    if len(trades) != 0 and trades[-1].outcome: # if there isnt an active trade
        print('checking for entries')
        if check_for_entries(row.data, minlist, maxlist): # check for entries
            print('trade opened: ', row.data, check_for_entries(row.data, minlist, maxlist))
            trades.append(trade(row.data, check_for_entries(row.data, minlist, maxlist))) #open trade

    validate_min_and_max(row.data, minlist, maxlist)

    if not pd.isna(row.min):
        print('row min: ', row.min) 
        minlist.append(row.min) # add the min point if it exists
    if not pd.isna(row.max):
        print('row max: ', row.max)
        maxlist.append(row.max) # add the max point if it exists
    

print(trades)
print('number of trades: ', len(trades))
won_trades = 0
lost_trades = 0
capital = 1000
print('starting capital: ', capital)
for trade in trades:
    if trade.outcome == 'loss':
        lost_trades += 1
        capital *= 0.99
    if trade.outcome == 'profit':
        won_trades += 1
        capital *= 1.02

print('won_trades: ', won_trades)
print('lost_trades: ', lost_trades)
print('ending capital: ', capital)



# Plot results
plt.title("Amazon Share Price")
plt.scatter(df.index, df['min'], c='r')
plt.scatter(df.index, df['max'], c='g')
plt.plot(df.index, df['data'])
plt.show()