import numpy as np
import pandas as pd
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema


stocks = ['GOOG', 'TLS']
data = pdr.get_data_yahoo(stocks, start = '2019-01-01')['Close']


xs = data.TLS.tolist()
df = pd.DataFrame(xs, columns=['data'])



# plt.plot(data.GOOG)
# plt.ylabel('some numbers')
# plt.show()



n = 10  # number of points to be checked before and after

# Find local peaks

df['min'] = df.iloc[argrelextrema(df.data.values, np.less_equal,
                    order=n)[0]]['data']
df['max'] = df.iloc[argrelextrema(df.data.values, np.greater_equal,
                    order=n)[0]]['data']

# Plot results
plt.title("Amazon Share Price")
plt.scatter(df.index, df['min'], c='r')
plt.scatter(df.index, df['max'], c='g')
plt.plot(df.index, df['data'])
plt.show()